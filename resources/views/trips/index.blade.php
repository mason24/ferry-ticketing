@extends('layout.master')
@section('content')
                        <div class="box pattern pattern-sandstone">
                            <div class="box-header">
                                <i class="icon-table"></i>
                                <h5>
                                    Trips
                                </h5>
                            </div>
                            <div class="box-content box-table">
                            <div><a href="trips/create">New Trip</a></div>
                            Sort: <a href="{{ $url }}&sort=asc">Ascending</a> | <a href="{{ $url }}&sort=desc">Descending</a>
                            <form action="{{ $url }}" method="get">
                                <table id="sample-table" class="table table-hover table-bordered tablesorter">
                                    <thead>
                                        <tr>
                                            <th>                        
                                                <div class="controls">
                                                    <select class="span3" name="origin">
                                                            <option value="0">--Origin--</option>
                                                        @foreach($jetties as $jetty)
                                                            <option value="{{ $jetty->id }}" {{ $origin == $jetty->id ? 'selected' : ''}} >{{ $jetty->Jetty_Name }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </th>
                                            <th>                        
                                                <div class="controls">
                                                    <select class="span3" name="destination">
                                                            <option value="0">--Destination--</option>
                                                        @foreach($jetties as $jetty)
                                                            <option value="{{ $jetty->id }}" {{ $destination == $jetty->id ? 'selected' : ''}} >{{ $jetty->Jetty_Name }}</option>
                                                        @endforeach
                                                    </select>                                                    
                                                </div>
                                            </th>
                                            <th>                        
                                                <div class="controls">
                                                    <select name="vessel" class="span3">
                                                        <option value="0">--Choose Vessel--</option>
                                                        @foreach($vessels as $vessel)
                                                        <option value="{{ $vessel->id }}" {{ $vessel_id == $vessel->id ? 'selected' : '' }} >{{ $vessel->Vessel_Name }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </th>
                                            <th>                        
                                                <div class="input-prepend date datepicker">
                                                    <span class="add-on"><i class="icon-th"></i></span>
                                                    <input name="date" class="span2" type="text" placeholder="Date" value="{{ $selected_depart_date }}" readonly="readonly" style="cursor:pointer"/>
                                                </div>   
                                            </th>                                            
                                            <th>                        
                                                <div class="controls">
                                                    <input id="current-pass-control" name="departure_datetime" class="span2" type="text" placeholder="Departure Datetime" value="{{ $depart_time }}"/>
                                                </div>
                                            </th>

                                            <th>                        
                                                <div class="controls">
                                                    <input id="current-pass-control" name="arrival_datetime" class="span2" type="text" placeholder="Arrival Datetime" value="{{ $arrival_time }}"/>
                                                </div>
                                            </th>
                                            <th>                                                
                                                <div class="controls">
                                                    <input class="btn btn-primary" type="submit" value="Filter"/>
                                                </div>
                                                <div class="controls">
                                                    <a class="btn btn-default" href="{{ URL::to('/trips') }}">Clear Filter</a>
                                                </div>                                                
                                            </th>
                                        </tr>                                    
                                        <tr>
                                            <!-- <th>Trip Code</th>
                                            <th>Trip Name</th> -->
                                            <th>Origin</th>
                                            <th>Destination</th>
                                            <th>Operating Vessel</th>
                                            <th>Date</th>
                                            <th>Departure Datetime</th>
                                            <th>Arrival Datetime</th>
                                            <th class="td-actions"></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    	@foreach ($trips as $trip)
	                                        <tr>
	                                            <!-- <td>{{ $trip->Trip_Code }}</td>
                                                <td>{{ $trip->Trip_Name }}</td> -->
                                                <td>{{ $trip->route->route_from->Jetty_Name }}</td>
                                                <td>{{ $trip->route->route_to->Jetty_Name }}</td>
                                                <td>{{ $trip->vessel->Vessel_Name }}</td>
                                                <td>{{ $trip->Depart_Date }}</td>
                                                <td>{{ $trip->Depart_Time }}</td>
                                                <td>{{ $trip->Arvl_Time }}</td>
	                                            <td class="td-actions">
	                                                <a href="trips/edit/{{ $trip->id }}" class="btn btn-small btn-info">
	                                                    <i class="btn-icon-only icon-edit"></i>
	                                                </a>

	                                                <a href="trips/delete/{{ $trip->id }}" class="btn btn-small btn-danger">
	                                                    <i class="btn-icon-only icon-remove"></i>
	                                                </a>
	                                            </td>
	                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </form>
                                {{ $trips->links() }}
                            </div>
                        </div>
@endsection