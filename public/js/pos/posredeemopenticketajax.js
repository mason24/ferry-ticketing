var site_url = window.location.href;


   
$(document).ready(function(){
	 


	$(".onward-trip-date").change(function () {

			$('.onwarddatetext').html($('.onward-trip-date').val());

			$('input[name=\'onward_date\']').val($('.onward-trip-date').val());

			var originID = $('input[name="origin"]').val();

			var destinationID = $('input[name="destination"]').val();

	        var url = site_url+"/trips/"+originID+"/"+destinationID+"/onward/"+$('.onward-trip-date').val();

	        $.ajax({
	            url: url,
	            success: function (data) {
	                $('.onward-trips').html(data);
	            },
	            error: function (data) {
	                console.log('Error:', data);
	            }
	        }); 
	    

	});	

	// $(".return-trip-date").change(function () {

	// 		$('.returndatetext').html($('.return-trip-date').val());

	// 		$('input[name=\'return_date\']').val($('.return-trip-date').val());		

	// 		var originID = $('input[name="origin"]').val();

	// 		var destinationID = $('input[name="destination"]').val();		

	//         var url = site_url+"/trips/"+destinationID+"/"+originID+"/return/"+$('.return-trip-date').val();

	//         $.ajax({
	//             url: url,
	//             success: function (data) {
	//                 $('.return-trips').html(data);
	//             },
	//             error: function (data) {
	//                 console.log('Error:', data);
	//             }
	//         }); 

	//     disable_return_trips();

	// });	    

});

$("input[name='discount']").keyup(function(){

	var subtotal = parseFloat($('input[name="subtotal"]').val());

	var discount_amount = parseFloat($(this).val());

    $("#tds").html(discount_amount.toFixed(2));

    var new_gtotal = subtotal - discount_amount;

    $('input[name="gtotal"]').val(new_gtotal.toFixed(2));

    $('#gtotal').html(new_gtotal.toFixed(2));

});

function availability(elem,type){

	if(type == 'return'){
		$('.journeyicon').html('<div class="fa fa-exchange"></div>');
	}
	else{
		$('.journeyicon').html('<div class="fa fa-long-arrow-right"></div>');
	}

	$('.'+type+'triptime').html(elem.attr('data-time-text'));

	$('.'+type+'-trip-button').removeClass('active');

	elem.addClass('active');

	var trip_id = elem.attr('rel');

	var url = site_url+"/availability/"+trip_id+'/'+type;

	$('input[name="'+type+'_trip"]').val(trip_id);

    $.ajax({
        url: url,
        success: function (data) {

            $('.'+type+'-availability').html(data);

		   // This button will increment the value	

        },
        error: function (data) {
            console.log('Error:', data);
        }
    }); 

}

function select_destination(elem){

	$('input[name=\'destination\']').val(elem.attr('rel'));

	$('.destinationtext').html(elem.attr('data-destination-code'));

}

function total_passenger(elem){

	if(elem.attr('data-trip-type') == 'onward'){

		var total_passengers = 0;

		var total_adults = 0;

		var total_children = 0;	

		// var total_adults = $('.total_adults').val() == '' ? 0 : $('.total_adults').val();

		// var total_children = $('.total_children').val() == '' ? 0 : $('.total_children').val();

		for(var a=0;a<$('.onward_adults').length;a++){

			total_adults += parseInt($('.onward_adults').eq(a).val());

		}

		$('input[name="total_adults"]').val(total_adults);

		for(var c=0;c<$('.onward_children').length;c++){

			total_children += parseInt($('.onward_children').eq(c).val());

		}

		$('input[name="total_children"]').val(total_children);	

		total_passengers = total_adults + total_children;

		$('#total-passengers').html(total_passengers);

	    // var url = site_url+"/passenger-details/"+total_adults+"/"+total_children;

	    // $.ajax({
	    //     url: url,
	    //     success: function (data) {
	    //         $('.passenger-details-body').html(data);
	    //     },
	    //     error: function (data) {
	    //         console.log('Error:', data);
	    //     }
	    // }); 

	}

}

function check_passenger_selection(elem){

	var triptype = elem.attr('data-trip-type');

	if(triptype == 'onward'){

		var total_passengers = 0;

		var tripid = elem.attr('data-trip-id');

		var vesselseatid = elem.attr('data-vessel-seat-id');

		var availability = $('.vessel_seat_'+tripid+'_'+vesselseatid).val();

		for(var i = 0;i < $('.passenger_'+tripid+'_'+vesselseatid).length;i++){

			total_passengers += parseFloat($('.passenger_'+tripid+'_'+vesselseatid).eq(i).val());

		}

		if(total_passengers > availability){

			alert('You have selected more passengers than the availability');

			return false;
		}

	}
	else if(triptype == 'return'){

		var total_adults = $('input[name="total_adults"]').val();

		var total_children = $('input[name="total_children"]').val();

		var return_total_adults = 0;

		var return_total_children = 0;

		for(var a=0;a<$('.return_adults').length;a++){

			return_total_adults += parseInt($('.return_adults').eq(a).val());

		}

		if(total_adults < return_total_adults){

			alert('You can only select '+total_adults+' adult'+(total_adults > 1 ? 's' : ''));

			return false;

		}

		for(var c=0;c<$('.return_children').length;c++){

			return_total_children += parseInt($('.return_children').eq(c).val());

		}

		if(total_children < return_total_children){

			alert('You can only select '+total_children+' '+(total_children > 1 ? 'children' : 'child'));

			return false;

		}			

	}

	

	update_ticket_details(triptype);

}

function update_ticket_details(triptype){

	var html = '';

	var passenger_type_text = '';

	var vessel_seat_name = '';

	var gtotal = 0;

	// var total_adults = 0;

	// var total_children = 0;

	var passenger_number = 1;

	for(var i=0;i<$('.onward-passengers').length;i++){

		if($('.onward-passengers').eq(i) != '0'){

			passenger_type_text = $('.onward-passengers').eq(i).attr('data-passenger-type-text');

			vessel_seat_category_id = $('.onward-passengers').eq(i).attr('data-seat-category-id');

			vessel_seat_name = $('.onward-passengers').eq(i).attr('data-vessel-seat-name');

			var total_passengers = parseInt($('.onward-passengers').eq(i).val());

			for(var j=0;j<total_passengers;j++){

				var price = parseFloat($('.onward-passengers').eq(i).attr('data-price'));



				html += '<div class="col-md-8"><div class="form-group"><span style="font-size:12px">P'+passenger_number+' '+passenger_type_text+' ('+vessel_seat_name+') <a href="#" data-toggle="modal" data-target="#onward_passenger_details_'+j+'_modal"><i class="fa fa-edit"></i></a> <input type="checkbox"/> Citizen</span></div></div><div class="col-md-4"><div class="form-group"><div class="text-right"><label class="">RM '+price.toFixed(2)+'</label></div></div></div>';

				html += ' <div class="modal fade" id="onward_passenger_details_'+j+'_modal" role="dialog"> <div class="modal-dialog"> <div class="modal-content"> <div class="modal-header"> <button type="button" class="close" data-dismiss="modal">&times;</button> <h4 class="modal-title">Passenger '+passenger_number+' Details</h4> </div><div class="modal-body"> <div class="row"> <div class="col-md-6"> <div class="form-group"> <label>Full Name</label> <input type="text" name="passenger_name[]" class="form-control"/> </div></div><div class="col-md-6"> <div class="form-group"> <label>Identity Type</label> <select class="form-control" name="id_type[]"> <option>Mykad</option> <option>Mykid</option> <option>Passport</option> </select> </div></div><div class="col-md-6"> <div class="form-group"> <label>Identity Number</label> <input type="text" name="identity_number[]" class="form-control"/> </div></div></div></div><div class="modal-footer"> <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> </div></div></div></div>';

				html += '<input type="hidden" name="passenger_price[]" value="'+price.toFixed(2)+'"/>';

				html += '<input type="hidden" name="passenger_seat_category_id[]" value="'+vessel_seat_category_id+'"/>';

				html += '<input type="hidden" name="passenger_type[]" value="'+passenger_type_text+'"/>';

				gtotal+=price;

				passenger_number++;

			}

		}


		
	}

	$('.panel-onward').html(html);

	// $('.panel-return').html(html);

	// if(triptype == 'return'){

	// 	html = '';

	// 	passenger_number = 1;

	// 	for(var i=0;i<$('.return-passengers').length;i++){

	// 		passenger_type_text = $('.onward-passengers').eq(i).attr('data-passenger-type-text');

	// 		vessel_seat_name = $('.onward-passengers').eq(i).attr('data-vessel-seat-name');

	// 		if($('.return-passengers').eq(i) != '0'){

	// 			var total_passengers = parseInt($('.return-passengers').eq(i).val());

	// 			for(var j=0;j<total_passengers;j++){

	// 				var price = parseFloat($('.return-passengers').eq(i).attr('data-price'));

	// 				html += '<div class="col-md-8"><div class="form-group"><span style="font-size:12px">P'+passenger_number+' '+passenger_type_text+' ('+vessel_seat_name+')</div></div><div class="col-md-4"><div class="form-group"><div class="text-right"><label class="">RM '+price.toFixed(2)+'</label></div></div></div>';

	// 				// html += ' <div class="modal fade" id="return_passenger_details_'+j+'_modal" role="dialog"> <div class="modal-dialog"> <div class="modal-content"> <div class="modal-header"> <button type="button" class="close" data-dismiss="modal">&times;</button> <h4 class="modal-title">Passenger '+passenger_number+' Details</h4> </div><div class="modal-body"> <div class="row"> <div class="col-md-6"> <div class="form-group"> <label>Full Name</label> <input type="text" class="form-control"/> </div></div><div class="col-md-6"> <div class="form-group"> <label>Identity Type</label> <select class="form-control"> <option>Mykad</option> <option>Mykid</option> <option>Passport</option> </select> </div></div><div class="col-md-6"> <div class="form-group"> <label>Identity Number</label> <input type="text" name="identity_number" class="form-control"/> </div></div></div></div><div class="modal-footer"> <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> </div></div></div></div>';

	// 				gtotal+=price;

	// 				passenger_number++;

	// 			}

	// 		}


			
	// 	}

	// 	$('.panel-return').html(html);

	// }	



	$('#total').html(gtotal.toFixed(2));

	$('input[name="subtotal"]').val(gtotal.toFixed(2));

	var discount_amount = parseFloat($('input[name="discount"]').val());

	var new_gtotal = gtotal - discount_amount;

	$('#gtotal').html(new_gtotal.toFixed(2));

	$('input[name="gtotal"]').val(new_gtotal.toFixed(2));

	disable_return_trips();

}

function disable_return_trips(){

	var attempttotal = 0;

	for(var i=0;i<$('.onward-passengers').length;i++){

		if($('.onward-passengers').eq(i).val() != '0'){

			var vesselseatcategoryid = $('.onward-passengers').eq(i).attr('data-seat-category-id');

			var onwardtripid = $('.onward-passengers').eq(i).attr('data-trip-id');

			attempttotal += parseInt($('.onward-passengers').eq(i).val());


		}


	}

	var tripid = $('.return_capacity_'+vesselseatcategoryid).attr('data-trip-id');

	// alert(tripid);

	if(parseInt($('.return_capacity_'+vesselseatcategoryid).val()) < attempttotal){

		$('#return-trip-'+tripid).prop('disabled',true);

	}
	else{

		$('#return-trip-'+tripid).prop('disabled',false);

	}

	for(var j=0;j<$('.return_seat_'+vesselseatcategoryid).length;j++){



	}

}
// $('.qtyplus').click(function(e){

function incrementvalue(elem){


    // Stop acting like a button
    // e.preventDefault();
    // Get the field name
    fieldName = elem.attr('field');

    // alert(fieldName);
    // Get its current value
    var currentVal = parseInt($('input[name='+fieldName+']').val());


    // If is not undefined
    if (!isNaN(currentVal)) {
        // Increment
        $('input[name='+fieldName+']').val(currentVal + 1);
    } else {
        // Otherwise put a 0 there
        $('input[name='+fieldName+']').val(0);
    }

    total_passenger(elem);

    check_passenger_selection(elem);
}
// This button will decrement the value till 0


// $(".qtyminus").click(function(e) {

function decrementvalue(elem){	
    // Stop acting like a button
    // e.preventDefault();
    // Get the field name
    fieldName = elem.attr('field');
    // Get its current value
    var currentVal = parseInt($('input[name='+fieldName+']').val());
    // If it isn't undefined or its greater than 0
    if (!isNaN(currentVal) && currentVal > 0) {
        // Decrement one
        $('input[name='+fieldName+']').val(currentVal - 1);
    } else {
        // Otherwise put a 0 there
        $('input[name='+fieldName+']').val(0);
    }

    total_passenger(elem);

    check_passenger_selection(elem);
}
