<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('settings', function (Blueprint $table) {
            $table->integer('branch_id');
            $table->string('weather_city',50);
            $table->string('weather_region',5);
            $table->text('smtp_host');
            $table->string('smtp_port',50);
            $table->text('smtp_from_email');
            $table->text('smtp_username');
            $table->text('smtp_password');
            $table->integer('new_booking_notification');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('settings');
    }
}
