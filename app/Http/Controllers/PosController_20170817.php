<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Jetty;

use App\Route;

use App\Trip;

use App\TripPrices;

use App\Vessel_Seat;

use App\Vessel;

use App\Booking;

class PosController extends Controller
{
    public function index(){

    	$origins = Jetty::orderBy('Jetty_Name')->get();

    	return view('pos.index',compact('origins'));

    }

    public function destination($id){

    	$destination_ids = Route::select('Route_To')->where('Route_From','=',$id)->get();

    	foreach($destination_ids as $destination_id){

    		$destinations = Jetty::where('id','=',$destination_id->Route_To)->get();

    	}

    	return view('pos.destination',compact('destinations'));

    }

    public function trips($from,$to,$type,$date){

        $depart_date = date_format(date_create($date),'Y-m-d');

        $route = Route::select('id')->where([['Route_From','=',$from],['Route_To','=',$to]])->first();

        $trips = Trip::orderBy('Depart_Time')->where([

            ['Route_Id', '=', $route->id],

            ['Depart_Date', '=', $depart_date]

        ])->get();


        $html = '<div class="col-md-8">
    <div class="form-group">
        <div class="text-right"><!-- 
            <label>Availability:-</label>  -->
            <span class="{{ $type }}-availability"></span>
        </div>
    </div>
</div>
<div class="col-md-12">
    <div class="btn-group btn-group-justified pos-grid-nav">';
        foreach($trips as $trip){

            $vessel = Trip::select('Vessel_Id')->where('id','=',$trip->id)->first();

            $vessel_seats = Vessel_Seat::select('id','seat_category_name','capacity')->where('vessel_id','=',$vessel->Vessel_Id)->get();

            foreach($vessel_seats as $vessel_seat){

                echo '<input type="hidden" id="'.$vessel_seat->id.'" value="'.$vessel_seat->capacity.'"/>';

            }

            $html .= '<div class="btn-group">
                <button data-time-text="'.date_format(date_create($trip->Depart_Time),'H:i').'" style="z-index:10002;" id="'.$type.'-trip-'.$trip->id.'" class="btn btn-default '.$type.'-trip-button " title="" type="button" rel="'.$trip->id.'" onclick="availability($(this),\''.$type.'\')">
                <b>'.date_format(date_create($trip->Depart_Time),'H:i').'</b>
                </button>
            </div>';
        }                                                                      
    $html .= '</div>
</div>';

        return view('pos.trips',compact('trips','type','html'));   
        
 
    }

    public function availability($tripid,$type){

        $vessel = Trip::select('Vessel_Id')->where('id','=',$tripid)->first();

        $vessel_seats = Vessel_Seat::select('id','seat_category_name','capacity')->where('vessel_id','=',$vessel->Vessel_Id)->get();



        // $bookings = Booking::where('selected_onward_trip_id','=',$tripid)->count();

        // $capacity = $vessel_seat->Capacity;

        // $seat_balance = $capacity - $bookings;

        $adult_prices = $child_prices = array();

        foreach($vessel_seats as $vessel_seat){

            $price_info = TripPrices::select('adult_price','child_price')->where([['vessel_seat_id','=',$vessel_seat->id],['trip_id','=',$tripid]])->first();

            $adult_prices[$tripid][$vessel_seat->id] = $price_info->adult_price;

            $child_prices[$tripid][$vessel_seat->id] = $price_info->child_price;

        }


        return view('pos.availability',compact('vessel_seats','tripid','adult_prices','child_prices','type')); 

    }

    public function passenger_details($total_adults,$total_children){

        $passenger_number = 1;

        return view('pos.passenger_details',compact('total_adults','total_children','passenger_number'));

    }


}
