<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Route;

use App\Jetty;

class RouteController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }     
    public function index(){

        $routes = new Route;

        if(request()->has('sort')){

            $routes = $routes->with('route_from','route_to')->orderBy('Route_Name',request('sort'));

        }

    	
        $routes = $routes->paginate(20)->appends([

            'sort' => request('sort')

        ]);

    	return view('routes.index',compact('routes'));
    }

    public function create(){

        $Jetty = new Jetty;

        $jetties = $Jetty->orderBy('Jetty_Name')->get();  

        return view('routes.create',compact('jetties'));

        // $this->validate(request() , {

        //  'Company_Code' => 'required',

        //  'Vessel_Code' => 'required'

        // });

        // $routes->create(request(['Company_Code','Vessel_Code']));

        // return redirect('/bookings');

    }

    public function store(){

        $origin = Jetty::select('Jetty_Code')->where('id','=',request('Route_From'))->first();

        $destination = Jetty::select('Jetty_Code')->where('id','=',request('Route_To'))->first();

        $this->validate(request(),[

            'Company_Code' => 'required|max:15',

            'Route_From' => 'required|max:100',

            'Route_To' => 'required|max:100'

        ]);

        $route = new Route;

        $route->Message_ID = '111';

        $route->Company_Code = request('Company_Code');

        $route->Route_Code = 'default';

        $route->Route_Name = 'default';

        $route->Route_From = request('Route_From');

        $route->Route_To = request('Route_To');

        $route->save();

        $route_id = $route->id;

       $route->where('id', $route_id)->update([

            'Route_Code' => $origin->Jetty_Code . $destination->Jetty_Code . $route_id,

            'Route_Name' => $origin->Jetty_Code . $destination->Jetty_Code . $route_id

            ]);        

        return redirect('routes');


    }

    public function show($id){

        $routes = new Route;

        $route = $routes->findOrFail($id);

        $Jetty = new Jetty;

        $jetties = $Jetty->orderBy('Jetty_Name')->get();          

        return view('routes.edit',compact('jetties','route'));
    }

    public function update($id){

        $routes = new Route;

        $this->validate(request(),[

            'Company_Code' => 'required|max:15',

            // 'Route_Code' => 'required|max:10',

            // 'Route_Name' => 'required|max:100',

            'Route_From' => 'required|max:100',

            'Route_To' => 'required|max:100'

        ]);

        $route = $routes->find($id);

        $route->Message_ID = '111';

        $route->Company_Code = request('Company_Code');

        // $route->Route_Code = request('Route_Code');

        // $route->Route_Name = request('Route_Name');

        $route->Route_From = request('Route_From');

        $route->Route_To = request('Route_To');

        $route->save();

        return redirect('routes');

    }    

    public function delete($id){

        $routes = new Route;

        $route = $routes->find($id);

        $route->delete();

        return redirect()->back();

    }
}
