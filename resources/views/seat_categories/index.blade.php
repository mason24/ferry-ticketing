@extends('layout.master')
    @section('content')
        @if(session()->has('message'))
            <div class="alert alert-success">
                {{ session()->get('message') }}
            </div>
        @endif

                        <div class="box pattern pattern-sandstone">
                            <div class="box-header">
                                <i class="icon-table"></i>
                                <h5>
                                    Seat Categories
                                </h5>

                            </div>
                            <div class="form-group">
                                <a href="seat-category/create">New Seat Category</a> 
                            </div>                            
                            <div class="form-group">
                                Sort: <a href="seat-category?sort=asc">Ascending</a> | <a href="seat-category?sort=desc">Descending</a> 
                            </div>
                            <div class="box-content box-table">
                                <table id="sample-table" class="table table-hover table-bordered tablesorter">
                                    <thead>
                                        <tr>
                                            <th>Seat Category Name</th>
                                            <th>Created Date</th>
                                            <th>Updated Date</th>
                                            <th class="td-actions"></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    	@foreach ($seat_categories as $seat_category)
	                                        <tr>
	                                            <td>{{ $seat_category->Seat_Category_Name }}</td>
	                                            <td>{{ $seat_category->created_at }}</td>
	                                            <td>{{ $seat_category->updated_at }}</td>
	                                            <td class="td-actions">
	                                                <a href="seat-category/edit/{{ $seat_category->id }}" class="btn btn-small btn-info">
	                                                    <i class="btn-icon-only icon-edit"></i>
	                                                </a>

	                                                <a href="seat-category/delete/{{ $seat_category->id }}" class="btn btn-small btn-danger">
	                                                    <i class="btn-icon-only icon-remove"></i>
	                                                </a>
	                                            </td>
	                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>

                                {{ $seat_categories->links() }}
                            </div>
                        </div>
@endsection