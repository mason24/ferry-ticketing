@extends('layout.master')
@section('content')
    @include('layout.errors')
    <form method="post" action="{{ URL::to('/routes') }}">
        {{ csrf_field() }}
        <div class="row">
            <div id="acct-password-row" class="span7">
                <fieldset>
                    <legend>New Route</legend><br>
                    <!-- <div class="control-group ">
                        <label for="Company_Code" class="control-label">Company Code <span class="required">*</span></label>
                        <div class="controls">
                            <input id="current-pass-control" name="Company_Code" class="span4" type="text" value=""/>

                        </div>
                    </div> -->
                    <input name="Company_Code" class="span4" type="hidden" value="DS"/>
                    <input name="Route_Code" class="span4" type="hidden" value=""/>
                    <input name="Route_Name" class="span4" type="hidden" value=""/>
                    
                    <div class="control-group ">
                        <label class="control-label">Route From <span class="required">*</span></label>
                        <div class="controls">
                            <select name="Route_From" class="chosen span4">
                                <option>--Choose--</option>
                                @foreach($jetties as $jetty)
                                    <option value="{{ $jetty->id }}">{{ $jetty->Jetty_Name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="control-group ">
                        <label class="control-label">Route To <span class="required">*</span></label>
                        <div class="controls">
                            <select name="Route_To" class="chosen span4">
                                <option>--Choose--</option>
                                @foreach($jetties as $jetty)
                                    <option value="{{ $jetty->id }}">{{ $jetty->Jetty_Name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>                                        
                </fieldset>
            </div>
        </div>
        <footer id="submit-actions" class="form-actions">
            <button id="submit-button" type="submit" class="btn btn-primary" value="">Create</button>
            <a href="{{ URL::to('/routes') }}" class="btn btn-default">Back</a>
        </footer>
    </form>
@endsection