@extends('layout')
@section('content')
                        <div class="box pattern pattern-sandstone">
                            <div class="box-header">
                                <i class="icon-table"></i>
                                <h5>
                                    Trips
                                </h5>
                            </div>
                            <div class="box-content box-table">
                            Sort: <a href="routes?sort=asc">Ascending</a> | <a href="routes?sort=desc">Descending</a>
                                <table id="sample-table" class="table table-hover table-bordered tablesorter">
                                    <thead>
                                        <tr>
                                            <th>Trip Code</th>
                                            <th>Trip Name</th>
                                            <th>Jetty Station</th>
                                            <th>Trip Date</th>
                                            <th>Created Date</th>
                                            <th>Updated Date</th>
                                            <th class="td-actions"></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    	@foreach ($trips as $trip)
	                                        <tr>
	                                            <td>{{ $trip->Trip_Code }}</td>
	                                            <td>{{ $trip->Trip_Name }}</td>
                                                <td>{{ $trip->Jetty_Station }}</td>
                                                <td>{{ $trip->Trip_Date->toFormattedDateString() }}</td>
	                                            <td>{{ $trip->created_at }}</td>
	                                            <td>{{ $trip->updated_at }}</td>
	                                            <td class="td-actions">
	                                                <a href="javascript:;" class="btn btn-small btn-info">
	                                                    <i class="btn-icon-only icon-edit"></i>
	                                                </a>

	                                                <a href="javascript:;" class="btn btn-small btn-danger">
	                                                    <i class="btn-icon-only icon-remove"></i>
	                                                </a>
	                                            </td>
	                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>

                                {{ $trips->links() }}
                            </div>
                        </div>
@endsection