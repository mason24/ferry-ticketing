<div class="col-md-8">
    <div class="form-group">
        <div class="text-right"><!-- 
            <label>Availability:-</label>  -->
            <span class="{{ $type }}-availability"></span>
        </div>
    </div>
</div>
<div class="col-md-12">
    <div class="btn-group btn-group-justified pos-grid-nav">
    	@foreach($trips as $trip)
        <div class="btn-group">
            <button data-time-text="{{ date_format(date_create($trip->Depart_Time),'H:i') }}" style="z-index:10002;" id="{{ $type }}-trip-{{ $trip->id }}" class="btn btn-default {{ $type }}-trip-button " title="" type="button" rel="{{ $trip->id }}" onclick="availability($(this),'{{ $type }}')">
            <b>{{ date_format(date_create($trip->Depart_Time),'H:i') }}</b>
            </button>
        </div>
        @endforeach                                                                      
    </div>
</div>