<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Passenger extends Model
{
    public function seat_category(){

    	return $this->belongsTo('App\SeatCategory','SeatCategoryId');

    }

    public function boarding_passes(){

    	return $this->hasMany('App\BoardingPass','passenger_id');

    }

    public function booking(){

    	return $this->belongsTo('App\Booking','booking_id');

    }       
}
