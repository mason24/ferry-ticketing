<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Customer;

use App\CustomerGroup;

use App\Country;

class CustomerController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }     
    public function index(){

        $customer = new Customer;

        if(request()->has('sort')){

            $customers = $customer->orderBy('name',request('sort'));

        }

    	
        $customers = $customer->paginate(20)->appends([

            'sort' => request('sort')

        ]);

    	return view('customers.index',compact('customers'));
    }

    public function create(){

        $customer_groups = new CustomerGroup;

        $country = new Country;

        $customer_groups = $customer_groups->orderBy('name')->get();

        $countries = $country->orderBy('name')->get();

        return view('customers.create',compact('customer_groups','countries'));

    }

    public function store(){

        $this->validate(request(),[

            'name' => 'required',

            'identity_type' => 'required|not_in:--Choose--',

            'identity_number' => 'required',

            'gender' => 'required|not_in:--Choose--',

            'country' => 'required|not_in:--Choose--',

            'address' => 'required',

            'state' => 'required',

            'postcode' => 'required',

            'email' => 'required',

            'contact_number' => 'required',

            'emergency_contact_number' => 'required'            

        ]);

        $customer = new Customer;

        $customer->name = request('name');

        $customer->id_type = request('identity_type');

        $customer->id_no = request('identity_number');

        $customer->dob = date_format(date_create(request('date_of_birth')),'Y-m-d');

        $customer->gender = request('gender');

        $customer->nationality = request('country');

        $customer->add1 = request('address');

        $customer->add2 = request('address_2');

        $customer->add3 = request('address_3');

        $customer->state = request('state');

        $customer->postcode = request('postcode');

        $customer->email = request('email');

        $customer->contact_no = request('contact_number');

        $customer->emergency_contact_no = request('emergency_contact_number');

        if(request('customer_group') != 0){

            $customer->customer_group_id = request('customer_group');

        }

        $customer->save();

        return redirect('customer');


    }

    public function show($id){

        $customer_groups = new CustomerGroup;

        $country = new Country;

        $customer = new Customer;

        $customer_groups = $customer_groups->orderBy('name')->get();

        $countries = $country->orderBy('name')->get();

        $customer_info = $customer->findOrFail($id);

        return view('customers.edit',compact('customer_info','countries','customer_groups'));
    }

    public function update($id){

        $customer = new Customer;

        $this->validate(request(),[

            'name' => 'required',

            'identity_type' => 'required|not_in:--Choose--',

            'identity_number' => 'required',

            'gender' => 'required|not_in:--Choose--',

            'country' => 'required|not_in:--Choose--',

            'address' => 'required',

            'state' => 'required',

            'postcode' => 'required',

            'email' => 'required',

            'contact_number' => 'required',

            'emergency_contact_number' => 'required'            

        ]);

        $customer = $customer->find($id);

        $customer->name = request('name');

        $customer->id_type = request('identity_type');

        $customer->id_no = request('identity_number');

        $customer->dob = date_format(date_create(request('date_of_birth')),'Y-m-d');

        $customer->gender = request('gender');

        $customer->nationality = request('country');

        $customer->add1 = request('address');

        $customer->add2 = request('address_2');

        $customer->add3 = request('address_3');

        $customer->state = request('state');

        $customer->postcode = request('postcode');

        $customer->email = request('email');

        $customer->contact_no = request('contact_number');

        $customer->emergency_contact_no = request('emergency_contact_number');

        if(request('customer_group') != 0){

            $customer->customer_group_id = request('customer_group');

        }

        $customer->save();

        return redirect('customer');

    }   

    public function delete($id){

        $user_groups = new User_Group;

        $user_group = $user_groups->find($id);

        $user_group->delete();

        return redirect()->back();

    }
}
