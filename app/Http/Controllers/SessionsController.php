<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;

use App\User_Group;

class SessionsController extends Controller
{

   /* public function __construct(){

        $this->middleware('guest');

    }*/
    public function create(){

        return view('login.index');
    	
    }

    public function store(){

        if(! auth()->attempt(request(['username','password']))){

            return back()->withErrors([

                'message' => 'Please check your credentials and try again.'

            ]);

        }
        if(request('system') == 'pos'){

            return redirect('pos');

        }
        else{

            return redirect('dashboard');

        }
        

    }

    public function destroy(){

    	auth()->logout();

    	return redirect('login');
    }

    public function users(){

        $user = new User;

        if(request()->has('sort')){

            $users = $user->orderBy('name',request('sort'));

        }

    	
        $users = $user->paginate(20)->appends([

            'sort' => request('sort')

        ]);

    	return view('users.index',compact('users'));
    }

    public function show($id){

        $users = new User;

        $user = $users->findOrFail($id);

        $user_groups = new User_Group;

    	$user_group_list = $user_groups->get();        

        return view('users.edit', ['user_groups'=> $user_group_list , 'user'=>$user]);
    }

    public function update($id){

        $jetties = new User;

        $this->validate(request(),[

            'Jetty_Name' => 'required',

            'Jetty_Code' => 'required'

        ]);

        $jetty = $jetties->find($id);

        $jetty->Jetty_Name = request('Jetty_Name');

        $jetty->Jetty_Code = request('Jetty_Code');

        $jetty->save();

        return redirect('jetties');

    }

    public function delete($id){

        $users = new User;

        $user = $users->find($id);

        $user->delete();

        return redirect()->back();

    }     
}
