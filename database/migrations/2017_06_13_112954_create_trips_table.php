<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTripsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trips', function (Blueprint $table) {
            $table->increments('id');
            $table->string('Message_ID',30);
            $table->string('Company_Code',15);
            $table->string('Trip_Code',10);
            $table->string('Trip_Name',100);
            $table->integer('Vessel_Id')->unsigned();
            $table->foreign('Vessel_Id')->references('id')->on('vessels')->onDelete('cascade');
            $table->integer('Route_Id')->unsigned();
            $table->foreign('Route_Id')->references('id')->on('routes')->onDelete('cascade');
            $table->date('Depart_Date');
            $table->time('Depart_Time');
            $table->date('Arvl_Date');
            $table->time('Arvl_Time');                                   
            $table->integer('status');                                   
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trips');
    }
}
