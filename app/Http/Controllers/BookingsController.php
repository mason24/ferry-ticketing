<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Jetty;

use App\Booking;

class BookingsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }     
    public function index(){

        $bookings = Booking::with('origin','destination')->paginate(20)->appends([

            'search' => request('search')

        ]);

    	return view('bookings.index',compact('bookings'));
    }

    public function create(){

        $jetty = new Jetty;

        $jetties = $jetty->orderBy('Jetty_Name')->get();          

        return view('bookings.create', [

            'jetties'=> $jetties

        ]);
    }    

    public function report(){

        $bookings = Booking::paginate(20)->appends([

            'search' => request('search')

        ]);
        return view('report.sales',compact('bookings')); 

    }
}
