<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TripPrices extends Model
{
    public function seat_category(){

    	return $this->belongsTo('App\SeatCategory','vessel_seat_id');

    }
}
