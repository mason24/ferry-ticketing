<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;

use App\User_Group;

class RegistrationController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }     
    public function create(){

        $user_groups = new User_Group;

    	$user_group_list = $user_groups->get();

        return view('sessions.create', ['user_groups'=> $user_group_list]);

    }

    public function store(){

        $this->validate(request(),[

            'name' => 'required',

            'email' => 'required|email',

            'username' => 'required',

            'password' => 'required|confirmed'

        ]);

        $user = new User;

        $user->name = request('name');

        $user->username = request('username');

        $user->email = request('email');

        $user->password = bcrypt(request('password'));

        $user->user_group_id = request('user_group_id');

        $user->save();

       	// auth()->login($user);

       	return redirect('users');

    }    
}
