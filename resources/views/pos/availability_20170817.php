@foreach($vessel_seats as $vessel_seat)
	<a class="btn btn-primary btn-sm" data-toggle="modal" data-target="#select_passengers_{{ $tripid }}_{{ $vessel_seat->id }}_{{ $type }}_modal"><i class="fa fa-user"></i></a> <label style="font-style:italic">{{ $vessel_seat->seat_category_name }}:</label> {{ $vessel_seat->capacity }} 

<div class="modal fade text-left" id="select_passengers_{{ $tripid }}_{{ $vessel_seat->id }}_{{ $type }}_modal" role="dialog">
	<div class="modal-dialog">

	  <input type="hidden" data-trip-id="{{ $tripid }}" data-vessel-seat-id="{{ $vessel_seat->id }}" class="available_vessel_seats {{ $type }}_vessel_seats vessel_seat_{{ $tripid }}_{{ $vessel_seat->id }} {{ $type }}_seat_id_{{ $vessel_seat->id }}" value="{{ $vessel_seat->capacity }}"/>
	  <div class="modal-content">
	    <div class="modal-header">
	      <button type="button" class="close" data-dismiss="modal">&times;</button>
	      <h4 class="modal-title">Select Passengers ({{ $vessel_seat->seat_category_name }}) <b style="color:green">{{ $vessel_seat->capacity }}</b></h4>
	    </div>
	    <div class="modal-body">
	      <div class="row">
	        <div class="col-md-6">
	            <div class="form-group">
	                <label>Total Adults</label>
	                <div>
	                    <input onclick="decrementvalue($(this))" type='button' value='-' class='qtyminus' field='total_adults_{{ $tripid }}_{{ $vessel_seat->id }}_{{ $type }}' data-trip-type="{{ $type }}" data-vessel-seat-id="{{ $vessel_seat->id }}" data-trip-id="{{ $tripid }}"/>

	                    <input type="text" data-passenger-type-text="Adult" data-vessel-seat-name="{{ $vessel_seat->seat_category_name }}" data-trip-type="{{ $type }}" data-vessel-seat-id="{{ $vessel_seat->id }}" data-price="{{ $adult_prices[$tripid][$vessel_seat->id] }}" data-seat-balance="{{ $vessel_seat->capacity }}" class="passenger_{{ $tripid }}_{{ $vessel_seat->id }} total_adults_{{ $tripid }}_{{ $vessel_seat->id }} qty {{ $type }}-passengers passenger_qty vessel_seat_{{ $vessel_seat->id }} {{ $type }}_adults" name="total_adults_{{ $tripid }}_{{ $vessel_seat->id }}_{{ $type }}" value="0" readonly/>

	                    <input onclick="incrementvalue($(this))" type='button' value='+' class='qtyplus' field='total_adults_{{ $tripid }}_{{ $vessel_seat->id }}_{{ $type }}' data-trip-type="{{ $type }}" data-vessel-seat-id="{{ $vessel_seat->id }}" data-trip-id="{{ $tripid }}"/>
	                </div>
	            </div>
	        </div>
	        <div class="col-md-6">
	            <div class="form-group">
	                <label>Total Children</label>
	                <div>
	                    <input onclick="decrementvalue($(this))" type='button' value='-' class='qtyminus' field='total_children_{{ $tripid }}_{{ $vessel_seat->id }}_{{ $type }}' data-trip-type="{{ $type }}" data-vessel-seat-id="{{ $vessel_seat->id }}" data-trip-id="{{ $tripid }}"/>

	                    <input type="text" data-passenger-type-text="Child" data-vessel-seat-name="{{ $vessel_seat->seat_category_name }}" data-trip-type="{{ $type }}" data-vessel-seat-id="{{ $vessel_seat->id }}" data-price="{{ $child_prices[$tripid][$vessel_seat->id] }}" data-vessel-seat-id="{{ $vessel_seat->id }}" data-seat-balance="{{ $vessel_seat->capacity }}" class="passenger_{{ $tripid }}_{{ $vessel_seat->id }} total_children_{{ $tripid }}_{{ $vessel_seat->id }} qty {{ $type }}-passengers passenger_qty vessel_seat_{{ $vessel_seat->id }} {{ $type }}_children" name="total_children_{{ $tripid }}_{{ $vessel_seat->id }}_{{ $type }}" value="0" readonly/>

	                    <input onclick="incrementvalue($(this))" type='button' value='+' class='qtyplus' field='total_children_{{ $tripid }}_{{ $vessel_seat->id }}_{{ $type }}' data-trip-type="{{ $type }}" data-vessel-seat-id="{{ $vessel_seat->id }}" data-trip-id="{{ $tripid }}"/>
	                </div>
	            </div>
	        </div>            
	      </div>                                                                                                                               
	    </div>
	    <div class="modal-footer">
	      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	    </div>
	  </div>
	  
	</div>
</div>	
@endforeach