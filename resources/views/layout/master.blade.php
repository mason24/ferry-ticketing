<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Ferry Ticketing</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <meta name="layout" content="main"/>
    
    <!-- <script type="text/javascript" src="http://www.google.com/jsapi"></script> -->
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script src="{{ asset('js/jquery/jquery-1.8.2.min.js') }}" type="text/javascript" ></script>
    <script src="{{ asset('js/jquery/jquery.simpleWeather.min.js') }}" type="text/javascript" ></script>
    <script src="{{ asset('js/color-picker/js/bootstrap-colorpicker.min.js') }}" type="text/javascript" ></script>
    <link href="{{ asset('css/customize-template.css') }}" type="text/css" media="screen, projection" rel="stylesheet" />
    <link href="{{ asset('js/color-picker/css/bootstrap-colorpicker.min.css') }}" type="text/css" rel="stylesheet" />

    <style>
    </style>
</head>
    <body>
        <div class="navbar navbar-fixed-top">
            <div class="navbar-inner">
                <div class="container">
                    <!-- <button class="btn btn-navbar" data-toggle="collapse" data-target="#app-nav-top-bar">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button> -->
                    <a href="{{ URL::to('/dashboard') }}" class="brand"><i class="icon-cog"> Ferry Ticketing</i></a>
                    <div id="app-nav-top-bar" class="nav-collapse">
                        <!-- <ul class="nav">
                            
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">TRY ME!
                                        <b class="caret hidden-phone"></b>
                                    </a>
                                    <ul class="dropdown-menu">
                                        <li>
                                            <a href="dashboard.html">Dashboard</a>
                                        </li>
                                        <li>
                                            <a href="form.html">Form</a>
                                        </li>
                                        <li>
                                            <a href="custom-view.html">Custom View</a>
                                        </li>
                                        <li>
                                            <a href="login.html">Login Page</a>
                                        </li>
                                    </ul>
                                </li>
                                
                            
                        </ul> -->
                        @if(Auth::check())
                        <ul class="nav pull-right">
                            
                             <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">{{ Auth::user()->username }}
                                    <b class="caret hidden-phone"></b>
                                </a>
                                <ul class="dropdown-menu">
                                    <li>
                                        <a href="{{ URL::to('/logout') }}">Log Out</a>
                                    </li>
                                    
                                </ul>
                            </li> 
                        </ul>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <div id="body-container">
            <div id="body-content">
                @include('layout.nav')
                <section class="nav nav-page">
                        <div class="container">
                            <div class="row">
                                <div class="span12">
                                    <header class="page-header">
                                        <h3>
                                           <!--  <small>
                                            [ <a href="report">Daily Sales Report</a> ] 
                                            [ <a href="report">Customize Report</a> ] 
                                            [ <a href="report">Register Report</a> ] 
                                            [ <a href="report">Passenger Manifest</a> ] 
                                            [ <a href="report">System Log</a> ] 
                                            </small> -->
                                        </h3>
                                    </header>
                                </div>
                                <!-- <div class="page-nav-options">
                                    <div class="span9">
                                        <ul class="nav nav-pills">
                                            <li>
                                                <a href="#"><i class="icon-home icon-large"></i></a>
                                            </li>
                                        </ul>
                                    
                                    </div>
                                </div> -->
                            </div>
                        </div>
                    </section>                            
                <div class='container'>
                    @yield('content')
                </div>

            </div>
        </div>
        <script>
            $(function () {
              $('.html_color_hex').colorpicker();
            });
        </script>
        <div id="spinner" class="spinner" style="display:none;">
            Loading&hellip;
        </div>

        @include('layout.footer')
        

    </body>
</html>