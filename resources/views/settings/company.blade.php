@extends('layout.master')
@section('content')
    @include('layout.errors')
    <form method="post" action="{{ URL::to('/settings/company') }}">
        {{ csrf_field() }}
        <div class="row">
            <div id="acct-password-row" class="span7">
            @foreach($company_info as $company)
                <fieldset>
                    <legend>Company Settings</legend><br>
                    <div class="control-group ">
                        <label for="Vessel_Code" class="control-label">Company Name <span class="required">*</span></label>
                        <div class="controls">
                            <input id="current-pass-control" name="company_name" class="span4" type="text" value="{{ $company->Company_Name }}"/>

                        </div>
                    </div>                                        
                    <div class="control-group ">
                        <label for="Vessel_Name" class="control-label">Company Code <span class="required">*</span></label>
                        <div class="controls">
                            <input id="current-pass-control" name="company_code" class="span4" type="text" value="{{ $company->Company_Code }}"/>

                        </div>
                    </div> 
                    <div class="control-group ">
                        <label for="Vessel_Name" class="control-label">Company Address Line 1 <span class="required">*</span></label>
                        <div class="controls">
                            <input id="current-pass-control" name="company_address" class="span4" type="text" value="{{ $company->Company_Address1 }}"/>

                        </div>
                    </div>
                    <div class="control-group ">
                        <label for="Vessel_Name" class="control-label">Company Address Line 2 <span class="required">*</span></label>
                        <div class="controls">
                            <input id="current-pass-control" name="company_address_line_2" class="span4" type="text" value="{{ $company->Company_Address2 }}"/>

                        </div>
                    </div>
                    <div class="control-group ">
                        <label for="Vessel_Name" class="control-label">Company Address Line 3 <span class="required">*</span></label>
                        <div class="controls">
                            <input id="current-pass-control" name="company_address_line_3" class="span4" type="text" value="{{ $company->Company_Address3 }}"/>

                        </div>
                    </div>
                    <div class="control-group ">
                        <label for="Vessel_Name" class="control-label">Postcode <span class="required">*</span></label>
                        <div class="controls">
                            <input id="current-pass-control" name="postcode" class="span4" type="text" value="{{ $company->Postcode }}"/>

                        </div>
                    </div>
                    <div class="control-group ">
                        <label for="Vessel_Name" class="control-label">State <span class="required">*</span></label>
                        <div class="controls">
                            <input id="current-pass-control" name="state" class="span4" type="text" value="{{ $company->State }}"/>

                        </div>
                    </div>
                    <div class="control-group ">
                        <label for="Vessel_Name" class="control-label">Contact Person <span class="required">*</span></label>
                        <div class="controls">
                            <input id="current-pass-control" name="contact_person" class="span4" type="text" value="{{ $company->Contact_Person }}"/>

                        </div>
                    </div>
                    <div class="control-group ">
                        <label for="Vessel_Name" class="control-label">Contact Number <span class="required">*</span></label>
                        <div class="controls">
                            <input id="current-pass-control" name="contact_number" class="span4" type="text" value="{{ $company->Contact_No }}"/>

                        </div>
                    </div>
                    <div class="control-group ">
                        <label for="Vessel_Name" class="control-label">Email Address <span class="required">*</span></label>
                        <div class="controls">
                            <input id="current-pass-control" name="email_address" class="span4" type="text" value="{{ $company->Email_Address }}"/>

                        </div>
                    </div>
                    <div class="control-group ">
                        <label for="Vessel_Name" class="control-label">Status <span class="required">*</span></label>
                        <div class="controls">
                            <select class="chosen span2" name="status">
                                    <option value="1" {{ ($company->Active == 1 ? 'selected' : '') }}>Active</option>
                                    <option value="0" {{ ($company->Active == 0 ? 'selected' : '') }}>Inactive</option>
                            </select>
                        </div>
                    </div>                                                                                                                                      
                </fieldset>  
                @endforeach             
            </div>
        </div>
        <footer id="submit-actions" class="form-actions">
            <button id="submit-button" type="submit" class="btn btn-primary" value="">Save Changes</button>
        </footer>
    </form>
@endsection