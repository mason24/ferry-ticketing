<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Ferry Ticketing POS</title>

    <!-- Bootstrap Core CSS -->
    <link href="{{ asset('css/pos/bootstrap.min.css') }}" rel="stylesheet">

    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

    <!-- Custom CSS -->
    <link href="{{ asset('css/pos/sb-admin.css') }}" rel="stylesheet">
    <link href="{{ asset('css/pos/posajax.css') }}" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="{{ asset('css/pos/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css">

    <script src="{{ asset('js/pos/jquery.js') }}"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.4/angular.min.js"></script>
   

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="{{ URL::to('/pos') }}">Ferry Ticketing POS</a>
            </div>
            <!-- Top Menu Items -->
            <ul class="nav navbar-right top-nav">
                           
                <li class="dropdown">
                    <a href="#" id="register-details-btn" onclick="registerDetails()" class="btn-primary" style="color:#fefefe" data-toggle="modal" data-target="#cashRegisterModal">
                            <i class="fa fa-list"></i> Cash Register Details
                        </a>
                </li>
                <li class="dropdown">
                    <a href="#" class="btn-danger" onclick="closeRegister()"  style="color:#fefefe" data-toggle="modal" data-target="#closeRegisterModal">
                            <i class="fa fa-times-circle"></i> Close Register
                        </a>
                </li>
                @if(Auth::check())
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> {{ Auth::user()->username }} </a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="{{ URL::to('/logout') }}"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                        </li>
                    </ul>
                </li>
                @endif                

            </ul>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
        </nav>
        @if($pos_register_info === false)
            <div class="modal fade" id="open_register_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <form action="{{ URL::to('/pos/open') }}" method="post">
                    {{ csrf_field() }}
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title" id="myModalLabel">Open Register</h4>
                            </div>
                            <div class="modal-body">
                                                         
                                <div class="row">
                                    <div class="col-lg-12">
                                        <label>Cash in hand</label> (eg. 1000.50)
                                        <input class="form-control" name="cash_in_hand" autocomplete="off" />
                                    </div>
                                </div>                      
                            </div>
                            <div class="modal-footer">
                                <input type="hidden" name="key" value='{{ base64_encode("//{$_SERVER["HTTP_HOST"]}{$_SERVER["REQUEST_URI"]}") }}'/>
                                
                                <input class="btn btn-primary" type="submit" name="open_register" value="Open Register"/> 
                                <a href="{{ URL::to('/logout') }}" class="btn btn-default">Log Out</a>
                            </div>
                        </div>
                        <!-- /.modal-content -->
                    </div>
                </form>
            </div>  
            <script>
                $(document).ready(function(){
                    $('#open_register_modal').modal({backdrop: 'static', keyboard: false});
                }); 
            </script>
        @endif
        <div id="page-wrapper">

            <div class="container-fluid">

                <div class="modal fade" id="cashRegisterModal" role="dialog">
                    <div class="modal-dialog">
                    
                      <!-- Modal content-->
                        <div class="modal-content cash_register_details">

                        </div>
                      
                    </div>
                  </div> 
                <div class="modal fade" id="closeRegisterModal" role="dialog">
                    <div class="modal-dialog">
                    
                      <!-- Modal content-->
                        <form action="{{ URL::to('/pos/close') }}" method="post" target="_blank" id="formCloseRegister">
                            {{ csrf_field() }}
                            <div class="modal-content close_register_details">

                            </div>
                        </form>
                      
                    </div>
                  </div>                             
                <!-- /.row -->
                @yield('content')    
            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    

    

    <!-- Bootstrap Core JavaScript -->
    <script src="{{ asset('js/pos/bootstrap.min.js') }}"></script>

    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

  <script>
    var d = new Date();

    var month = d.getMonth()+1;
    var day = d.getDate();

    var date = (day<10 ? '0' : '') + day+'-'+(month<10 ? '0' : '') + month+'-'+d.getFullYear();


    $( function() {
        

        $( ".datepicker" ).datepicker({

            defaultDate: "+1w",

            minDate: 0,

            firstDay: 0,

            dateFormat: 'dd-mm-yy',

            changeMonth: true,

            showButtonPanel: true,

            numberOfMonths: 2,

            onClose: function( selectedDate ) {      

                var minDate = $(this).datepicker('getDate');

                var newMin = new Date(minDate.setDate(minDate.getDate() + 0));

                $( "#booking-to" ).datepicker( "option", "minDate", newMin );

            }

        });        
    });

    
  </script>     
  <style>
    .ui-datepicker{
    z-index: 999 !important;
    }

    .qty {
        width: 40px;
        height: 25px;
        text-align: center;
    }
    input.qtyplus { width:25px; height:25px;}
    input.qtyminus { width:25px; height:25px;}
</style>  
</body>

</html>
