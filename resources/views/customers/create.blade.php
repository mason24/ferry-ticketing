@extends('layout.master')
@section('content')
    @include('layout.errors')
    <form method="post" action="{{ URL::to('/customer/new') }}">
        {{ csrf_field() }}
        <div class="row">
            <div id="acct-password-row" class="span7">
                <fieldset>
                    <legend>New Customer</legend><br>
                    <div class="control-group ">
                        <label for="title" class="control-label">Name <span class="required">*</span></label>
                        <div class="controls">
                            <input id="current-pass-control" name="name" class="span4" type="text" required/>

                        </div>
                    </div>
                    <div class="control-group ">
                        <label for="title" class="control-label">Identity Type <span class="required">*</span></label>
                        <div class="controls">
                            <select name="identity_type" class="chosen span4">
                                <option>--Choose--</option>
                                <option value="ic">IC/MyKad/MyKid</option>
                                <option value="passport">Passport</option>
                            </select>

                        </div>
                    </div>
                    <div class="control-group ">
                        <label for="title" class="control-label">Identity No. <span class="required">*</span></label>
                        <div class="controls">
                            <input id="current-pass-control" name="identity_number" class="span4" type="text" required/>

                        </div>
                    </div>                    
                                                    
                    <div class="control-group ">
                        <label for="title" class="control-label">Date of Birth <span class="required">*</span></label>
                        <div class="controls">
                            <div class="input-prepend date datepicker">
                                <span class="add-on"><i class="icon-th"></i></span>
                                <input name="date_of_birth" class="span4" type="text" value="{{ date('m/d/Y') }}" readonly="readonly" style="cursor:pointer"/>
                            </div> 
                        </div>
                    </div>
                    <div class="control-group ">
                        <label for="title" class="control-label">Gender <span class="required">*</span></label>
                        <div class="controls">
                            <select name="gender" class="chosen span4">
                                <option>--Choose--</option>
                                <option value="M">Male</option>
                                <option value="F">Female</option>
                                <option value="U">Unknown</option>
                            </select>

                        </div>
                    </div>
                    <div class="control-group ">
                        <label for="title" class="control-label">Country <span class="required">*</span></label>
                        <div class="controls">
                            <select class="chosen span4" name="country">
                                <option>--Choose--</option>
                                @foreach($countries as $country)
                                    <option value="{{ $country->id }}">{{ $country->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="control-group ">
                        <label for="title" class="control-label">Address <span class="required">*</span></label>
                        <div class="controls">
                            <input type="text" name="address" class="span4" required/>

                        </div>
                    </div>
                    <div class="control-group ">
                        <label for="title" class="control-label">Address (Line 2)</label>
                        <div class="controls">
                            <input type="text" name="address_2" class="span4"/>

                        </div>
                    </div>
                    <div class="control-group ">
                        <label for="title" class="control-label">Address (Line 3)</label>
                        <div class="controls">
                            <input type="text" name="address_3" class="span4"/>

                        </div>
                    </div>
                    <div class="control-group ">
                        <label for="title" class="control-label">State <span class="required">*</span></label>
                        <div class="controls">
                            <input type="text" name="state" class="span4" required/>

                        </div>
                    </div>
                    <div class="control-group ">
                        <label for="title" class="control-label">Postcode <span class="required">*</span></label>
                        <div class="controls">
                            <input type="text" name="postcode" class="span4" required/>

                        </div>
                    </div> 
                    <div class="control-group ">
                        <label for="title" class="control-label">Email Address</label>
                        <div class="controls">
                            <input type="text" name="email" class="span4"/>

                        </div>
                    </div>
                    <div class="control-group ">
                        <label for="title" class="control-label">Contact Number <span class="required">*</span></label>
                        <div class="controls">
                            <input type="text" name="contact_number" class="span4" required/>

                        </div>
                    </div>                                
                    <div class="control-group ">
                        <label for="title" class="control-label">Emergency Contact Number <span class="required">*</span></label>
                        <div class="controls">
                            <input type="text" name="emergency_contact_number" class="span4"/>

                        </div>
                    </div>
                    <div class="control-group ">
                        <label for="title" class="control-label">Assign to Customer Group</label>
                        <div class="controls">
                            <select class="chosen span4" name="customer_group">
                                <option value="0">--Choose--</option>
                                @foreach($customer_groups as $customer_group)
                                    <option value="{{ $customer_group->id }}">{{ $customer_group->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>                    
                </fieldset>
            </div>
        </div>
        <footer id="submit-actions" class="form-actions">
            <button id="submit-button" type="submit" class="btn btn-primary" value="">Create</button>
            <a href="{{ URL::to('/customer') }}" class="btn btn-default">Back</a>
        </footer>
    </form>
@endsection