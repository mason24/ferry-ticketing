@foreach($prices as $price)

<div class="panel panel-default">
    <div class="panel-heading">
        <label>{{ $price->seat_category->Seat_Category_Name }}</label>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label>Total Adults</label>
                    <div>
                        <input onclick="decrementvalue($(this))" type='button' value='-' class='qtyminus' field="total_adults_{{ $price->id }}"/>

                        <input class="qty adults" data-price="{{ $price->adult_price }}" data-seat-category-id="{{ $price->id }}" data-seat-category-name="{{ $price->seat_category->Seat_Category_Name }}" type="text" data-passenger-type-text="Adult" name="total_adults_{{ $price->id }}" value="0" readonly/>

                        <input onclick="incrementvalue($(this))" type='button' value='+' class='qtyplus' field="total_adults_{{ $price->id }}"/>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label>Total Children</label>
                    <div>
                        <input onclick="decrementvalue($(this))" type='button' value='-' class='qtyminus' field="total_children_{{ $price->id }}"/>

                        <input class="qty children" data-price="{{ $price->child_price }}" data-seat-category-id="{{ $price->id }}" data-seat-category-name="{{ $price->seat_category->Seat_Category_Name }}" type="text" data-passenger-type-text="Adult" name="total_children_{{ $price->id }}" value="0" readonly/>

                        <input onclick="incrementvalue($(this))" type='button' value='+' class='qtyplus' field="total_children_{{ $price->id }}"/>
                    </div>
                </div>
            </div>            
        </div>
    </div>
</div>    
<input type="hidden" class="adult_citizen_price_{{ $price->seat_category_id }}" value="{{ $price->adult_citizen_price }}"/>
<input type="hidden" class="child_citizen_price_{{ $price->seat_category_id }}" value="{{ $price->child_citizen_price }}"/>
@endforeach