@extends('layout.master')
@section('content')
    @include('layout.errors')
    <form method="post" action="{{ URL::to('/user_groups') }}">
        {{ csrf_field() }}
        <div class="row">
            <div id="acct-password-row" class="span7">
                <fieldset>
                    <legend>New User Group</legend><br>
                    <div class="control-group ">
                        <label for="title" class="control-label">User Group Title <span class="required">*</span></label>
                        <div class="controls">
                            <input id="current-pass-control" name="title" class="span4" type="text" value=""/>

                        </div>
                    </div>                                       
                </fieldset>
            </div>
        </div>
        <footer id="submit-actions" class="form-actions">
            <button id="submit-button" type="submit" class="btn btn-primary" value="">Create</button>
            <a href="{{ URL::to('/user_groups') }}" class="btn btn-default">Back</a>
        </footer>
    </form>
@endsection