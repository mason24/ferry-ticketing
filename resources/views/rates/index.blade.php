@extends('layout.master')
@section('content')
                        <div class="box pattern pattern-sandstone">
                            <div class="box-header">
                                <i class="icon-table"></i>
                                <h5>
                                    Rates
                                </h5>

                            </div>
                            <div class="form-group">
                                <a href="rates/create">New Rate</a> 
                            </div>                            
                            <div class="form-group">
                                Sort: <a href="rates?sort=asc">Ascending</a> | <a href="rates?sort=desc">Descending</a> 
                            </div>
                            <div class="box-content box-table">
                                <table id="sample-table" class="table table-hover table-bordered tablesorter">
                                    <thead>
                                        <tr>
                                            <th>Rate Title</th>
                                            <th>Rate Type</th>
                                            <th>Created Date</th>
                                            <th>Updated Date</th>
                                            <th class="td-actions"></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    	@foreach ($rates as $rate)
	                                        <tr>
	                                            <td>{{ $rate->Rate_Title }}</td>
	                                            <td>{{ $rate->Rate_Type }}</td>
	                                            <td>{{ $rate->created_at }}</td>
	                                            <td>{{ $rate->updated_at }}</td>
	                                            <td class="td-actions">
	                                                <a href="rates/edit/{{ $rate->id }}" class="btn btn-small btn-info">
	                                                    <i class="btn-icon-only icon-edit"></i>
	                                                </a>

	                                                <a href="rates/delete/{{ $rate->id }}" class="btn btn-small btn-danger">
	                                                    <i class="btn-icon-only icon-remove"></i>
	                                                </a>
	                                            </td>
	                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>

                                {{ $rates->links() }}
                            </div>
                        </div>
@endsection