@extends('layout.master')
@section('content')
    @include('layout.errors')
    <form method="post" action="{{ URL::to('/open-ticket/new') }}">
        {{ csrf_field() }}
        <div class="row">
            <div id="acct-password-row" class="span7">
                <fieldset>
                    <legend>New Open Ticket</legend><br>
                    <div class="control-group ">
                        <label for="Trip_Code" class="control-label">From <span class="required">*</span></label>
                        <div class="controls">
                            <select name="origin" class="chosen span4">
                                <option>--Choose--</option>
                                @foreach($jetties as $jetty)
                                    <option value="{{ $jetty->id }}">{{ $jetty->Jetty_Name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="control-group ">
                        <label class="control-label">To <span class="required">*</span></label>
                        <div class="controls">
                            <select name="destination" class="chosen span4">
                                <option>--Choose--</option>
                                @foreach($jetties as $jetty)
                                    <option value="{{ $jetty->id }}">{{ $jetty->Jetty_Name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="control-group ">
                        <label class="control-label">Valid Till <span class="required">*</span></label>
                        <div class="controls">
                            <div class="input-prepend date datepicker">
                                <span class="add-on"><i class="icon-th"></i></span>
                                <input name="validity" class="span4" type="text" value="{{ date('m/d/Y') }}" readonly="readonly" style="cursor:pointer"/>
                            </div>

                        </div>
                    </div>                    
                    <div class="control-group ">
                        <label class="control-label">No. Of Adults</label>
                        <div class="controls">
                            <select name="adults" class="chosen span4" data-placeholder="Select" onchange="vesselSeats($(this).val())">
                                
                            @for($i=1;$i<=200;$i++)
                                <option value="{{ $i }}">{{ $i }}</option>
                            @endfor
                            </select>
                        </div>
                    </div>
                    <div class="control-group ">
                        <label class="control-label">No. Of Children</label>
                        <div class="controls">
                            <select name="children" class="chosen span4" data-placeholder="Select" onchange="vesselSeats($(this).val())">
                                
                            @for($i=0;$i<=200;$i++)
                                <option value="{{ $i }}">{{ $i }}</option>
                            @endfor
                            </select>
                        </div>
                    </div>
                    <div class="control-group ">
                        <label class="control-label">Open Ticket Code <span class="required">*</span></label>
                        <div class="controls">
                            <div class="input-prepend">
                                <input class="span4 open_ticket_code" name="open_ticket_code" type="text" readonly="readonly">
                                <span class="add-on" style="cursor:pointer" onclick="generateCode()">Generate</span>
                            </div>
                        </div>
                    </div>                                                                                                
                </fieldset>
            </div>
        </div>
        <footer id="submit-actions" class="form-actions">
            <button id="submit-button" type="submit" class="btn btn-primary" value="">Create</button>
            <a href="{{ URL::to('/open-ticket') }}" class="btn btn-default">Back</a>
        </footer>
    </form>  
<script>
function generateCode(){

    var url = "{{ URL::to('/open-ticket/code') }}";

    $.ajax({
        url: url,
        success: function (data) {
            $('.open_ticket_code').val(data);
        },
        error: function (data) {
            console.log('Error:', data);
        }
    }); 
 

}
</script>      
@endsection