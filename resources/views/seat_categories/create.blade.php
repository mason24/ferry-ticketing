@extends('layout.master')
@section('content')
    @include('layout.errors')
    <form method="post" action="{{ URL::to('/seat-categories/new') }}">
        {{ csrf_field() }}
        <div class="row">
            <div id="acct-password-row" class="span7">
                <fieldset>
                    <legend>New Seat Category</legend><br>
                    <div class="control-group ">
                        <label for="Jetty_Name" class="control-label">Seat Category Name <span class="required">*</span></label>
                        <div class="controls">
                            <input id="current-pass-control" name="Seat_Category_Name" class="span4" type="text" value=""/>

                        </div>
                    </div>
                                           
                </fieldset>
            </div>
        </div>
        <footer id="submit-actions" class="form-actions">
            <button id="submit-button" type="submit" class="btn btn-primary" value="">Create</button>
            <a href="{{ URL::to('/seat-categories') }}" class="btn btn-default">Back</a>
        </footer>
    </form>
@endsection