@extends('layout.master')
@section('content')
                        <div class="box pattern pattern-sandstone">
                            <div class="box-header">
                                <i class="icon-table"></i>
                                <h5>
                                    Customer Groups
                                </h5>

                            </div>
                            <div class="form-group">
                                <a href="group/new">New Customer Group</a> 
                            </div>                            
                            <div class="form-group">
                                Sort: <a href="group?sort=asc">Ascending</a> | <a href="group?sort=desc">Descending</a> 
                            </div>
                            <div class="box-content box-table">
                                <table id="sample-table" class="table table-hover table-bordered tablesorter">
                                    <thead>
                                        <tr>
                                            <th>Customer Group Name</th>
                                            <th>ID</th>
                                            <th>Created Date</th>
                                            <th>Updated Date</th>
                                            <th class="td-actions"></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    	@foreach ($customer_groups as $customer_group)
	                                        <tr>
                                                <td>{{ $customer_group->name }}</td>
	                                            <td>{{ $customer_group->id }}</td>
	                                            <td>{{ $customer_group->created_at }}</td>
	                                            <td>{{ $customer_group->updated_at }}</td>
	                                            <td class="td-actions">
	                                                <a href="group/edit/{{ $customer_group->id }}" class="btn btn-small btn-info">
	                                                    <i class="btn-icon-only icon-edit"></i>
	                                                </a>

	                                                <a href="group/delete/{{ $customer_group->id }}" class="btn btn-small btn-danger">
	                                                    <i class="btn-icon-only icon-remove"></i>
	                                                </a>
	                                            </td>
	                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>

                                {{ $customer_groups->links() }}
                            </div>
                        </div>
@endsection