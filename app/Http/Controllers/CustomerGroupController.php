<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\CustomerGroup;

use App\Customer;

class CustomerGroupController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }     
    public function index(){

        $customer_groups = new CustomerGroup;

        if(request()->has('sort')){

            $customer_groups = $customer_groups->orderBy('name',request('sort'));

        }

    	
        $customer_groups = $customer_groups->paginate(20)->appends([

            'sort' => request('sort')

        ]);

    	return view('customer_groups.index',compact('customer_groups'));
    }

    public function create(){

        return view('customer_groups.create');

    }

    public function store(Request $request){

        $this->validate(request(),[

            'name' => 'required',

            'imported_file'   => 'mimes:csv,txt',

        ]);     

        $customer_group = new CustomerGroup;

        $customer_group->name = request('name');

        $customer_group->description = request('description');

        $customer_group->save();

        $customer_group_id = $customer_group->id;

        if($request->file('imported_file'))
        {

            $path = $request->file('imported_file')->getRealPath();

            $data = \Excel::load($path, function($reader) {

            })->get();

            if(!empty($data) && $data->count()){

                $data = $data->toArray();

                foreach($data as $field){

                    Customer::insert([
                        [
                            'name' => $field['full_name'],

                            'id_type' => 'ic',

                            'id_no' => $field['nric'],

                            'dob' => $field['date_of_birth'],

                            'gender' => $field['gender'],

                            'nationality' => $field['nationality'],

                            'add1' => $field['address_line_1'],

                            'add2' => $field['address_line_2'],

                            'add3' => $field['address_line_3'],

                            'state' => $field['state'],

                            'postcode' => $field['postcode'],

                            'email' => $field['email_address'],

                            'contact_no' => $field['contact_number'],

                            'emergency_contact_no' => $field['emergency_contact_number'],

                            'customer_group_id' => $customer_group_id,

                            'created_at' => \Carbon\Carbon::now(),

                            'updated_at' => \Carbon\Carbon::now()
                        ]
                    ]);                     

                    // echo $field['full_name'].'<br>';
                    
                    // $customer->name = $field['full_name'];

                    // $customer->id_type = 'ic';

                    // $customer->id_no = $field['nric'];

                    // $customer->dob = date_format(date_create($field['date_of_birth']),'Y-m-d');

                    // $customer->gender = $field['gender'];

                    // $customer->nationality = $field['nationality'];

                    // $customer->add1 = $field['address_line_1'];

                    // $customer->add2 = $field['address_line_2'];

                    // $customer->add3 = $field['address_line_3'];

                    // $customer->state = $field['state'];

                    // $customer->postcode = $field['postcode'];

                    // $customer->email = $field['email_address'];

                    // $customer->contact_no = $field['contact_number'];

                    // $customer->emergency_contact_no = $field['emergency_contact_number'];

                    // $customer->customer_group_id = $customer_group_id;

                    // $customer->save();
                }
            }
        } 

        // echo '<pre>';
        // print_r($data);
        // echo '</pre>';     

        return redirect('group');


    }

    public function show($id){

        $CustomerGroup = new CustomerGroup;

        $group = $CustomerGroup->findOrFail($id);

        return view('customer_groups.edit',compact('group'));
    }

    public function update($id){

        $CustomerGroup = new CustomerGroup;

        $this->validate(request(),[

            'name' => 'required',

            'imported_file'   => 'mimes:csv,txt',

        ]);  

        $CustomerGroup = $CustomerGroup->find($id);

        $CustomerGroup->name = request('name');      

        $CustomerGroup->save();

        return redirect('group');

    }   

    public function delete($id){

        $CustomerGroup = new CustomerGroup;

        $CustomerGroup = $CustomerGroup->find($id);

        $CustomerGroup->delete();

        return redirect()->back();

    }
}
