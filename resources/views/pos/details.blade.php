<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h4 class="modal-title" id="myModalLabel">Sales ({{ $info->opened_at }} - {{ date('Y-m-d H:i:s') }})</h4>
</div>
<div class="modal-body">
    <div class="row">
        <div class="col-lg-12">
            <div class="form-group">
                <div class="col-md-6"><div class="form-group">Cash in hand</div></div>
                <div class="col-md-6"><div class="form-group"><div class="text-right">{{ sprintf('%0.2f',$info->cash_in_hand) }}</div></div></div>
            </div>
            <div class="form-group">
                <div class="col-md-6"><div class="form-group">Cash Payment</div></div>
                <div class="col-md-6"><div class="form-group"><div class="text-right">{{ sprintf('%0.2f',$total_sales) }}</div></div></div>
            </div>
            <div class="form-group">
                <div class="col-md-6"><div class="form-group">Credit Card Payment</div></div>
                <div class="col-md-6"><div class="form-group"><div class="text-right">0.00</div></div></div>
            </div>
            <div class="form-group">
                <div class="col-md-6"><div class="form-group">Total Sales</div></div>
                <div class="col-md-6"><div class="form-group"><div class="text-right">{{ sprintf('%0.2f',$total_sales) }}</div></div></div>
            </div>
            <div class="form-group">
                <div class="col-md-6"><div class="form-group"><strong>Total Cash</strong></div></div>
                <div class="col-md-6"><div class="form-group"><div class="text-right" style="font-weight:bold">{{ sprintf('%0.2f',$total_cash) }}</div></div></div>
            </div>                  
        </div>
    </div>
</div>