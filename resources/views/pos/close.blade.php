<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h4 class="modal-title" id="myModalLabel">Sales ({{ $info->opened_at }} - {{ date('Y-m-d H:i:s') }})</h4>
</div>
<div class="modal-body">
    <div class="row">
        <div class="col-lg-12">
            <div class="form-group">
                <div class="col-md-6"><div class="form-group">Cash in hand</div></div>
                <div class="col-md-6"><div class="form-group"><div class="text-right">{{ sprintf('%0.2f',$info->cash_in_hand) }}</div></div></div>
            </div>
            <div class="form-group">
                <div class="col-md-6"><div class="form-group">Cash Payment</div></div>
                <div class="col-md-6"><div class="form-group"><div class="text-right">{{ sprintf('%0.2f',$total_sales) }}</div></div></div>
            </div>
            <div class="form-group">
                <div class="col-md-6"><div class="form-group">Credit Card Payment</div></div>
                <div class="col-md-6"><div class="form-group"><div class="text-right">0.00</div></div></div>
            </div>
            <div class="form-group">
                <div class="col-md-6"><div class="form-group">Total Sales</div></div>
                <div class="col-md-6"><div class="form-group"><div class="text-right">{{ sprintf('%0.2f',$total_sales) }}</div></div></div>
            </div>
            <div class="form-group">
                <div class="col-md-6"><div class="form-group"><strong>Total Cash</strong></div></div>
                <div class="col-md-6"><div class="form-group"><div class="text-right" style="font-weight:bold">{{ sprintf('%0.2f',$total_cash) }}</div></div></div>
            </div>                  
        </div>

            <!-- <div class="col-md-12">
                <div class="form-group">
                    <div class="form-group"><label>Your user password *</label>
                        <input type="password" class="form-control user_password" name="user_password"/>
                    </div>
                </div>      
            </div>  -->         
            <div class="col-md-6">
                <div class="form-group">
                    <div class="form-group"><label>Total Cash *</label>
                        <input type="text" class="form-control total_cash_submitted" name="total_cash_submitted"/>
                    </div>
                </div>      
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <div class="form-group"><label>Total Credit Card Slips *</label>
                        <input type="text" class="form-control total_cc_slips_submitted" name="total_cc_slips_submitted"/>
                    </div>
                </div>  
            </div>
            <div class="col-lg-12">
                <div class="form-group">
                    <div class="form-group">
                        <label>Note</label>
                        <textarea class="form-control" name="register_notes" rows="5" style="max-width:100%"></textarea>
                    </div>
                </div>  
            </div>        
    </div>
</div>
    <div class="modal-footer">
        <button type="button" class="btn btn-primary close_register_button" onclick="

        $('#formCloseRegister').submit();

        setTimeout(function(){ location.reload(true); }, 800);


        ">Close register</button>
        
            <input type="hidden" name="pos_register_id" value="{{ $info->id }}"/>
            <input type="hidden" name="total_cash" value="{{ sprintf('%0.2f',$total_cash) }}"/>
            <input type="hidden" name="total_cc_slips" value="0"/>
            <input type="hidden" name="closed_at" value="{{ date('Y-m-d H:i:s') }}"/>
            <input type="submit" name="close_register_form_submit" style="display:none"/>
        
    </div>