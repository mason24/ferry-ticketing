<script src="{{ asset('js/pos/jquery.js') }}"></script>
<script src="{{ asset('js/jquery-barcode.min.js') }}"></script>
<button style="    color: #fff;
    background-color: #337ab7;
    border-color: #2e6da4;" type="button" onclick="    
    window.opener.location = document.referrer;
    window.close();">Back</button>
@foreach($passengers as $passenger)
            <div class="printingBox" style="display:none;padding: 0px 20px;">


                <table style="float:left;border-right: 2px dashed #333;padding-right: 10px;width: 390px;">
                    <tbody>
                        <tr>
                            <td colspan="2" style="width: 60%;"><img style="display:inline" src="https://www.dragonstarship.com/images/logo.png" alt="" height="50" /></td>
                            <td colspan="2" style="font-size: 14px;vertical-align:top;text-transform:uppercase;width: 40%;"><b>Open Ticket</b><br><b style="font-weight:900">({{ $passenger->Pass_type }})</b></td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <b style="font-size:12px">Name</b><br>
                                <span>{{ $passenger->Pass_name }}  {{ $passenger->pass_langkawicitizen == 1 ? '(Citizen)' : '' }}</span>            
                            </td>
                            
                            

                                            
                        </tr>
                        <tr>
                            <td style="width: 33.333%;">
                                <b style="font-size:12px">From</b><br>
                                  
                                <span style="font-size:13px">{{ $booking->origin->Jetty_Name }}</span>            
                            </td>
                            <td style="padding-left:10px;width: 33.333%;">
                                <b style="font-size:12px">Seat Type</b><br>
                                {{ $passenger->seat_category->Seat_Category_Name }}
                            </td>
                            <td style="padding-left:10px;width: 33.333%;">
                                <b style="font-size:12px">Expiry Date</b><br>
                                {{ $booking->open_ticket_expiry_date }}
                            </td>        
                            
                            
                        </tr>
                        <tr>
                            <td style="width: 33.333%;">
                                <b style="font-size:12px">To</b><br>
                                <span  style="font-size:13px">{{ $booking->destination->Jetty_Name }}</span>          
                            </td>
                            <td style="padding-left:10px;width: 33.333%;">
                                <b style="font-size:12px">Price</b><br>
                                RM {{ sprintf('%0.2f',$passenger->ticket_price) }}
                            </td>               
                            
                        </tr>
                        <tr>
                            <td style="width: 33.333%;">
                                <b style="font-size:12px">Booking ID</b><br>
                                <span style="font-size:13px">{{ $passenger->booking_id }}</span>           
                            </td>
                            <td style="padding-left:10px;width: 33.333%;">
                                <b style="font-size:12px">Code</b><br>
                                {{ $passenger->open_ticket_code }}
                            </td>
                        </tr>
                        
                                           
                    </tbody>
                </table>
                
                <!--<span style="font-weight:bold;font-size:15px;text-transform:uppercase">Boarding Pass</span>-->
                <table style="padding-left: 10px;width: 200px;max-width: 200px;">
                    <tbody>
                        <tr>
                            <td valign="top" style="width: 50%;"><b style="font-size:12px">Name</b> </td>
                            <td style="width: 50%;"><span>{{ (strlen($passenger->Pass_name) > 10 ? substr($passenger->Pass_name, 0 , 7).'...' : $passenger->Pass_name) }} ({{ $passenger->seat_category->Seat_Category_Name }}) {{ $passenger->pass_langkawicitizen == 1 ? 'Citizen' : '' }}</span></td>
                        </tr>                              

                        <tr>
                            <td valign="top" style="width: 50%;"><b style="font-size:12px">From</b></td>
                            <td style="font-size:12px;width: 50%;">{{ $booking->origin->Jetty_Name }}</td>
                        </tr>
                        <tr>
                            <td valign="top" style="width: 50%;"><b style="font-size:12px">To</b></td>
                            <td style="font-size:12px;width: 50%;">{{ $booking->destination->Jetty_Name }}</td>
                        </tr>
                        <tr>
                            <td valign="top" style="width: 50%;"><b style="font-size:12px">Booking ID</b></td>
                            <td style="font-size:12px;width: 50%;">{{ $passenger->booking_id }}</td>
                        </tr>               
                        
                        <tr>
                            <td valign="top" style="width: 50%;"><b style="font-size:12px">Seat</b></td>
                            <td style="font-size:12px;width: 50%;">{{ $passenger->seat_category->Seat_Category_Name }}</td>
                        </tr>
                        <tr>
                            <td valign="top" style="width: 50%;"><b style="font-size:12px">Price</b></td>
                            <td style="font-size:12px;width: 50%;">RM {{ sprintf('%0.2f',$passenger->ticket_price) }}</td>
                        </tr>
                        <tr>
                            <td valign="top" style="width: 50%;"><b style="font-size:12px">Expiry Date</b></td>
                            <td style="font-size:12px;width: 50%;">{{ $booking->open_ticket_expiry_date }}</td>
                        </tr>                        
                        <tr>
                            <td valign="top" style="width: 50%;"><b style="font-size:12px">Code</b></td>
                            <td style="font-size:12px;width: 50%;">{{ $passenger->open_ticket_code }}</td>
                        </tr>
                        <tr>
                            <td colspan="2"><div id="barcode_{{ $passenger->open_ticket_code }}"></div></td>
                        </tr>                                       
                    </tbody>
                </table>    
              <script>
              $("#barcode_{{ $passenger->open_ticket_code }}").barcode("{{ $passenger->open_ticket_code }}", "code93", {barWidth:2, barHeight:30});        
              </script> 
            </div>
            <div class="next-page"></div>
@endforeach            
<style>
button {
    display: inline-block;
    padding: 70px;
    margin-bottom: 0;
    font-size: 70px;
    font-weight: 400;
    line-height: 1.42857143;
    text-align: center;
    white-space: nowrap;
    -ms-touch-action: manipulation;
    touch-action: manipulation;
    cursor: pointer;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
    border: 1px solid transparent;
    border-radius: 4px;
}
body , table tr td{
    font-family:arial,sans-serif;
    font-size:14px;
}
.barcode canvas {
    height:40px;
    width: 200px;
}
@media print {
    button{
        display:none;
    }
    .printingBox{
        display: block !important;
    }
    .next-page{
        page-break-after: always;
    }
}
</style>
<script>
window.print()
</script>