@extends('layout.master')
@section('content')
    @include('layout.errors')
    <form method="post" action="{{ URL::to('/rates') }}">
        {{ csrf_field() }}
        <div class="row">
            <div id="acct-password-row" class="span7">
                <fieldset>
                    <legend>New Rate</legend><br>
                    <div class="control-group ">
                        <label for="Rate_Title" class="control-label">Rate Title<span class="required">*</span></label>
                        <div class="controls">
                            <input id="current-pass-control" name="Rate_Title" class="span4" type="text" value="{{ $task->Rate_Title }}"/>

                        </div>
                    </div>
                    <div class="control-group ">
                        <label for="Rate_Type" class="control-label">Rate Type<span class="required">*</span></label>
                        <div class="controls">
                            <select id="current-pass-control" name="Rate_Type" class="span4">
                                <option>Discount</option>
                                <option>Additional Charge</option>
                            </select>

                        </div>
                    </div>                                        
                    <div class="control-group ">
                        <label for="Amount" class="control-label">Amount <span class="required">*</span></label>
                        <div class="controls">
                            <input id="current-pass-control" name="Amount" class="span4" type="text" value="{{ $task->Amount }}"/>

                        </div>
                    </div> 
                    <div class="control-group ">
                        <label for="Amount_Type" class="control-label">Amount Type <span class="required">*</span></label>
                        <div class="controls">
                            <select id="current-pass-control" name="Amount_Type" class="span4">
                                <option>Percentage</option>
                                <option>Value</option>
                            </select>
                        </div>
                    </div>                                       
                </fieldset>
            </div>
        </div>
        <footer id="submit-actions" class="form-actions">
            <button id="submit-button" type="submit" class="btn btn-primary" value="">Create</button>
            <a href="{{ URL::to('/rates') }}" class="btn btn-default">Back</a>
        </footer>
    </form>
@endsection