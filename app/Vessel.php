<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vessel extends Model
{
    public function vessel_seats(){

        return $this->hasMany('App\Vessel_Seat');

    }
    public function trip(){

        return $this->hasOne('App\Trip','Vessel_Id');

    }    
}
