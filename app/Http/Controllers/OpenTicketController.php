<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;

use App\Jetty;

use App\OpenTicket;

use App\OpenTicketPrice;

use App\SeatCategory;

use App\TripPrices;

use App\Booking;

use App\Passenger;


class OpenTicketController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }     

	public function index(){

        $OpenTicket = new OpenTicket;
    	
        $open_tickets = $OpenTicket->paginate(20);

    	return view('open_ticket.index',compact('open_tickets','pos_register_info'));
	}    

    public function create(){

        $jetty = new Jetty; 

        $jetties = $jetty->orderBy('Jetty_Name')->get();

        return view('open_ticket.create', ['jetties'=> $jetties]);  

    }   


    public function code(){

		$chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";

		$code = "";

		for ($i = 0; $i <= 5; $i++) {

		    $code .= $chars[mt_rand(0, strlen($chars)-1)];

		} 

		$code = $this->check_existing_code($code);  

        return $code;	
    }
    public function check_existing_code($code){

		if (Passenger::where('open_ticket_code', '=', $code)->exists()) {

			$this->code();

		}
		else{

			return $code;

		}
    }

    // public function store(){

    //     $this->validate(request(),[

    //         'origin' => 'required',

    //         'destination' => 'required'

    //     ]);   

    //     $OpenTicket = new OpenTicket;

    //     $OpenTicket->origin = request('origin');

    //     $OpenTicket->destination = request('destination');

    //     $OpenTicket->valid_till = date_format(date_create(request('expiry_date')),'Y-m-d');

    //     $OpenTicket->no_of_adults = request('total_adults');

    //     $OpenTicket->no_of_children = request('total_children');

    //     $OpenTicket->save();

    //     $open_ticket_id = $OpenTicket->id;

    //     $passenger_names = request('passenger_name');

    //     $passenger_ticket_prices = request('passenger_price');

    //     $id_types = request('id_type');

    //     $id_numbers = request('identity_number');

    //     $passenger = new Passenger;

    //     for($i=0;$i<count($passenger_names);$i++){

    //         $open_ticket_code = $this->code();

    //         $passenger->insert([
    //             [
    //                 'Pass_name' => $passenger_names[$i],

    //                 'pass_identity_type' => $id_types[$i],

    //                 'pass_identity_id' => $id_numbers[$i],

    //                 'pass_langkawicitizen' => 0,

    //                 'booking_id' => '111',

    //                 'open_ticket_id' => $open_ticket_id,

    //                 'open_ticket_code' => $open_ticket_code,

    //                 'ticket_price' => $passenger_ticket_prices[$i],

    //                 'status' => 1,
    //             ]
    //         ]); 

    //     }        

    //     return redirect('pos/open-ticket');
 
    // }

    public function store(){

        $this->validate(request(),[

            'origin' => 'required',

            'destination' => 'required'

        ]); 

        $booking_id = date('Ymd') . time() . rand(10*45, 100*98);  

        $Booking = new Booking;

        $Booking->booking_id = $booking_id;

        $Booking->journey = 'one-way';

        $Booking->origin_id = request('origin');

        $Booking->destination_id = request('destination');

        // $Booking->selected_onward_trip_id = 0;

        // $Booking->selected_return_trip_id = 0;


        $Booking->total_adults = request('total_adults');

        $Booking->total_children = request('total_children');

        $Booking->total_tax = 0;

        $Booking->total_discount = 0;

        $Booking->grand_total_amount = request('gtotal');

        $Booking->created_by = \Auth::user()->id;

        $Booking->source = 'counter';
        
        $Booking->pos_register_id = request('pos_register_id');

        $Booking->open_ticket_expiry_date = date_format(date_create(request('expiry_date')),'Y-m-d');

        $Booking->payment_mode = 'open_ticket';

        $Booking->booking_status = 1;        

        $Booking->save();

        $open_ticket_id = $Booking->id;

        $passenger_names = request('passenger_name');

        $passenger_seat_categories = request('passenger_seat_category_id');

        $passenger_types = request('passenger_type');

        $passenger_ticket_prices = request('passenger_price');

        // $passenger_citizens = request('passenger_citizen');

        $id_types = request('id_type');

        $id_numbers = request('identity_number');

        $passenger = new Passenger;

        for($i=0;$i<count($passenger_names);$i++){

            $open_ticket_code = $this->code();

            $passenger->insert([
                [
                    'Pass_name' => (empty($passenger_names[$i]) ? 'N/A' : $passenger_names[$i]),

                    'Pass_type' => $passenger_types[$i],

                    'pass_identity_type' => $id_types[$i],

                    'pass_identity_id' => (empty($id_numbers[$i]) ? 'N/A' : $id_numbers[$i]),

                    'pass_langkawicitizen' => (request('passenger_citizen_'.$i) !== null ? 1 : 0),

                    'SeatCategoryId' => $passenger_seat_categories[$i],

                    'booking_id' => $booking_id,

                    'open_ticket_code' => $open_ticket_code,

                    'ticket_price' => $passenger_ticket_prices[$i],

                    'status' => 1,
                ]
            ]); 

        }        

        return redirect('pos/open-ticket/print/'.$booking_id);
 
    }    

    public function pos_index(){

        $pos_register_info = $this->is_opened(1);

        $origins = Jetty::orderBy('Jetty_Name')->get();

        return view('pos.open_ticket',compact('origins','pos_register_info'));

    }

    // public function seat_category($origin,$destination){

    //     $seat_category_array = array();

    //     $seat_categories = SeatCategory::orderBy('Seat_Category_Name')->get();

    //     foreach($seat_categories as $seat_category){

    //         $adult_price_info = TripPrices::select('adult_price')->where('vessel_seat_id','=',$seat_category->id)->first();

    //         $child_price_info = TripPrices::select('child_price')->where('vessel_seat_id','=',$seat_category->id)->first();

    //         if($adult_price_info !== null){

    //             $seat_category_array[] = array(

    //                 'id' => $seat_category->id,

    //                 'Seat_Category_Name' => $seat_category->Seat_Category_Name,

    //                 'adult_price' => $adult_price_info->adult_price,

    //                 'child_price' => $child_price_info->child_price

    //             );            
    //         }



    //     }

    //     return view('open_ticket.seat_category',compact('seat_category_array'));

    // }

    public function seat_category($origin,$destination){

        $prices = OpenTicketPrice::where([['origin_id','=',$origin],['destination_id','=',$destination]])->get();

        return view('open_ticket.seat_category',compact('prices'));

    }    

    public function check_code($code,$origin,$destination){

        $is_expired = false;

        $passenger_info = Passenger::select('id','Pass_type','SeatCategoryId','ticket_price','open_ticket_code_used','booking_id')->where('open_ticket_code', '=', $code)->first();

        if($passenger_info !== null){
            
            $booking_id = $passenger_info->booking_id;

            $open_ticket = Booking::select('open_ticket_expiry_date')->where('booking_id', '=', $booking_id)->first();

            $expiry_date = $open_ticket->open_ticket_expiry_date;

            if($expiry_date < date('Y-m-d')){

                $is_expired = true;

            }

            $passenger_info->is_expired = $is_expired;

            // if ($passenger_info !== null) {

            //     $is_valid = OpenTicket::where([

            //         ['origin', '=', $origin],

            //         ['destination', '=', $destination],

            //         ['id', '=', $passenger_info->open_ticket_id],

            //         ['valid_till', '>=', date('Y-m-d')]

            //     ])->first();

            //     if($is_valid !== null){

            //         return $passenger_info;

            //     }
            //     else{
            //         return 'null';
            //     }

            // }
            // else{

            //     return false;

            // }

            // return view('pos.open_ticket_details',compact('passenger_info'));
            return response()->json(['passenger_info' => $passenger_info]);

        }

    }

    public function print_ticket($booking_id){

        $booking = Booking::where('booking_id','=',$booking_id)->first();

        $passengers = Passenger::where('booking_id','=',$booking_id)->get();

        return view('open_ticket.print',compact('passengers','booking'));

    } 

    public function is_opened($user_id){

        $pos_info = DB::table('pos_registers')->where([

            ['user_id','=',$user_id],

            ['status','=','open']

        ])->first();

        if($pos_info === null){

            return false;

        }
        return $pos_info;

    }        
}
