@extends('layout.master')
@section('content')
            <!-- <div class="span15">
                <div class="blockoff-left">
                    <legend class="lead">
                        Welcome Kar Chun
                    </legend>
                    <p>
                        <strong>Your last login was on: </strong> {{ date('H:i') }}
                    </p>
                </div>
            </div> -->
            <div class="span5">
                <div class="box">
                    
                    <div class="box-content" style="background-color: #e9e9e9;background-image: linear-gradient(to bottom, #eeeeee, #e1e1e1);background-repeat: repeat-x;">
                        <div class="btn-group-box" style="position:relative;">
                            <h1 style="position:absolute;color:#777777;margin-top: 0;"><i class="icon-calendar icon-large"></i></h1>
                            <div style="text-align:right">
                                <h3>{{ $total_bookings_today }}</h3>
                                <span>New bookings today</span>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
            <div class="span5">
                <div class="box">
                    
                    <div class="box-content" style="background-color: #e9e9e9;background-image: linear-gradient(to bottom, #eeeeee, #e1e1e1);background-repeat: repeat-x;">
                        <div class="btn-group-box" style="position:relative;">
                            <h1 style="position:absolute;color:#777777;margin-top: 0;"><i class="icon-bar-chart icon-large"></i></h1>
                            <div style="text-align:right">
                                <h3><sup style="font-size:12px">RM</sup> {{ sprintf('%0.2f',$total_revenue_today) }}</h3>
                                <span>Total revenue today</span>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
            <div class="span5">
                <div class="box">
                    
                    <div class="box-content" style="background-color: #e9e9e9;background-image: linear-gradient(to bottom, #eeeeee, #e1e1e1);background-repeat: repeat-x;">
                        <div class="btn-group-box" style="position:relative;">
                            <h1 style="position:absolute;color:#777777;margin-top: 0;"><div class="icon-large" style="width:50px;height:50px;background-image:url({{ asset('images/ferry.png') }});background-size: 50px 50px;
    background-repeat: no-repeat;"></div></h1>
                            <div style="text-align:right">
                                <h3>{{ $total_departures_today }}</h3>
                                <span>Ferries to departure today</span>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div> 
            <div class="span7">
                <div class="box">
                    <div class="box-header">
                        <h5>Weather Report</h5>
                    </div>
                    <div class="box-content">
                        <div id="weather"></div>
                    </div>
                </div>
            </div>                                                           
            <div class="span8">
                <div class="box">
                    <div class="box-header">
                        <h5>Quick Links</h5>
                    </div>
                    <div class="box-content">
                        <div class="btn-group-box">
                            <button class="btn" onclick="window.location.href='bookings'"><i class="icon-calendar icon-large"></i><br/>Bookings</button>
                            <button class="btn" onclick="window.location.href='trips'"><i class="icon-time icon-large"></i><br/>Trips</button>
                            <button class="btn" onclick="window.location.href='group'"><i class="icon-group icon-large"></i><br/>Groups</button>
                            <button class="btn" onclick="window.location.href='vessels'"><div class="icon-large" style="width:25px;height:25px;background-image:url({{ asset('images/route.png') }});background-size: 25px 25px;
    background-repeat: no-repeat;margin: 0 auto;"></div>Routes</button>
                            <button class="btn" onclick="window.location.href='group'"><i class="icon-list-alt icon-large"></i><br/>Reports</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="span15">
                <script type="text/javascript">
                    google.load('visualization', '1', {'packages': ['corechart']});
                    google.setOnLoadCallback(drawVisualization);
                    
                    function drawVisualization() {
                        visualization_data = new google.visualization.DataTable();
                        
                        visualization_data.addColumn('string', 'Task');
                        
                        visualization_data.addColumn('number', 'Total Revenue (RM)');
                        
                        
                        visualization_data.addRow(['Jan', {{ sprintf('%0.2f',$total_revenue_january) }} ]);
                        visualization_data.addRow(['Feb', {{ sprintf('%0.2f',$total_revenue_february) }} ]);
                        visualization_data.addRow(['Mar', {{ sprintf('%0.2f',$total_revenue_march) }} ]);
                        visualization_data.addRow(['Apr', {{ sprintf('%0.2f',$total_revenue_april) }} ]);
                        visualization_data.addRow(['May', {{ sprintf('%0.2f',$total_revenue_may) }} ]);
                        visualization_data.addRow(['Jun', {{ sprintf('%0.2f',$total_revenue_june) }} ]);
                        visualization_data.addRow(['Jul', {{ sprintf('%0.2f',$total_revenue_july) }} ]);
                        visualization_data.addRow(['Aug', {{ sprintf('%0.2f',$total_revenue_august) }} ]);
                        visualization_data.addRow(['Sep', {{ sprintf('%0.2f',$total_revenue_september) }} ]);
                        visualization_data.addRow(['Oct', {{ sprintf('%0.2f',$total_revenue_october) }} ]);
                        visualization_data.addRow(['Nov', {{ sprintf('%0.2f',$total_revenue_november) }} ]);
                        visualization_data.addRow(['Dec', {{ sprintf('%0.2f',$total_revenue_december) }} ]);
                        
                        
                      
                        visualization = new google.visualization.ColumnChart(document.getElementById('barchart'));
                    
                        
                    
                    
                    
                        
                        
                        visualization.draw(visualization_data, {title: 'Revenue Chart ({{ date("Y") }})', height: 300});
                    
                        
                    }
                </script>
                <div class="blockoff-left">
                    <div id="barchart"></div>
                </div>
            </div>
        </div>
        <style>
        /*
          Docs at http://http://simpleweatherjs.com

          Look inspired by http://www.degreees.com/
          Used for demo purposes.

          Weather icon font from http://fonts.artill.de/collection/artill-weather-icons

          DO NOT hotlink the assets/font included in this demo. If you wish to use the same font icon then download it to your local assets at the link above. If you use the links below odds are at some point they will be removed and your version will break.
        */

        @font-face {
            font-family: 'weather';
            src: url('https://s3-us-west-2.amazonaws.com/s.cdpn.io/93/artill_clean_icons-webfont.eot');
            src: url('https://s3-us-west-2.amazonaws.com/s.cdpn.io/93/artill_clean_icons-webfont.eot?#iefix') format('embedded-opentype'),
                 url('https://s3-us-west-2.amazonaws.com/s.cdpn.io/93/artill_clean_icons-webfont.woff') format('woff'),
                 url('https://s3-us-west-2.amazonaws.com/s.cdpn.io/93/artill_clean_icons-webfont.ttf') format('truetype'),
                 url('https://s3-us-west-2.amazonaws.com/s.cdpn.io/93/artill_clean_icons-webfont.svg#artill_clean_weather_iconsRg') format('svg');
            font-weight: normal;
            font-style: normal;
        }

        #weather {
          margin: 0px auto;
          text-align: center;
        }

        #weather i {
          font-family: weather;
          font-size: 60px;
          font-weight: normal;
          font-style: normal;
          line-height: 1.0;
          text-transform: none;
        }

        .icon-0:before { content: ":"; }
        .icon-1:before { content: "p"; }
        .icon-2:before { content: "S"; }
        .icon-3:before { content: "Q"; }
        .icon-4:before { content: "S"; }
        .icon-5:before { content: "W"; }
        .icon-6:before { content: "W"; }
        .icon-7:before { content: "W"; }
        .icon-8:before { content: "W"; }
        .icon-9:before { content: "I"; }
        .icon-10:before { content: "W"; }
        .icon-11:before { content: "I"; }
        .icon-12:before { content: "I"; }
        .icon-13:before { content: "I"; }
        .icon-14:before { content: "I"; }
        .icon-15:before { content: "W"; }
        .icon-16:before { content: "I"; }
        .icon-17:before { content: "W"; }
        .icon-18:before { content: "U"; }
        .icon-19:before { content: "Z"; }
        .icon-20:before { content: "Z"; }
        .icon-21:before { content: "Z"; }
        .icon-22:before { content: "Z"; }
        .icon-23:before { content: "Z"; }
        .icon-24:before { content: "E"; }
        .icon-25:before { content: "E"; }
        .icon-26:before { content: "3"; }
        .icon-27:before { content: "a"; }
        .icon-28:before { content: "A"; }
        .icon-29:before { content: "a"; }
        .icon-30:before { content: "A"; }
        .icon-31:before { content: "6"; }
        .icon-32:before { content: "1"; }
        .icon-33:before { content: "6"; }
        .icon-34:before { content: "1"; }
        .icon-35:before { content: "W"; }
        .icon-36:before { content: "1"; }
        .icon-37:before { content: "S"; }
        .icon-38:before { content: "S"; }
        .icon-39:before { content: "S"; }
        .icon-40:before { content: "M"; }
        .icon-41:before { content: "W"; }
        .icon-42:before { content: "I"; }
        .icon-43:before { content: "W"; }
        .icon-44:before { content: "a"; }
        .icon-45:before { content: "S"; }
        .icon-46:before { content: "U"; }
        .icon-47:before { content: "S"; }

        #weather h2 {
          margin: 0 0 8px;
          font-size: 60px;
          font-weight: 300;
          text-shadow: 0px 1px 3px rgba(0, 0, 0, 0.15);
        }

        #weather ul {
          margin: 0;
          padding: 0;
        }

        #weather li {
          padding: 0px 20px;  
          background: #fff;
          background: rgba(255,255,255,0.90);
          display: inline-block;
          border-radius: 5px;
        }

        #weather .currently {
          margin: 0 20px;
        }
        </style>        
        <script>
        // v3.1.0
        //Docs at http://simpleweatherjs.com
        $(document).ready(function() {
          $.simpleWeather({
            location: 'Langkawi, MY',
            woeid: '',
            unit: 'c',
            success: function(weather) {
              html = '<h2><i class="icon-'+weather.code+'"></i> '+weather.temp+'&deg;'+weather.units.temp+'</h2>';
              html += '<ul><li>'+weather.city+', '+weather.region+'</li>';
              html += '<li>'+weather.currently+'</li></ul>';
          
              $("#weather").html(html);
            },
            error: function(error) {
              $("#weather").html('<p>'+error+'</p>');
            }
          });
        });
        </script>        
@endsection