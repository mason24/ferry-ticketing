@extends('layout.master')
@section('content')
    <div class="box pattern pattern-sandstone">
        <div class="box-header">
            <i class="icon-table"></i>
            <h5>
                Bookings
            </h5>
        </div>
        <div class="box-content box-table">
       <!--  <div><a href="bookings/new">New Booking</a></div> -->
        Sort: <a href="bookings?sort=asc">Ascending</a> | <a href="bookings?sort=desc">Descending</a>
            <table id="sample-table" class="table table-hover table-bordered tablesorter">
                <thead>
                    <tr>
                        <th>Booking ID</th>
                        <th>Journey</th>
                        <th>From</th>
                        <th>To</th>
                        <th>Source</th>
                        <th>Created Date</th>
                        <th>Updated Date</th>
                        <th>Status</th>
                        <th>Grand Total Amount (RM)</th><!-- 
                        <th class="td-actions"></th> -->
                    </tr>
                </thead>
                <tbody>
                	@foreach ($bookings as $booking)
                        <tr>
                            <td>{{ $booking->booking_id }}</td>
                            <td>{{ $booking->journey }}</td>
                            <td>{{ $booking->origin->Jetty_Name }}</td>
                            <td>{{ $booking->destination->Jetty_Name }}</td>
                            <td>{{ $booking->source }}</td>
                            <td>{{ $booking->created_at }}</td>
                            <td>{{ $booking->updated_at }}</td>
                            <td>{{ $booking->booking_status == 1 ? 'Confirmed' : 'Pending' }}</td>
                            <td style="text-align:right">{{ sprintf('%0.2f',$booking->grand_total_amount) }}</td>
                            <!-- <td class="td-actions">
                                <a href="javascript:;" class="btn btn-small btn-info">
                                    <i class="btn-icon-only icon-edit"></i>
                                </a>

                                <a href="javascript:;" class="btn btn-small btn-danger">
                                    <i class="btn-icon-only icon-remove"></i>
                                </a>
                            </td> -->
                        </tr>
                    @endforeach
                </tbody>
            </table>

            
        </div>
        {{ $bookings->links() }}
    </div>
@endsection