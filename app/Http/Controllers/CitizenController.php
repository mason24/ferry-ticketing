<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;

use App\Citizen;

use App\Jetty;

use App\Vessel;

use App\SeatCategory;

class CitizenController extends Controller
{
    public function index(){

    	$citizens = Citizen::paginate(20);

    	return view('citizen_rates.index',compact('citizens'));

    }

    public function create(){

    	$jetties = Jetty::orderBy('Jetty_Name')->get();

    	$vessels = Vessel::orderBy('Vessel_Name')->get();

    	return view('citizen_rates.create',compact('jetties','vessels'));

    }

    public function store(){

        $this->validate(request() , [

         'origin' => 'required|not_in:--Choose--',

         'destination' => 'required|not_in:--Choose--',

         'vessel' => 'required|not_in:--Choose--'

        ]);

        

        $citizen_id = DB::table('citizens')->insertGetId(array(

                'origin_id' => request('origin'),

                'destination_id' => request('destination'),

                'status' => request('status')
            
        ));       

        $adult_seats = request('adult-price');

        $child_seats = request('child-price');

        foreach($adult_seats as $seat_id => $price){

            DB::table('citizen_categories')->insert([
                [
                    'citizen_id' => $citizen_id,

                    'vessel_id' => request('vessel'),

                    'vessel_seat_id' => $seat_id,

                    'adult_price' => $adult_seats[$seat_id],

                    'child_price' => $child_seats[$seat_id]
                ]
            ]); 
                         
        }

        return redirect('/settings/citizen-rates');

    }    

    public function vessel_seats($vessel_id){

        $seats_array = array();

        $seats = DB::table('vessel__seats')->select('*')->where('vessel_id','=',$vessel_id)->get();

        foreach($seats as $seat){
            
            $seat_category = SeatCategory::select('Seat_Category_Name')->where('id','=',$seat->seat_category_id)->first();

            array_push($seats_array,array(

                'id' => $seat->id,

                'seat_category_name' => $seat_category->Seat_Category_Name,

                'vessel_id' => $seat->vessel_id,

                'seat_category_id' => $seat->seat_category_id,

                'capacity' => $seat->capacity

            ));

        }

        return view('vessels.seats',compact('seats_array'));

    }      
}
