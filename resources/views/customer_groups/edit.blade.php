@extends('layout.master')
@section('content')
    @include('layout.errors')
    <form method="post" action="{{ URL::to('/group/edit/'.$group->id) }}" enctype="multipart/form-data">
        {{ csrf_field() }}
        <div class="row">
            <div id="acct-password-row" class="span7">
                <fieldset>
                    <legend>Edit Customer Group</legend><br>
                    <div class="control-group ">
                        <label for="title" class="control-label">Customer Group Name <span class="required">*</span></label>
                        <div class="controls">
                            <input id="current-pass-control" name="name" class="span4" type="text" required value="{{ $group->name }}"/>

                        </div>
                    </div> 
                    <div class="control-group ">
                        <label for="title" class="control-label">Customer Group Description <span class="required">*</span></label>
                        <div class="controls">
                            <textarea id="current-pass-control" name="description" class="span4" style="max-width:100%" rows="10">{{ $group->description }}</textarea>

                        </div>
                    </div>
                    <div class="control-group ">
                        <label for="title" class="control-label">Upload Customer Data CSV File</label>
                        <div class="controls">
                            <input type="file" name="imported_file" accept=".csv"/>
                        </div>
                    </div>                                                                            
                </fieldset>
            </div>
        </div>
        <footer id="submit-actions" class="form-actions">
            <button id="submit-button" type="submit" class="btn btn-primary" value="">Save Changes</button>
            <a href="{{ URL::to('/group') }}" class="btn btn-default">Back</a>
        </footer>
    </form>
@endsection