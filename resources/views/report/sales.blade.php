@extends('layout.master')
@section('content')
    <div class="box pattern pattern-sandstone">
        <div class="box-header">
            <i class="icon-table"></i>
            <h5>
                Sales Report
            </h5>
        </div>
        <div class="box-content box-table">
        Sort: <a href="report/sales?sort=asc">Ascending</a> | <a href="report/sales?sort=desc">Descending</a>
            <table id="sample-table" class="table table-hover table-bordered tablesorter">
                <thead>
                    <tr>
                        <th>Date</th>
                        <th>Booking ID</th>
                        <th>Biller</th>
                        <th>Payer</th>
                        <th>Grand Total</th>
                        <th>Paid</th>
                        <th>Balance</th>
                        <th>Payment Status</th>
                        <th>Booking Type</th>
                        <th class="td-actions"></th>
                    </tr>
                </thead>
                <tbody>
                	@foreach ($bookings as $booking)
                        <tr>
                            <td>{{ $booking->created_at }}</td>
                            <td>{{ $booking->booking_id }}</td>
                            <td>{{ $booking->booking_id }}</td>
                            <td>{{ $booking->booking_id }}</td>
                            <td>{{ $booking->booking_id }}</td>
                            <td>{{ $booking->grand_total_amount }}</td>
                            <td>{{ $booking->grand_total_amount }}</td>
                            <td>{{ $booking->grand_total_amount }}</td>
                            <td>{{ $booking->grand_total_amount }}</td>
                            <td class="td-actions">
                                <a href="javascript:;" class="btn btn-small btn-info">
                                    <i class="btn-icon-only icon-edit"></i>
                                </a>

                                <a href="javascript:;" class="btn btn-small btn-danger">
                                    <i class="btn-icon-only icon-remove"></i>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>

            {{ $bookings->links() }}
        </div>
    </div>
@endsection