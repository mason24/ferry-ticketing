@extends('layout.master')
@section('content')
                        <div class="box pattern pattern-sandstone">
                            <div class="box-header">
                                <i class="icon-table"></i>
                                <h5>
                                    Users
                                </h5>

                            </div>
                            <div class="form-group">
                                <a href="register">New User</a> 
                            </div>                            
                            <div class="form-group">
                                Sort: <a href="users?sort=asc">Ascending</a> | <a href="users?sort=desc">Descending</a> 
                            </div>
                            <div class="box-content box-table">
                                <table id="sample-table" class="table table-hover table-bordered tablesorter">
                                    <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Username</th>
                                            <th>Created Date</th>
                                            <th>Updated Date</th>
                                            <th class="td-actions"></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    	@foreach ($users as $user)
	                                        <tr>
	                                            <td>{{ $user->name }}</td>
	                                            <td>{{ $user->username }}</td>
	                                            <td>{{ $user->created_at }}</td>
	                                            <td>{{ $user->updated_at }}</td>
	                                            <td class="td-actions">
	                                                <a href="users/edit/{{ $user->id }}" class="btn btn-small btn-info">
	                                                    <i class="btn-icon-only icon-edit"></i>
	                                                </a>

	                                                <a href="users/delete/{{ $user->id }}" class="btn btn-small btn-danger">
	                                                    <i class="btn-icon-only icon-remove"></i>
	                                                </a>
	                                            </td>
	                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>

                                {{ $users->links() }}
                            </div>
                        </div>
@endsection