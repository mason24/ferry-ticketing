<input type="checkbox"/> All citizens
@for($i=0;$i<$total_adults;$i++)

<div class="panel panel-info">
    <div class="panel-heading">
        <label>Passenger {{ $passenger_number++ }} (Adult)</label>
        <input type="checkbox" /> Citizen
    </div>
    <div class="panel-body dashboard_column">
        <div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label>Full Name</label>
					<input type="text" class="form-control"/>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label>Identity Type</label>
					<select class="form-control">
						<option>Mykad</option>
						<option>Mykid</option>
						<option>Passport</option>
					</select>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label>Identity Number</label>
					<input type="text" name="identity_number" class="form-control"/>
				</div>
			</div>
        </div>
    </div>
</div>


@endfor

@for($j=0;$j<$total_children;$j++)
<div class="panel panel-info">
    <div class="panel-heading">
        <label>Passenger {{ $passenger_number++ }} (Child)</label>
        <input type="checkbox" /> Citizen
    </div>
    <div class="panel-body dashboard_column">
        <div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label>Full Name</label>
					<input type="text" class="form-control"/>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label>Identity Type</label>
					<select class="form-control">
						<option>Mykad</option>
						<option>Mykid</option>
						<option>Passport</option>
					</select>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label>Identity Number</label>
					<input type="text" name="identity_number" class="form-control"/>
				</div>
			</div>
        </div>
    </div>
</div>

@endfor