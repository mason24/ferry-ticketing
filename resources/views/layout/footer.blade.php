        <footer class="application-footer">
            <div class="container">
                <p>Ferry Ticketing System</p>
                <div class="disclaimer">
                    <p>All rights reserved.</p>
                    <p>Copyright © Netsoft Media {{ date('Y') }}</p>
                </div>
            </div>
        </footer>
        <script type="text/javascript">
            $(function(){
                // document.forms['loginForm'].elements['j_username'].focus();
                // $('body').addClass('pattern pattern-sandstone');
                $("[rel=tooltip]").tooltip();
                $('.datepicker').datepicker();
                $(".chosen").chosen(); 
                
            });
        </script>
        <script src="{{ asset('js/bootstrap/bootstrap-alert.js') }}" type="text/javascript" ></script>
        <script src="{{ asset('js/bootstrap/bootstrap-modal.js') }}" type="text/javascript" ></script>
        <script src="{{ asset('js/bootstrap/bootstrap-dropdown.js') }}" type="text/javascript" ></script>
        <script src="{{ asset('js/bootstrap/bootstrap-scrollspy.js') }}" type="text/javascript" ></script>
        <script src="{{ asset('js/bootstrap/bootstrap-tab.js') }}" type="text/javascript" ></script>
        <script src="{{ asset('js/bootstrap/bootstrap-tooltip.js') }}" type="text/javascript" ></script>
        <script src="{{ asset('js/bootstrap/bootstrap-popover.js') }}" type="text/javascript" ></script>
        <script src="{{ asset('js/bootstrap/bootstrap-button.js') }}" type="text/javascript" ></script>
        <script src="{{ asset('js/bootstrap/bootstrap-collapse.js') }}" type="text/javascript" ></script>
        <script src="{{ asset('js/bootstrap/bootstrap-carousel.js') }}" type="text/javascript" ></script>
        <script src="{{ asset('js/bootstrap/bootstrap-typeahead.js') }}" type="text/javascript" ></script>
        <script src="{{ asset('js/bootstrap/bootstrap-affix.js') }}" type="text/javascript" ></script>
        <script src="{{ asset('js/bootstrap/bootstrap-datepicker.js') }}" type="text/javascript" ></script>
        <script src="{{ asset('js/jquery/jquery-tablesorter.js') }}" type="text/javascript" ></script>
        <script src="{{ asset('js/jquery/jquery-chosen.js') }}" type="text/javascript" ></script>
        <script src="{{ asset('js/jquery/virtual-tour.js') }}" type="text/javascript" ></script>