<script src="{{ asset('js/pos/jquery.js') }}"></script>
<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Cash Register Shift Report</h1>
        </div>
        <div class="col-lg-12">
            <div class="form-group">
            </div>
        </div>  
    </div>
      <div class="row">
        <div class="col-lg-12">
            <div class="form-group">
                <label>Open Time: </label> {{ $pos->opened_at }}
            </div>
        </div>  
        <div class="col-lg-12">
            <div class="form-group">
                <label>Opening Cash Balance (RM): </label> {{ sprintf('%0.2f',$pos->cash_in_hand) }}
            </div>
        </div>  
    </div>
    
 
    <div class="row">
        <div class="col-lg-12">
            <div class="form-group">
                <label>Closing Cash Balance (RM): </label> {{ sprintf('%0.2f',$pos->total_cash) }}
            </div>
        </div>  
        <div class="col-lg-12">
            <div class="form-group">
                <label>Total Credit Card Slips: </label> {{ $pos->total_cc_slips }}
            </div>
        </div>      
    </div> 
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-body">    
                    <div class="table-responsive">
                        <table class="table table-hover" border="0">
                            <thead>
                                <tr>
                                    <th style="width: 10%;">No.</th>
                                    <th style="width: 40%;">Transaction No.</th>
                                    <th style="width: 20%;">Seat No.</th>
                                    <th style="width: 20%;">Seat Type</th>
                                    <th style="width: 20%;">Ticket Type</th>
                                    <th style="width: 20%;">Passenger Type</th>
                                    <th style="width: 20%;">Citizen</th>
                                    <th style="width: 35%;">Payment Mode</th>
                                    <th style="width: 10%;">Amount (RM)</th>
                                    <th style="width: 20%;">Created By</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($sales as $sale)
                                <span style="opacity:0">{{ $total_transactions++ }}</span>
                                <span style="opacity:0">{{ $sale->pass_langkawicitizen == 1 ? $total_citizen++ : $total_citizen }}</span>
                                <span style="opacity:0">{{ is_null($sale->SeatNo) ? $total_seats : $total_seats++ }}</span>
                                <span style="opacity:0">{{ $sale->Pass_type == 'Adult' ? $total_adults++ : $total_adults }}</span>
                                <span style="opacity:0">{{ $sale->Pass_type == 'Child' ? $total_children++ : $total_children }}</span>
                                <span style="opacity:0">{{ $sale->payment_mode == 'cash' ? $total_cash_payments++ : $total_cash_payments }}</span>
                                <span style="opacity:0">{{ $sale->payment_mode == 'open_ticket' ? $total_open_tickets++ : $total_open_tickets }}</span>
                                <tr>
                                    <td>{{ $number++ }}</td>
                                    <td style="width:50px"><strong>{{ $sale->booking_id }}</strong></td>
                                    <td>{{ $sale->SeatNo }}</td>
                                    <td>{{ $sale->seat_category->Seat_Category_Name }}</td>
                                    <td>{{ $sale->payment_mode == 'cash' ? 'Boarding Ticket' : 'Open Ticket' }}</td>
                                    <td>{{ $sale->Pass_type }}</td>
                                    <td>{{ $sale->pass_langkawicitizen == 1 ? 'Y' : 'N' }}</td>
                                    <td>{{ is_null($sale->open_ticket_code) ? 'Cash' : $sale->open_ticket_code }}</td>
                                    <td style="text-align:right">{{ sprintf('%0.2f',$sale->ticket_price) }}</td>
                                    <td>{{ $sale->username }}</td>
                                </tr>
                                @endforeach
                                <tr>
                                    <td  style="text-align:right" colspan="8"><label> <b>GRAND TOTAL</b></label></td>
                                    <td style="text-align:right"><label><b>{{ sprintf('%0.2f',$total_amount) }}</b></label></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>  
                    
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-body">    
                    <div class="table-responsive">
                        <table class="table table-hover" border="0">
                            <thead>
                                <tr>
                                    <th style="width: 30%;">Total Transactions</th>
                                    <th style="width: 30%;">Total Seats Sold</th>
                                    <th style="width: 20%;">Total Citizens</th>
                                    <th style="width: 20%;">Total Adults</th>
                                    <th style="width: 20%;">Total Children</th>
                                    <th style="width: 20%;">Total Cash Payments</th>
                                    <th style="width: 20%;">Total Open Tickets</th>
                                </tr>
                            </thead>                        
                            <tbody>
                                <tr>
                                    <td>{{ $total_transactions }}</td>
                                    <td>{{ $total_seats }}</td>
                                    <td>{{ $total_citizen }}</td>
                                    <td>{{ $total_adults }}</td>
                                    <td>{{ $total_children }}</td>
                                    <td>{{ $total_cash_payments }}</td>
                                    <td>{{ $total_open_tickets }}</td>
                                    
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>

</div>
<script>
    // window.print();
</script>
<style>
table th{
    text-align:left;
}
</style>