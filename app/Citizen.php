<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Citizen extends Model
{
    public function origin(){

    	return $this->belongsTo('App\Jetty','origin_id');

    }

    public function destination(){

    	return $this->belongsTo('App\Jetty','destination_id');

    }      
}
