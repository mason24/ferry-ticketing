<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class User_Group extends Model
{
    public function users(){

    	return $this->hasMany('App\Users','user_group_id');

    }
}
