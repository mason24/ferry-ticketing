<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOpenTicketPricesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('open_ticket_prices', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('origin_id')->unsigned();
            $table->foreign('origin_id')->references('id')->on('jetties')->onDelete('cascade');
            $table->integer('destination_id')->unsigned();
            $table->foreign('destination_id')->references('id')->on('jetties')->onDelete('cascade');
            $table->integer('seat_category_id')->unsigned();
            $table->foreign('seat_category_id')->references('id')->on('seat_categories')->onDelete('cascade');
            $table->double('adult_price',12,2);
            $table->double('adult_citizen_price',12,2);
            $table->double('child_price',12,2);
            $table->double('child_citizen_price',12,2);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('open_ticket_prices');
    }
}
