<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePassengersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('passengers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('Message_ID',30)->nullable();
            $table->string('Company_Code',15)->nullable();
            $table->string('Trip_code',10)->nullable();
            $table->string('Jetty_Station',3)->nullable();
            $table->string('Vessel_Code',10)->nullable();
            $table->string('Route_code',10)->nullable();
            $table->integer('Company_Vst_id')->nullable();
            $table->string('Pass_name',100)->nullable();
            $table->string('Pass_type',20)->nullable();
            $table->string('pass_identity_type',20);
            $table->string('pass_identity_id',14)->nullable();
            $table->date('pass_dob')->nullable();
            $table->char('pass_gender',2)->nullable();
            $table->string('pass_nationality',3)->nullable();
            $table->char('pass_langkawicitizen',1);
            $table->string('pass_address1',500)->nullable();
            $table->string('pass_address2',200)->nullable();
            $table->string('pass_address3',100)->nullable();
            $table->string('pass_state',3)->nullable();
            $table->string('pass_postcode',6)->nullable();
            $table->string('pass_emergencyContact',10)->nullable();
            $table->string('pass_email_address',40)->nullable();
            $table->string('pass_contact_no',12)->nullable();
            $table->string('SeatNo',3)->nullable();
            $table->integer('SeatCategoryId')->nullable()->unsigned();
            $table->foreign('SeatCategoryId')->references('id')->on('seat_categories')->onDelete('cascade');
            $table->string('booking_id');
            $table->string('Boarding_Code',10)->nullable();
            $table->date('Est_Depart_Date')->nullable();
            $table->time('Est_Depart_Time')->nullable();
            $table->char('Check_In',1)->nullable();
            $table->date('Check_In_Date')->nullable();
            $table->time('Check_In_Time')->nullable();
            $table->string('open_ticket_code',50)->nullable();
            $table->tinyinteger('open_ticket_code_used')->default(0);
            $table->double('ticket_price',12,2)->default(0.00);
            $table->tinyinteger('status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('passengers');
    }
}
