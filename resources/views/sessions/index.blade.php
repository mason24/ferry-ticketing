@extends('layout.master')
@section('content')
                        <div class="box pattern pattern-sandstone">
                            <div class="box-header">
                                <i class="icon-table"></i>
                                <h5>
                                    Vessels
                                </h5>

                            </div>
                            <div class="form-group">
                                <a href="vessels/create">New Vessel</a> 
                            </div>                            
                            <div class="form-group">
                                Sort: <a href="vessels?sort=asc">Ascending</a> | <a href="jetties?sort=desc">Descending</a> 
                            </div>
                            <div class="box-content box-table">
                                <table id="sample-table" class="table table-hover table-bordered tablesorter">
                                    <thead>
                                        <tr>
                                            <th>Vessel Name</th>
                                            <th>Vessel Code</th>
                                            <th>Created Date</th>
                                            <th>Updated Date</th>
                                            <th class="td-actions"></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    	@foreach ($vessels as $vessel)
	                                        <tr>
	                                            <td>{{ $vessel->Vessel_Name }}</td>
	                                            <td>{{ $vessel->Vessel_Code }}</td>
	                                            <td>{{ $vessel->created_at }}</td>
	                                            <td>{{ $vessel->updated_at }}</td>
	                                            <td class="td-actions">
	                                                <a href="vessels/edit/{{ $vessel->id }}" class="btn btn-small btn-info">
	                                                    <i class="btn-icon-only icon-edit"></i>
	                                                </a>

	                                                <a href="vessels/delete/{{ $vessel->id }}" class="btn btn-small btn-danger">
	                                                    <i class="btn-icon-only icon-remove"></i>
	                                                </a>
	                                            </td>
	                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>

                                {{ $vessels->links() }}
                            </div>
                        </div>
@endsection