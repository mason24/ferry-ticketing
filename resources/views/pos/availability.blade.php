

  <!-- Modal -->
  <ul class="nav nav-tabs">
  	@foreach($vessel_seats as $vessel_seat)
    <li
    @if ($loop->first)
        class="active"
    @endif

    ><a data-toggle="tab" href="#seat{{ $vessel_seat->id }}">{{ $vessel_seat->seat_category->Seat_Category_Name }} {{ $balance_seats[$tripid][$vessel_seat->seat_category_id] }} / {{ $vessel_seat->capacity }}</a></li>
    @endforeach

  </ul>

  <div class="tab-content">
  	@foreach($vessel_seats as $vessel_seat)
    <div id="seat{{ $vessel_seat->id }}" class="tab-pane fade

    @if ($loop->first)
        in active
    @endif

    ">
      
	  <div class="row">
		<div class="col-md-6">
			<div class="form-group">
				<label>Total Adults</label>
				<div>
				@for($i = 1;$i <= 15;$i++)
					<button type="button" class="btn btn-primary">{{ $i }}</button>
				@endfor
				</div>
			</div>
		</div>
		<div class="col-md-6">
			<div class="form-group">
				<label>Total Adults (Citizen)</label>
				<div>
				@for($i = 1;$i <= 15;$i++)
					<button type="button" class="btn btn-primary">{{ $i }}</button>
				@endfor
				</div>
			</div>
		</div>			  
	  </div> 
	  <div class="row">
		<div class="col-md-6">
			<div class="form-group">
				<label>Total Chlldren</label>
				<div>
				@for($i = 1;$i <= 15;$i++)
					<button type="button" class="btn btn-primary">{{ $i }}</button>
				@endfor
				</div>
			</div>
		</div>
		<div class="col-md-6">
			<div class="form-group">
				<label>Total Chlldren (Citizen)</label>
				<div>
				@for($i = 1;$i <= 15;$i++)
					<button type="button" class="btn btn-primary">{{ $i }}</button>
				@endfor
				</div>
			</div>
		</div>  
	  </div> 	       
    </div>
<input type="hidden" class="adult_citizen_price_{{ $vessel_seat->seat_category_id }}" value="{{ $adult_citizen_prices[$tripid][$vessel_seat->id] }}"/>
<input type="hidden" class="child_citizen_price_{{ $vessel_seat->seat_category_id }}" value="{{ $child_citizen_prices[$tripid][$vessel_seat->id] }}"/>    
    @endforeach
    
  </div>

