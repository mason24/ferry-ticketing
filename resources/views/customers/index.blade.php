@extends('layout.master')
@section('content')
                        <div class="box pattern pattern-sandstone">
                            <div class="box-header">
                                <i class="icon-table"></i>
                                <h5>
                                    Customers
                                </h5>

                            </div>
                            <div class="form-group">
                                <a href="customer/new">New Customer</a> 
                            </div>                            
                            <div class="form-group">
                                Sort: <a href="customer?sort=asc">Ascending</a> | <a href="customer?sort=desc">Descending</a> 
                            </div>
                            <div class="box-content box-table">
                                <table id="sample-table" class="table table-hover table-bordered tablesorter">
                                    <thead>
                                        <tr>
                                            <th>Customer Name</th>
                                            <th>Contact Number</th>
                                            <th>Date of Birth</th>
                                            <th>Gender</th>
                                            <th>Created Date</th>
                                            <th>Updated Date</th>
                                            <th class="td-actions"></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    	@foreach ($customers as $customer)
	                                        <tr>
                                                <td>{{ $customer->name }}</td>
                                                <td>{{ $customer->contact_no }}</td>
                                                <td>{{ $customer->dob }}</td>
	                                            <td>{{ $customer->gender }}</td>
	                                            <td>{{ $customer->created_at }}</td>
	                                            <td>{{ $customer->updated_at }}</td>
	                                            <td class="td-actions">
	                                                <a href="customer/edit/{{ $customer->id }}" class="btn btn-small btn-info">
	                                                    <i class="btn-icon-only icon-edit"></i>
	                                                </a>

	                                                <a href="customer/delete/{{ $customer->id }}" class="btn btn-small btn-danger">
	                                                    <i class="btn-icon-only icon-remove"></i>
	                                                </a>
	                                            </td>
	                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>

                                {{ $customers->links() }}
                            </div>
                        </div>
@endsection