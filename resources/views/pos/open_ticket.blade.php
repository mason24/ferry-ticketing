@extends('pos_layout.master')
@section('content')
@if(session()->has('message'))
<div class="alert alert-success">
    {{ session()->get('message') }}
</div>
@endif
@include('pos_layout.errors')
<div id="wrapper">
    <header id="header" class="navbar">
        <div class="container">
            <a class="navbar-brand" href="{{ URL::to('/pos') }}"><span class="logo"><span class="pos-logo-lg"></span><span class="pos-logo-sm">POS</span></span></a>
            <div class="header-nav">
            </div>
        </div>
    </header>
    <div id="content">
        <div class="c1">
            <div class="pos">
                <div id="pos">
                    <form id="formOpenTicket" action="{{ URL::to('/pos/open-ticket') }}" data-toggle="validator" role="form" id="pos-sale-form" method="post" accept-charset="utf-8" target="blank">
                        {{ csrf_field() }}
                        <div id="rightdiv">
                            @if($pos_register_info !== false)
                                <input type="hidden" name="pos_register_id" value="{{ $pos_register_info->id }}"/>
                            @elseif(!empty($pos_register_id))
                                <input type="hidden" name="pos_register_id" value="{{ $pos_register_id }}"/>
                            @endif
                            <div id="left-top">
                                <div style="position: absolute; left:-9999px;"><input type="text" name="test" value="" id="test" class="kb-pad ui-keyboard-input ui-widget-content ui-corner-all" aria-haspopup="true" role="textbox"></div>
                                <div class="form-group">
                                 
                                    <div style="clear:both;"></div>
                                </div>
                                
                            </div>
                            <div id="print">
                                <div id="left-middle" style="height: 14px; min-height: 278px;">
                                    <table class="table items table-striped table-bordered table-condensed table-hover" id="posTable" style="margin-bottom: 0px; padding: 0px;">
                                        <thead class="tableFloatingHeaderOriginal">
                                            <tr>
                                                <th>
                                                    Ticket Details
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            
                                        </tbody>
                                    </table>                                
                                    <div id="product-list" class="ps-container" style="height: 9px; min-height: 248px;overflow-y:auto">

                                        <div id="ticket-details" style="overflow-y:auto;overflow-x:hidden;">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label style="font-size:20px" class="origintext"></label>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <div class="text-center">
                                                                    <label style="font-size:20px" class="journeyicon">
                                                                        <div class="fa fa-long-arrow-right"></div>
                                                                    </label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <div class="text-right">
                                                                    <label style="font-size:20px" class="destinationtext"></label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <div class="col-md-6"><div class="form-group">Expiry Date: <label class="onwarddatetext">N/A</label></div></div>
                                                        <div class="col-md-6"><div class="form-group"><div class="text-right"><label class="onwardtriptime"></label></div></div></div>
                                                    </div>
                                                </div>
                                            </div>
                                           <!--  <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <div class="col-md-6"><div class="form-group">Return: <label class="returndatetext">N/A</label></div></div>
                                                        <div class="col-md-6"><div class="form-group"><div class="text-right"><label class="returntriptime"></label></div></div></div>
                                                    </div>
                                                </div>
                                            </div> -->
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group passenger-fare-details">
                                                        <div class="panel panel-default">
                                                            <div class="panel-heading">
                                                                <label>Passenger Details</label>
                                                            </div>
                                                            <div class="panel-body">
                                                                <div class="row">
                                                                    <div class="col-md-12">
                                                                        <div class="form-group panel-passengers">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            
                                                        </div>
                                                        <!-- <div class="panel panel-default">
                                                            <div class="panel-heading">
                                                                <label>Return</label>
                                                            </div>
                                                            <div class="panel-body panel-return">
                                                            </div>
                                                            
                                                        </div> -->                                                        
                                                    </div>
                                                </div>
                                            </div> 
                                        </div>                                        
                                        <div style="clear:both;"></div>
                                        
                                    </div>
                                </div>
                                <div style="clear:both;"></div>
                                <div id="left-bottom">
                                    <table id="totalTable" style="width:100%; float:right; padding:5px; color:#000; background: #FFF;">
                                        <tbody>
                                            <tr>
                                                <td style="padding: 5px 10px;border-top: 1px solid #DDD;">Passengers</td>
                                                <td class="text-right" style="padding: 5px 10px;font-size: 14px; font-weight:bold;border-top: 1px solid #DDD;">
                                                    <!-- <span id="total-passengers">0</span> -->
                                                </td>
                                                <td style="padding: 5px 10px;border-top: 1px solid #DDD;"></td>
                                                <td class="text-right" style="padding: 5px 10px;font-size: 14px; font-weight:bold;border-top: 1px solid #DDD;">
                                                    <span id="total-passengers">0</span>
                                                </td>
                                            </tr>
                                                                                      
                                            <tr>
                                                <td style="padding: 5px 10px; border-top: 1px solid #666; border-bottom: 1px solid #333; font-weight:bold; background:#333; color:#FFF;" colspan="2">
                                                    Total Payable                                        
                                                </td>
                                                <td class="text-right" style="padding:5px 10px 5px 10px; font-size: 14px;border-top: 1px solid #666; border-bottom: 1px solid #333; font-weight:bold; background:#333; color:#FFF;" colspan="2">
                                                    <span id="gtotal">0.00</span>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <div class="clearfix"></div>
                                    <div id="botbuttons" class="col-xs-12 text-center">
                                        <input type="hidden" name="biller" id="biller" value="3">
                                        <div class="row">
                                            <!-- <div class="col-xs-4" style="padding: 0;">
                                                <div class="btn-group-vertical btn-block">
                                                    <button type="button" class="btn btn-warning btn-block btn-flat" id="suspend">
                                                        Suspend                                            
                                                    </button>

                                                    <button type="button" class="btn btn-primary btn-block" id="select_passengers" data-toggle="modal" data-target="#select_passengers_modal">
                                                            Select Passengers   
                                                    </button>
                                                    <button type="button" class="btn btn-info btn-block" id="print_order" data-toggle="modal" data-target="#passenger_details_modal">
                                                    Passenger Details 
                                                    </button>
                                                    <button type="button" class="btn btn-danger btn-block btn-flat" id="reset">
                                                        Reset                                                     
                                                    </button>


                                                      <div class="modal fade" id="passenger_details_modal" role="dialog">
                                                        <div class="modal-dialog">
                                                        
                                                          <div class="modal-content">
                                                            <div class="modal-header">
                                                              <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                              <h4 class="modal-title">Passenger Details</h4>
                                                            </div>
                                                            <div class="modal-body passenger-details-body">
                                                                
                                                            </div>
                                                            <div class="modal-footer">
                                                              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                            </div>
                                                          </div>
                                                          
                                                        </div>
                                                      </div>                                                                                                                                                              
                                                </div>
                                            </div>
                                            <div class="col-xs-4" style="padding: 0;">
                                                <div class="btn-group-vertical btn-block">
                                                


                                                </div>
                                            </div> -->
                                            <div class="col-xs-12" style="padding: 0;">
                                                <button type="button" class="btn btn-success btn-block" id="payment" style="/*height:67px;*/">
                                                <i class="fa fa-money" style="margin-right: 5px;"></i>Purchase                                            
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                    <div style="clear:both; height:5px;"></div>
                                    <div id="num">
                                        <div id="icon"></div>
                                    </div>
                                    <span id="hidesuspend"></span>
                                    <input type="hidden" name="origin" value="">
                                    <input type="hidden" name="destination" value="">
                                    <input type="hidden" name="expiry_date" value="{{ date('d-m-Y') }}">
                                    <input type="hidden" name="total_adults" value="">
                                    <input type="hidden" name="total_children" value="">
                                    <input type="hidden" name="subtotal" value="">
                                    <input type="hidden" name="gtotal" value="">
                                    <div id="payment-con">
                                        
                                    </div>
                                    <input name="order_tax" type="hidden" value="1" id="postax2">
                                </div>
                            </div>
                        </div>
                    </form>
                    <div id="cp">
                        <div id="cpinner">
                            <div class="quick-menu">
                                <div id="proContainer">
                                    <div id="ajaxproducts">
                                        <div id="item-list" style="height: 254px; min-height: 515px;">
                                            <div class="form-group">
                                                <div class="col-md-6">
                                                    <div class="panel panel-primary panel-origin">
                                                        <div class="panel-heading">
                                                            <label>Step 1</label> Select an origin
                                                        </div>
                                                        <div class="panel-body dashboard_column dashboard_latest_list" style="
                                                            height: 150px;
                                                            max-height: 150px !important;
                                                            overflow-y: auto !important;
                                                            ">
                                                            <div class="row">
                                                                @foreach($origins as $origin)
                                                                <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <button data-origin-code="{{ $origin->Jetty_Code }}" data-tooltip="{{ $origin->Jetty_Name }}" type="button" rel="{{ $origin->id }}" value="{{ $origin->id }}" style="width:100%;font-size: 20px;background-color:{{ $origin->pos_button_background_color }}" class="btn btn-default origin-button"><b {{ $origin->pos_button_background_color == '#ffffff' ? '' : 'style=color:white' }}>{{ $origin->Jetty_Code }}</b>
                                                                        </button>                                                                      
                                                                    </div>
                                                                </div>
                                                                @endforeach
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 destination-selection">
                                                    <div class="panel panel-primary">
                                                        <div class="panel-heading">
                                                            <label>Step 2</label> Select a destination
                                                        </div>
                                                        <div class="panel-body dashboard_column dashboard_latest_list" style="height: 150px;max-height: 150px !important;overflow-y: auto !important;">
                                                            <div class="row destination-list">
                                                                <div class="col-lg-12">
                                                                    <div class="form-group">
                                                                        <label>Select an origin first.</label>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="panel panel-primary panel-trip-selection">
                                                        <div class="panel-heading">
                                                            <label>Step 3</label> Open ticket details
                                                        </div>
                                                        <div class="panel-body">
                                                            <div class="row">
                                                                <div class="col-md-3">
                                                                    <div class="form-group">
                                                                        <label>Expiry Date</label>
                                                                        <input type="text" class="datepicker form-control expiry-date" style="cursor:pointer" value="{{ date('d-m-Y') }}" placeholder="Select date"/>
                                                                    </div>
                                                                </div>                                                              

                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <div class="form-group">
                                                                        <div class="passenger-selection">
                                                                
                                                                        </div> 
                                                                    </div>
                                                                </div> 
                                                                                                                              

                                                            </div>                                                            
                                                        </div>
                                                    </div>
                                                </div>
                                                                                              
                                                
                                            </div>
                                        </div>
                                        <div class="btn-group btn-group-justified pos-grid-nav">
                                            
                                        </div>
                                    </div>
                                    <div style="clear:both;"></div>
                                </div>
                            </div>
                        </div>
                        <div style="clear:both;"></div>
                    </div>
                    <div style="clear:both;"></div>
                </div>
                <div style="clear:both;"></div>
            </div>
        </div>
    </div>
</div>

 
<style>
.qty {
    width: 40px;
    height: 25px;
    text-align: center;
}
input.qtyplus { width:25px; height:25px;}
input.qtyminus { width:25px; height:25px;}
</style>
<div class="rotate btn-cat-con">
    <a href="{{ URL::to('/pos') }}" id="open-category" class="btn btn-primary open-category">POS</a>
</div>
<script src="{{ asset('js/pos/posopenticket.js') }}"></script>
@endsection