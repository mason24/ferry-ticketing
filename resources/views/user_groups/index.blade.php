@extends('layout.master')
@section('content')
                        <div class="box pattern pattern-sandstone">
                            <div class="box-header">
                                <i class="icon-table"></i>
                                <h5>
                                    User Groups
                                </h5>

                            </div>
                            <div class="form-group">
                                <a href="user_groups/create">New User Group</a> 
                            </div>                            
                            <div class="form-group">
                                Sort: <a href="user_groups?sort=asc">Ascending</a> | <a href="jetties?sort=desc">Descending</a> 
                            </div>
                            <div class="box-content box-table">
                                <table id="sample-table" class="table table-hover table-bordered tablesorter">
                                    <thead>
                                        <tr>
                                            <th>User Group Title</th>
                                            <th>Created Date</th>
                                            <th>Updated Date</th>
                                            <th class="td-actions"></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    	@foreach ($user_groups as $user_group)
	                                        <tr>
	                                            <td>{{ $user_group->title }}</td>
	                                            <td>{{ $user_group->created_at }}</td>
	                                            <td>{{ $user_group->updated_at }}</td>
	                                            <td class="td-actions">
	                                                <a href="user_groups/edit/{{ $user_group->id }}" class="btn btn-small btn-info">
	                                                    <i class="btn-icon-only icon-edit"></i>
	                                                </a>

	                                                <a href="user_groups/delete/{{ $user_group->id }}" class="btn btn-small btn-danger">
	                                                    <i class="btn-icon-only icon-remove"></i>
	                                                </a>
	                                            </td>
	                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>

                                {{ $user_groups->links() }}
                            </div>
                        </div>
@endsection