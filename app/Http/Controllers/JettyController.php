<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Jetty;

class JettyController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }     
    public function index(){

        $jetties = new Jetty;

        if(request()->has('sort')){

            $jetties = $jetties->orderBy('Jetty_Name',request('sort'));

        }

    	
        $jetties = $jetties->paginate(20)->appends([

            'sort' => request('sort')

        ]);

    	return view('jetties.index',compact('jetties'));
    }

    public function create(){

        return view('jetties.create');

    }

    public function store(){

        $this->validate(request(),[

            'Jetty_Name' => 'required',

            'Jetty_Code' => 'required'

        ]);

        $jetty = new Jetty;

        $jetty->Jetty_Name = request('Jetty_Name');

        $jetty->Jetty_Code = request('Jetty_Code');

        $jetty->pos_button_background_color = request('pos_button_background_color');

        $jetty->save();

        return redirect('jetties');


    }

    public function show($id){

        $jetties = new Jetty;

        $jetty = $jetties->findOrFail($id);

        return view('jetties.edit')->withTask($jetty);
    }

    public function update($id){

        $jetties = new Jetty;

        $this->validate(request(),[

            'Jetty_Name' => 'required',

            'Jetty_Code' => 'required'

        ]);

        $jetty = $jetties->find($id);

        $jetty->Jetty_Name = request('Jetty_Name');

        $jetty->Jetty_Code = request('Jetty_Code');

        $jetty->pos_button_background_color = request('pos_button_background_color');

        $jetty->save();

        return redirect('jetties')->with('message', 'Jetty was successfully saved');

    }    

    public function delete($id){

        $jetties = new Jetty;

        $jetty = $jetties->find($id);

        $jetty->delete();

        return redirect()->back();

    }
}
