@extends('layout.master')
@section('content')
                        <div class="box pattern pattern-sandstone">
                            <div class="box-header">
                                <i class="icon-table"></i>
                                <h5>
                                    Routes
                                </h5>

                            </div>
                            <div class="form-group">
                                <a href="routes/create">New Route</a> 
                            </div>    
                            <div class="form-group">                        
                                Sort: <a href="routes?sort=asc">Ascending</a> | <a href="routes?sort=desc">Descending</a>
                            </div>
                            <div class="box-content box-table">
                                <table id="sample-table" class="table table-hover table-bordered tablesorter">
                                    <thead>
                                        <tr>
                                            <th>Route Code</th>
                                            <th>Route Name</th>
                                            <th>From</th>
                                            <th>To</th>
                                            <th>Company Code</th>
                                            <th>Created Date</th>
                                            <th>Updated Date</th>
                                            <th class="td-actions"></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    	@foreach ($routes as $route)
	                                        <tr>
	                                            <td>{{ $route->Route_Code }}</td>
	                                            <td>{{ $route->Route_Name }}</td>
                                                <td>{{ $route->route_from->Jetty_Name }}</td>
                                                <td>{{ $route->route_to->Jetty_Name }}</td>
	                                            <td>{{ $route->Company_Code }}</td>
	                                            <td>{{ $route->created_at }}</td>
	                                            <td>{{ $route->updated_at }}</td>
	                                            <td class="td-actions">
	                                                <a href="routes/edit/{{ $route->id }}" class="btn btn-small btn-info">
	                                                    <i class="btn-icon-only icon-edit"></i>
	                                                </a>

	                                                <a href="routes/delete/{{ $route->id }}" class="btn btn-small btn-danger">
	                                                    <i class="btn-icon-only icon-remove"></i>
	                                                </a>
	                                            </td>
	                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>

                                {{ $routes->links() }}
                            </div>
                        </div>
@endsection