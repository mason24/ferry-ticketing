@extends('layout.master')
@section('content')
    @include('layout.errors')
    <form method="post" action="{{ URL::to('/settings/open-ticket/new') }}">
        {{ csrf_field() }}
        <div class="row">
            <div id="acct-password-row" class="span7">
                <fieldset>
                    <legend>New Open Ticket Price</legend><br>

                    <div class="control-group ">
                        <label class="control-label">From <span class="required">*</span></label>
                        <div class="controls">
                            <select name="origin" class="chosen span4">
                                <option>--Choose--</option>
                                @foreach($jetties as $jetty)
                                    <option value="{{ $jetty->id }}">{{ $jetty->Jetty_Name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="control-group ">
                        <label class="control-label">To <span class="required">*</span></label>
                        <div class="controls">
                            <select name="destination" class="chosen span4">
                                <option>--Choose--</option>
                                @foreach($jetties as $jetty)
                                    <option value="{{ $jetty->id }}">{{ $jetty->Jetty_Name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="control-group ">
                        <label class="control-label">Seat Category <span class="required">*</span></label>
                        <div class="controls">
                            <select name="seat_category" class="chosen span4">
                                <option>--Choose--</option>
                                @foreach($seat_categories as $seat_category)
                                    <option value="{{ $seat_category->id }}">{{ $seat_category->Seat_Category_Name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>                    
                    <div class="control-group ">
                        <label class="control-label">Adult Price <span class="required">*</span></label>
                        <div class="controls">
                            <div class="input-prepend">
                                <span class="add-on">RM</span>
                                <input class="span2" type="text" name="adult_price">
                            </div>
                        </div>
                    </div>
                    <div class="control-group ">
                        <label class="control-label">Adult Citizen Price <span class="required">*</span></label>
                        <div class="controls">
                            <div class="input-prepend">
                                <span class="add-on">RM</span>
                                <input class="span2" type="text" name="adult_citizen_price">
                            </div>
                        </div>
                    </div>

                    <div class="control-group ">
                        <label class="control-label">Child Price <span class="required">*</span></label>
                        <div class="controls">
                            <div class="input-prepend">
                                <span class="add-on">RM</span>
                                <input class="span2" type="text" name="child_price">
                            </div>
                        </div>
                    </div>
                    <div class="control-group ">
                        <label class="control-label">Child Citizen Price <span class="required">*</span></label>
                        <div class="controls">
                            <div class="input-prepend">
                                <span class="add-on">RM</span>
                                <input class="span2" type="text" name="child_citizen_price">
                            </div>
                        </div>
                    </div>                                                                                                  
                </fieldset>
            </div>
        </div>
        <footer id="submit-actions" class="form-actions">
            <button id="submit-button" type="submit" class="btn btn-primary" value="">Create</button>
            <a href="{{ URL::to('/settings/open-ticket') }}" class="btn btn-default">Back</a>
        </footer>
    </form>
@endsection