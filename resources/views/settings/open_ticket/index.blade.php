@extends('layout.master')
    @section('content')
        @if(session()->has('message'))
            <div class="alert alert-success">
                {{ session()->get('message') }}
            </div>
        @endif

                        <div class="box pattern pattern-sandstone">
                            <div class="box-header">
                                <i class="icon-table"></i>
                                <h5>
                                    Open Ticket Prices
                                </h5>

                            </div>
                            <div class="form-group">
                                <a href="open-ticket/new">New Open Ticket Price</a> 
                            </div>                            
                            <div class="form-group">
                                Sort: <a href="settings/open-ticket?sort=asc">Ascending</a> | <a href="settings/open-ticket?sort=desc">Descending</a> 
                            </div>
                            <div class="box-content box-table">
                                <table id="sample-table" class="table table-hover table-bordered tablesorter">
                                    <thead>
                                        <tr>
                                            <th>From</th>
                                            <th>To</th>
                                            <th>Seat Category</th>
                                            <th>Adult Price (RM)</th>
                                            <th>Adult Citizen Price (RM)</th>
                                            <th>Child Price (RM)</th>
                                            <th>Child Citizen Price (RM)</th>
                                            <th class="td-actions"></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($open_ticket_prices as $open_ticket_price)
                                            <tr>
                                                <td>{{ $open_ticket_price->origin->Jetty_Name }}</td>
                                                <td>{{ $open_ticket_price->destination->Jetty_Name }}</td>
                                                <td>{{ $open_ticket_price->seat_category->Seat_Category_Name }}</td>
                                                <td>{{ sprintf('%0.2f',$open_ticket_price->adult_price) }}</td>
                                                <td>{{ sprintf('%0.2f',$open_ticket_price->adult_citizen_price) }}</td>
                                                <td>{{ sprintf('%0.2f',$open_ticket_price->child_price) }}</td>
                                                <td>{{ sprintf('%0.2f',$open_ticket_price->child_citizen_price) }}</td>
                                                <td class="td-actions">
                                                    <a href="settings/open-ticket/edit/{{ $open_ticket_price->id }}" class="btn btn-small btn-info">
                                                        <i class="btn-icon-only icon-edit"></i>
                                                    </a>

                                                    <a href="settings/open-ticket/delete/{{ $open_ticket_price->id }}" class="btn btn-small btn-danger">
                                                        <i class="btn-icon-only icon-remove"></i>
                                                    </a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>

                                {{ $open_ticket_prices->links() }}
                            </div>
                        </div>
@endsection