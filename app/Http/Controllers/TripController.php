<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;

use App\Trip;

use App\Jetty;

use App\Vessel;

use App\Route;

use App\TripPrices;

use App\SeatCategory;

class TripController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }     
    public function index(){

        $origin = $destination = $vessel_id = $selected_depart_date = $depart_time = $arrival_time = '';

        $vessel = new Vessel;

        $jetty = new Jetty;

        $url = request()->fullUrlWithQuery(

            ["sort"=>request('sort')],

            ["trip_code"=>request('trip_code')],

            ["vessel"=>request('vessel')]

        );

        $trips = new Trip;

        if(request()->has('sort')){

            $trips = $trips->orderBy('Trip_Name',request('sort'));

        }

        if(request()->has('origin')){

            // DB::enableQueryLog();

            $routes = Route::select('id')->where('Route_From','=',request('origin'))->get();

            foreach($routes as $route){

                $trips = $trips->where('Route_Id','=',$route->id);

            }

            // dd(DB::getQueryLog());

            $origin = request('origin');

        }

        if(request()->has('destination')){

            $routes = Route::select('id')->where('Route_To','=',request('destination'))->get();

            foreach($routes as $route){

                $trips = $trips->where('Route_Id','=',$route->id);

            }

            $destination = request('destination');

        }

        if(request()->has('vessel')){

            if(request('vessel') != 0){

                $trips = $trips->where('Vessel_Id','=',request('vessel'));

                $vessel_id = request('vessel');

            }
            

        }        

        if(request()->has('date')){

            $selected_depart_date = request('date');

            $depart_date = date('Y-m-d', strtotime(request('date')));

            $trips = $trips->where('Depart_Date','like','%'.$depart_date.'%');

        }

        if(request()->has('departure_datetime')){

            $depart_time = request('departure_datetime');

            $trips = $trips->where('Depart_Time','like','%'.request('departure_datetime').'%');

        } 

        if(request()->has('arrival_datetime')){

            $arrival_time = request('arrival_datetime');

            $trips = $trips->where('Arvl_Time','like','%'.request('arrival_datetime').'%');

        }                                   

        
        $trips = $trips->paginate(20)->appends([

            'sort' => request('sort'),

            'trip_code' => request('trip_code')

        ]);

        $vessels = $vessel->orderBy('Vessel_Name')->get();

        $jetties = $jetty->orderBy('Jetty_Name')->get();

        return view('trips.index',compact('trips','jetties','url','vessels','origin','destination','vessel_id','selected_depart_date','depart_time','arrival_time'));

        // return redirect('/trips')->with('trips','url');
    }

    public function store(){

        $this->validate(request() , [

         // 'Trip_Code' => 'required',

         // 'Trip_Name' => 'required',
         
         'vessel' => 'required|not_in:0',

         'Depart_Date' => 'required',

         'Arrival_Date' => 'required',

         'Depart_Time_Hour' => 'required|not_in:--Hour--',

         'Depart_Time_Minute' => 'required|not_in:--Minutes--',

         'Arrival_Time_Hour' => 'required|not_in:--Hour--',
         
         'Arrival_Time_Minute' => 'required|not_in:--Minutes--'

        ]);

        $route = Route::select('Route_From','Route_To')->where('id','=',request('route'))->first();        

        $Trip = new Trip;

        $Trip->Message_ID = '111';

        $Trip->Company_Code = '200';

        // $Trip->Trip_Code = request('Trip_Code');

        // $Trip->Trip_Name = request('Trip_Name');

        $Trip->Trip_Code = 'default';

        $Trip->Trip_Name = 'default';        

        $Trip->Vessel_Id = request('vessel');

        $Trip->Route_Id = request('route');

        $Trip->Depart_Date = date('Y-m-d', strtotime(request('Depart_Date')));

        $Trip->Arvl_Date = date('Y-m-d', strtotime(request('Arrival_Date')));

        $Trip->Depart_Time = request('Depart_Time_Hour').':'.request('Depart_Time_Minute');

        $Trip->Arvl_Time = request('Arrival_Time_Hour').':'.request('Arrival_Time_Minute');

        $Trip->status = request('status');

        $Trip->save();

        $trip_id = $Trip->id;

        $Trip->where('id', $trip_id)->update([

            'Trip_Code' => $route->route_from->Jetty_Code . $route->route_to->Jetty_Code . $trip_id,

            'Trip_Name' => $route->route_from->Jetty_Code . $route->route_to->Jetty_Code . $trip_id

            ]);          

        $trip_price = new TripPrices;

        $adult_seats = request('adult-price');

        $child_seats = request('child-price');

        foreach($adult_seats as $seat_id => $price){

            $trip_price->insert([
                [
                    'trip_id' => $trip_id,

                    'vessel_seat_id' => $seat_id,

                    'adult_price' => (empty($price) ? 0 : $price),

                    'child_price' => (empty($child_seats[$seat_id]) ? 0 : $child_seats[$seat_id])
                ]
            ]); 
                         
        }

        return redirect('/trips');

    }

    function create(){

        $vessel = new Vessel;

        $route = new Route;

        $vessels = $vessel->orderBy('Vessel_Name')->get();  

        $routes = $route->orderBy('Route_Name')->get();        

        return view('trips.create', ['vessels'=> $vessels , 'routes'=>$routes]);     

    }

    public function show($id){

        $trip = new Trip;

        $trip_price = new TripPrices;

        $vessel = new Vessel;

        $route = new Route;

        $vessels = $vessel->orderBy('Vessel_Name')->get();  

        $routes = $route->orderBy('Route_Name')->get(); 

        $trip_info = $trip->findOrFail($id);

        $trip_price_info = $trip_price->findOrFail($id);   

        $seats_array = $this->seats($trip_info->Vessel_Id);    

        // return view('trips.edit', ['vessels'=> $vessels , 'routes'=>$routes , 'trip_info'=>$trip_info , 'trip_price_info'=>$trip_price_info]);     
        return view('trips.edit', compact('vessels','routes','trip_info','trip_price_info','seats_array'));     
    }

    public function delete($id){

        $trip = new Trip;

        $trip = $trip->find($id);

        $trip->delete();

        DB::table('trip_prices')->where('trip_id', '=', $id)->delete();

        return redirect('/trips');
    }

    public function seats($vessel_id){

        $seats_array = array();

        $seats = DB::table('vessel__seats')->select('*')->where('vessel_id','=',$vessel_id)->get();

        foreach($seats as $seat){
            
            $seat_category = SeatCategory::select('Seat_Category_Name')->where('id','=',$seat->seat_category_id)->first();

            array_push($seats_array,array(

                'id' => $seat->id,

                'seat_category_name' => $seat_category->Seat_Category_Name,

                'vessel_id' => $seat->vessel_id,

                'seat_category_id' => $seat->seat_category_id,

                'capacity' => $seat->capacity

            ));

        }

        return $seats_array;

    }    
}
