<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Company;

use App\OpenTicketPrice;

use App\Jetty;

use App\SeatCategory;

class SettingController extends Controller
{
    public function system(){

    	return view('settings.system');
    	
    }
    public function company(){

    	$company_info = Company::get();

    	return view('settings.company',compact('company_info'));
    	
    }
    public function open_ticket_index(){

        $open_ticket_price = new OpenTicketPrice;

        
        $open_ticket_prices = $open_ticket_price->paginate(20)->appends([

            'sort' => request('sort')

        ]);        

    	return view('settings.open_ticket.index',compact('open_ticket_prices'));
    	
    }  

    public function open_ticket_create(){

        $Jetty = new Jetty;

        $jetties = $Jetty->orderBy('Jetty_Name')->get();

        $seat_category = new SeatCategory;

        $seat_categories = $seat_category->orderBy('Seat_Category_Name')->get();        

        return view('settings.open_ticket.create',compact('jetties','seat_categories'));

    }

    public function open_ticket_store(){

        $this->validate(request() , [

            'origin' => 'required|not_in:--Choose--',

            'destination' => 'required|not_in:--Choose--',

            'adult_price' => 'required',

            'adult_citizen_price' => 'required',

            'child_price' => 'required',

            'child_citizen_price' => 'required'

        ]);

        $open_ticket_price = new OpenTicketPrice;

        $open_ticket_price->insert([
            [
                'origin_id' => request('origin'),

                'destination_id' => request('destination'),

                'seat_category_id' => request('seat_category'),

                'adult_price' => request('adult_price'),

                'adult_citizen_price' => request('adult_citizen_price'),

                'child_price' => request('child_price'),
                
                'child_citizen_price' => request('child_citizen_price'),
            ]
        ]);         

        return redirect('/settings/open-ticket');

    }      
}
