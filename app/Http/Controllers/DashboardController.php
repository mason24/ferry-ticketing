<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Dashboard;

use App\Booking;

use App\Trip;


class DashboardController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }  
    public function index(){

    	$total_bookings_today = Booking::where('booking_status' , '=' , 1)
    									->where('created_at' , 'like' , '%'.date('Y-m-d').'%')
    									->count();

    	$total_revenue_today = Booking::where('booking_status', '=', 1)
    							->where('created_at' , 'like' , '%'.date('Y-m-d').'%')
    							->sum('grand_total_amount');

    	$total_departures_today = Trip::where('Depart_Date' , '=' , date('Y-m-d'))
    									->where('Depart_Time' , '>=' , date('H:i'))
    									->where('status' , '=' , 1)
    									->count();

        $total_revenue_january = Booking::where('created_at','like','%'.date('Y').'-01%')
                                          ->sum('grand_total_amount');

        $total_revenue_february = Booking::where('created_at','like','%'.date('Y').'-02%')
                                          ->sum('grand_total_amount');

        $total_revenue_march = Booking::where('created_at','like','%'.date('Y').'-03%')
                                          ->sum('grand_total_amount'); 

        $total_revenue_april = Booking::where('created_at','like','%'.date('Y').'-04%')
                                          ->sum('grand_total_amount');                                                                                                                  
        $total_revenue_may = Booking::where('created_at','like','%'.date('Y').'-05%')
                                          ->sum('grand_total_amount');

        $total_revenue_june = Booking::where('created_at','like','%'.date('Y').'-06%')
                                          ->sum('grand_total_amount'); 

        $total_revenue_july = Booking::where('created_at','like','%'.date('Y').'-07%')
                                          ->sum('grand_total_amount'); 

        $total_revenue_august = Booking::where('created_at','like','%'.date('Y').'-08%')
                                          ->sum('grand_total_amount'); 

        $total_revenue_september = Booking::where('created_at','like','%'.date('Y').'-09%')
                                          ->sum('grand_total_amount'); 

        $total_revenue_october = Booking::where('created_at','like','%'.date('Y').'-10%')
                                          ->sum('grand_total_amount');

        $total_revenue_november = Booking::where('created_at','like','%'.date('Y').'-11%')
                                          ->sum('grand_total_amount');  

        $total_revenue_december = Booking::where('created_at','like','%'.date('Y').'-12%')
                                          ->sum('grand_total_amount');                                                                                                                                                                                                                                                                                                    

    	return view('dashboard.index',compact(
	    				'total_bookings_today',
	    				'total_revenue_today',
	    				'total_departures_today',
                        'total_revenue_january',
                        'total_revenue_february',
                        'total_revenue_march',
                        'total_revenue_april',
                        'total_revenue_may',
                        'total_revenue_june',
                        'total_revenue_july',
                        'total_revenue_august',
                        'total_revenue_september',
                        'total_revenue_october',
                        'total_revenue_november',
                        'total_revenue_december'
    				));

    }
}
