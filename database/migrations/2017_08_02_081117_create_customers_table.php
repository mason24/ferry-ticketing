<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name',100);
            $table->string('id_type',20);
            $table->string('id_no',14);
            $table->date('dob');
            $table->string('gender',2);
            $table->string('nationality',100);
            $table->text('add1');
            $table->text('add2')->nullable();
            $table->text('add3')->nullable();
            $table->string('state',50);
            $table->string('postcode',50);
            $table->text('email')->nullable();
            $table->text('contact_no');
            $table->text('emergency_contact_no')->nullable();
            $table->integer('customer_group_id')->default(0)->nullable()->unsigned();
            $table->foreign('customer_group_id')->references('id')->on('customer_groups')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customers');
    }
}
