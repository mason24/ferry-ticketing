@extends('layout.master')
@section('content')
    @include('layout.errors')
    <form method="post" action="{{ URL::to('/jetties') }}">
        {{ csrf_field() }}
        <div class="row">
            <div id="acct-password-row" class="span7">
                <fieldset>
                    <legend>New Booking</legend><br>
                    <div class="control-group ">
                        <label for="Jetty_Name" class="control-label">Date</label>
                        <div class="input-prepend date datepicker">
                            <span class="add-on"><i class="icon-th"></i></span>
                            <input class="span2" type="text" value="{{ date('m/d/Y') }}" readonly="readonly" style="cursor:pointer">
                        </div>  
                    </div>

                    <div class="control-group ">
                        <label for="from" class="control-label">From</label>
                        <div class="controls">
                            <select class="chosen span2" data-placeholder="Select">
                                <option value="">--Choose--</option>
                                @foreach($jetties as $jetty)
                                <option value="{{ $jetty->id }}">{{ $jetty->Jetty_Name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="control-group ">
                        <label for="to" class="control-label">To</label>
                        <div class="controls">
                            <select class="chosen span2" data-placeholder="Select" onchange="testAJAX()">
                                <option value="">--Choose--</option>
                                @foreach($jetties as $jetty)
                                <option value="{{ $jetty->id }}">{{ $jetty->Jetty_Name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="control-group ">
                        <label for="to" class="control-label">Ticket Types</label>
                        <div class="controls">
                            <select class="span2" name="total_adults">
                                @for($i=1;$i<=100;$i++)
                                <option>{{ $i }} Adult(s)</option>
                                @endfor
                            </select>
                            <select class="span2" name="total_children">
                                @for($i=0;$i<=100;$i++)
                                <option>{{ $i }} Children</option>
                                @endfor
                            </select>

                        </div>
                    </div>
                    <!-- <div class="control-group ">
                        <label for="to" class="control-label">Return Ticket <input type="checkbox" name="return_ticket_selected" /></label>
                        
                    </div> -->                    
                    <div class="control-group ">
                        <label for="to" class="control-label">Subtotal</label>
                        <div class="input-prepend date">
                            <span class="add-on">RM</span>
                            <input class="span2" type="text" readonly="readonly" name="subtotal">
                        </div>
                    </div>
                    <div class="control-group ">
                        <label for="to" class="control-label">Tax</label>
                        <div class="input-prepend date">
                            <span class="add-on">RM</span>
                            <input class="span2" type="text" readonly="readonly" name="tax">
                        </div>
                    </div>
                    <div class="control-group ">
                        <label for="to" class="control-label">Total</label>
                        <div class="input-prepend date">
                            <span class="add-on">RM</span>
                            <input class="span2" type="text" readonly="readonly" name="total">
                        </div>
                    </div>
                    <div class="control-group ">
                        <label for="to" class="control-label">Status</label>
                        <div class="controls">
                            <select class="span2" name="booking_status">
                                <option>Confirmed</option>
                                <option>Pending</option>
                                <option>Cancelled</option>
                            </select>
                        </div>
                    </div>                                                                                                                                                          
                </fieldset>
            </div>
            <div id="acct-verify-row" class="span9">
                <fieldset>
                    <legend>Payer Details</legend>

                    <div class="control-group ">
                        <label class="control-label">Name</label>
                        <div class="controls">
                            <input name="payer_name" class="span3" type="text" value="" autocomplete="false">

                        </div>
                    </div>
                    <div class="control-group ">
                        <label class="control-label">Phone Number</label>
                        <div class="controls">
                            <input name="payer_phone_number" class="span3" type="text" value="" autocomplete="false">
                        </div>
                    </div>

                </fieldset>
            </div>            
            <div class="span8">
            </div>
        </div>
        <footer id="submit-actions" class="form-actions">
            <button id="submit-button" type="submit" class="btn btn-primary" value="">Create Booking</button>
            <a href="{{ URL::to('/jetties') }}" class="btn btn-default">Back</a>
        </footer>
    </form>
    <script>



        function testAJAX(){
            var url = "/ferry/public/bookings/ajax";

            $.ajax({
                url: url,
                success: function (data) {
                    alert(data);
                },
                error: function (data) {
                    console.log('Error:', data);
                }
            }); 
        }

       
    </script>    
@endsection