<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User_Group;

class UserGroupController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    } 
        
    public function index(){

        $user_groups = new User_Group;

        if(request()->has('sort')){

            $user_groups = $user_groups->orderBy('title',request('sort'));

        }

    	
        $user_groups = $user_groups->paginate(20)->appends([

            'sort' => request('sort')

        ]);

    	return view('user_groups.index',compact('user_groups'));
    }

    public function create(){

        return view('user_groups.create');

    }

    public function store(){

        $this->validate(request(),[

            'title' => 'required'

        ]);

        $user_group = new User_Group;

        $user_group->title = request('title');

        $user_group->save();

        return redirect('user_groups');


    }

    public function show($id){

        $user_groups = new User_Group;

        $user_group = $user_groups->findOrFail($id);

        return view('user_groups.edit')->withTask($user_group);
    }

    public function update($id){

        $user_groups = new User_Group;

        $this->validate(request(),[

			'title' => 'required'

        ]);

        $user_group = $user_groups->find($id);

        $user_group->title = request('title');      

        $user_group->save();

        return redirect('user_groups');

    }   

    public function delete($id){

        $user_groups = new User_Group;

        $user_group = $user_groups->find($id);

        $user_group->delete();

        return redirect()->back();

    }
}
