<script src="{{ asset('js/pos/jquery.js') }}"></script>
<script src="{{ asset('js/jquery-barcode.min.js') }}"></script>
<button style="    color: #fff;
    background-color: #337ab7;
    border-color: #2e6da4;" type="button" onclick="    
    window.opener.location = document.referrer;
    window.close();">Back</button>
@foreach($boarding_passes as $boarding_pass)
            <div class="printingBox" style="display:none;padding: 0px 20px;">


                <table style="float:left;border-right: 2px dashed #333;padding-right: 10px;width: 390px;">
                    <tbody>
                        <tr>
                            <td colspan="2" style="width: 60%;"><img style="display:inline" src="https://www.dragonstarship.com/images/logo.png" alt="" height="50" /></td>
                            <td colspan="2" style="font-size: 14px;vertical-align:top;text-transform:uppercase;width: 40%;"><b>Boarding Pass</b><br><b style="font-weight:900">({{ $boarding_pass->passenger->Pass_type }})</b></td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <b style="font-size:12px">Name</b><br>
                                <span>{{ $boarding_pass->passenger->Pass_name }} {{ $boarding_pass->passenger->pass_langkawicitizen == 1 ? '(Citizen)' : '' }}</span>            
                            </td>
                            
                            

                                            
                        </tr>
                        <tr>
                            <td style="width: 33.333%;">
                                <b style="font-size:12px">From</b><br>
                                    
                                <span style="font-size:13px">{{ $booking->origin->Jetty_Name }}</span>            
                            </td>
                            <td style="padding-left:10px;width: 33.333%;">
                                <b style="font-size:12px">Vessel</b><br>
                                {{ $booking->onward_trip->vessel->Vessel_Name }}
                            </td>
                            <td style="padding-left:10px;width: 33.333%;"><b style="font-size:12px">Seat</b><br><span style="font-size:13px">
                            {{ $boarding_pass->passenger->seat_category->Seat_Category_Name }}</span></td>
                            
                            
                            
                        </tr>
                        <tr>
                            <td style="width: 33.333%;">
                                <b style="font-size:12px">To</b><br>
                                <span  style="font-size:13px">{{ $booking->destination->Jetty_Name }}</span>          
                            </td>
                            <td style="padding-left:10px;width: 33.333%;">
                                <b style="font-size:12px">Date</b><br>
                                {{ $booking->onward_trip->Depart_Date }}
                            </td>               
                            <td style="padding-left:10px;width: 33.333%;"><b style="font-size:12px">Price</b><br><span style="font-size:13px">RM {{ sprintf('%0.2f',$boarding_pass->passenger->ticket_price) }}</span></td>
                        </tr>
                        <tr>
                            <td style="width: 33.333%;">
                                <b style="font-size:12px">Booking ID</b><br>
                                <span style="font-size:13px">{{ $boarding_pass->passenger->booking_id }}</span>           
                            </td>
                            <td style="padding-left:10px;width: 33.333%;">
                                <b style="font-size:12px">Boarding Time</b><br>
                                {{ $booking->boarding_time }}
                            </td>
                        </tr>
                        <tr>
                            <td style="width:100%" colspan="3">
                                <span style="font-size:11px">You must be at the gate by the boarding time indicated above.<br>Failure to do so may result in travel being denied.</span><br>
                                <!--<div style="margin-top:10px"class="qrcodeCanvas"></div>-->
                            </td>
                            
                        </tr>
                        <!-- <tr>
                            <td style="border-right: none;">
                                <button style="background: red;color: white;padding: 6 12;border-radius: 5px;border: none;cursor:pointer;clear: both;" onclick="window.print()">Print</button>
                            </td>
                            
                        </tr> -->                       
                    </tbody>
                </table>
                
                <!--<span style="font-weight:bold;font-size:15px;text-transform:uppercase">Boarding Pass</span>-->
                <table style="padding-left: 10px;width: 200px;max-width: 200px;">
                    <tbody>
                        <tr>
                            <td valign="top" style="width: 50%;"><b style="font-size:12px">Name</b> </td>
                            <td style="width: 50%;"><span>
                            {{ (strlen($boarding_pass->passenger->Pass_name) > 10 ? substr($boarding_pass->passenger->Pass_name, 0 , 7).'...' : $boarding_pass->passenger->Pass_name) }}
                            ({{ $boarding_pass->passenger->Pass_type }}) {{ $boarding_pass->passenger->pass_langkawicitizen == 1 ? 'Citizen' : '' }}</span></td>
                        </tr>               
                        <tr>
                            <td valign="top" style="width: 50%;"><b style="font-size:12px">Vessel</b></td>
                            <td style="font-size:12px;width: 50%;">{{ $booking->onward_trip->vessel->Vessel_Name }}</td>
                        </tr>               
                        <tr>
                            <td valign="top" style="width: 50%;"><b style="font-size:12px">Date</b></td>
                            <td style="font-size:12px;width: 50%;">{{ $booking->onward_trip->Depart_Date }}</td>
                        </tr>
                        <tr>
                            <td valign="top" style="width: 50%;"><b style="font-size:12px">From</b></td>
                            <td style="font-size:12px;width: 50%;">{{ $booking->origin->Jetty_Name }}</td>
                        </tr>
                        <tr>
                            <td valign="top" style="width: 50%;"><b style="font-size:12px">To</b></td>
                            <td style="font-size:12px;width: 50%;">{{ $booking->destination->Jetty_Name }}</td>
                        </tr>
                        <tr>
                            <td valign="top" style="width: 50%;"><b style="font-size:12px">Booking ID</b></td>
                            <td style="font-size:12px;width: 50%;">{{ $booking->booking_id }}</td>
                        </tr>               
                        <tr>
                            <td valign="top" style="width: 50%;"><b style="font-size:12px">Boarding Time</b></td>
                            <td style="font-size:12px;width: 50%;">{{ $booking->boarding_time }}</td>
                        </tr>
                        <tr>
                            <td valign="top" style="width: 50%;"><b style="font-size:12px">Seat No.</b></td>
                            <td style="font-size:12px;width: 50%;">{{ $boarding_pass->passenger->SeatNo }}</td>
                        </tr>
                        <tr>
                            <td valign="top" style="width: 50%;"><b style="font-size:12px">Price</b></td>
                            <td style="font-size:12px;width: 50%;">RM {{ sprintf('%0.2f',$boarding_pass->passenger->ticket_price) }}</td>
                        </tr>
                        <tr>
                            <td colspan="2"><div id="barcode_{{ $boarding_pass->boarding_pass_id }}"></div></td>
                        </tr>            
                    </tbody>
                </table>   
              <script>
              $("#barcode_{{ $boarding_pass->boarding_pass_id }}").barcode("{{ $boarding_pass->boarding_pass_id }}", "code93", {barWidth:1, barHeight:30});        
              </script>  
            </div>
            <div class="next-page"></div>
@endforeach            
<style>
button {
    display: inline-block;
    padding: 70px;
    margin-bottom: 0;
    font-size: 70px;
    font-weight: 400;
    line-height: 1.42857143;
    text-align: center;
    white-space: nowrap;
    -ms-touch-action: manipulation;
    touch-action: manipulation;
    cursor: pointer;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
    border: 1px solid transparent;
    border-radius: 4px;
}
body , table tr td{
    font-family:arial,sans-serif;
    font-size:14px;
}
.barcode canvas {
    height:40px;
    width: 200px;
}
@media print {
    button{
        display:none;
    }
    .printingBox{
        display: block !important;
    }
    .next-page{
        page-break-after: always;
    }
}
</style>
<script>
window.print()
</script>