<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('companies', function (Blueprint $table) {
            $table->increments('id');
            $table->string('Company_Code',15);
            $table->string('Company_Name',100);
            $table->string('Company_Address1',500);
            $table->string('Company_Address2',200);
            $table->string('Company_Address3',100);
            $table->string('Postcode',6);
            $table->char('State', 3);
            $table->string('Contact_Person',100);
            $table->string('Contact_No',12);
            $table->string('Email_Address',40);
            $table->char('Active',1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('companies');
    }
}
