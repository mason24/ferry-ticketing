<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BoardingPass extends Model
{
    public function passenger(){

    	return $this->belongsTo('App\Passenger','passenger_id');

    }
}
