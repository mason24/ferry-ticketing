var site_url = window.location.href;


   
$(document).ready(function(){


	$(window).keydown(function(event){
		if(event.keyCode == 13) {

		  event.preventDefault();

		  return false;

		}
	});

	$(".origin-button").click(function () {

			$('input[name=\'destination\']').val('');

			$('.destinationtext').html('');

			$('.onwarddatetext').text('N/A');		

			$('.onwardtriptime').empty();		
		
	    	var originID = $(this).attr('rel');

	    	$('input[name="origin"]').val(originID);

	    	$('.origintext').html($(this).attr('data-origin-code'));

	        var url = site_url+"/destination/"+originID;

	        $.ajax({
	            url: url,
	            success: function (data) {
	                $('.destination-list').html(data);
	            },
	            error: function (data) {
	                console.log('Error:', data);
	            }
	        }); 

	        $('#open-ticket-code').focus();

	        $('.journeyicon').hide();

	        $('.onward-trips').empty();

	        $('.panel-onward').empty();
	    

	});

	$(".onward-trip-date").change(function () {

			$('.onwarddatetext').html($('.onward-trip-date').val());

			$('input[name=\'onward_date\']').val($('.onward-trip-date').val());

			var originID = $('input[name="origin"]').val();

			var destinationID = $('input[name="destination"]').val();

	        var url = site_url+"/trips/"+originID+"/"+destinationID+"/onward/"+$('.onward-trip-date').val();

	        $.ajax({
	            url: url,
	            success: function (data) {
	                $('.onward-trips').html(data);
	            },
	            error: function (data) {
	                console.log('Error:', data);
	            }
	        }); 
	    
	    $('#open-ticket-code').focus();

	    $('.panel-onward').empty();

	    $('.onwardtriptime').empty();

	});	

	$(".return-trip-date").change(function () {

			$('.returndatetext').html($('.return-trip-date').val());

			$('input[name=\'return_date\']').val($('.return-trip-date').val());		

			var originID = $('input[name="origin"]').val();

			var destinationID = $('input[name="destination"]').val();		

	        var url = site_url+"/trips/"+destinationID+"/"+originID+"/return/"+$('.return-trip-date').val();

	        $.ajax({
	            url: url,
	            success: function (data) {
	                $('.return-trips').html(data);
	            },
	            error: function (data) {
	                console.log('Error:', data);
	            }
	        }); 

	    disable_return_trips();

	    $('#open-ticket-code').focus();

	});	

	$("#payment").click(function () {

			$("#payment").prop('disabled' , true);

			$("#payment").text('Processing...');

			$('#formPOS').submit();
		
	    	setTimeout(function(){ location.reload(true); }, 500);
	    

	});

	$('#open-ticket-code').bind("enterKey",function(e){
	   //do stuff here
	});
	$('#open-ticket-code').keyup(function(e){

	    if(e.keyCode == 13)
	    {
	        $(".btn-apply-open-ticket").trigger('click');

	        $('#open-ticket-code').val('');
	    }

	    

	});	

	$(".btn-apply-open-ticket").click(function () {

		var gTotal = parseFloat($('input[name="gtotal"]').val());

		var newGtotal = 0;

			if($('#open-ticket-code').val() == ''){

				alert('Please enter a ticket code');

			}
			else{

				var ok = 1;

				var passenger_index = 0;

				var originID = $('input[name="origin"]').val();

				var destinationID = $('input[name="destination"]').val();			
			
		    	var open_ticket_code = $('#open-ticket-code').val();

			    $(".used-open-ticket-codes").each(function(){
			    	if($(this).val() == open_ticket_code){

			    		alert('This ticket code is already used.');

			    		ok = 0;


			    	}else{


			    	}
			        
			    });

		        var url = site_url+"/open-ticket/retrieve/"+open_ticket_code+'/'+originID+'/'+destinationID;
	 			if(ok == 1){
				    $.get(url, function(data){


					        $.each(data,function(index,passenger){

					            // console.log(passenger.ticket_price);



					            if(!$.isEmptyObject(passenger)){

					    			if(passenger.is_expired == true){

					    				alert('This open ticket code has expired.');

					    			}
					    			else if(passenger.open_ticket_code_used == 1){

					    				alert('This open ticket is already used.');

					    			}else{


									    $(".passenger_seat_category_id").each(function(){

									    	passenger_index = parseInt($(this).attr('rel'));

									    	if($(this).val() == passenger.SeatCategoryId && passenger.Pass_type == $('.passenger_type_'+passenger_index).val()){

									    		


									    		if( $('#open_ticket_passenger_'+passenger_index).is(':empty') ) {

										            $('#open_ticket_passenger_'+passenger_index).append('<div class="col-md-8"><div class="form-group"><span style="font-size:12px">P'+(passenger_index+1)+' Open Ticket ('+open_ticket_code+')</span></div></div><div class="col-md-4 text-right"><label>- '+passenger.ticket_price.toFixed(2)+'</label><div class="form-group"></div></div>');

										            $('#open_ticket_passenger_'+passenger_index).append('<input type="hidden" rel="'+passenger.id+'" class="used-open-ticket-codes" value="'+open_ticket_code+'"/>');

										            $('#open_ticket_passenger_'+passenger_index).append('<input type="hidden" name="waive_passenger_'+passenger_index+'" value="'+passenger.id+'"/>');

										            $('#open_ticket_passenger_'+passenger_index).append('<input type="hidden" class="passenger_citizen_prices" value="'+passenger.ticket_price+'"/>');

										            $('#open-ticket-code').val('');

										            newGtotal = gTotal - passenger.ticket_price;

										            $('.open_ticket_passenger_'+passenger_index).val(1);

										            $('input[name="subtotal"]').val(newGtotal.toFixed(2));

										            $('#total').html(newGtotal.toFixed(2));


										            $('input[name="gtotal"]').val(newGtotal.toFixed(2));

										            $('#gtotal').html(newGtotal.toFixed(2));

										            return false;

									    		}




									    	}

									    	passenger_index++;
									        
									    });

									}



					            }
					            else{
					            	alert('Open ticket is not valid');
					            }


					        });


				            // console.log(data.passenger_info);
				    });

				}

			}

	});	   

});



// $("input[name='discount']").keyup(function(){
$(".btn-apply-discount").click(function(){

	var password = encodeURIComponent($('.user_password').val());

    var url = site_url+"/validate-password/"+password;

    $.ajax({
        url: url,
        success: function (data) {
        	
			var subtotal = 0;

		    $(".passenger_current_price").each(function(){

		    	subtotal += parseFloat($(this).val());
		        
		    });

			var discount_amount = $("input[name='discount']").val() == '' ? 0 : parseFloat($("input[name='discount']").val());

		    $("#tds").html(discount_amount.toFixed(2));

		    var new_gtotal = subtotal - discount_amount;

		    $('input[name="gtotal"]').val(new_gtotal.toFixed(2));

		    $('#gtotal').html(new_gtotal.toFixed(2));
        },
        error: function (data) {
            console.log('Error:', data);
        }
    });	

	// var subtotal = parseFloat($('input[name="subtotal"]').val());
	$('#open-ticket-code').focus();

});

function registerDetails(){

	var posRegisterID = $('input[name="pos_register_id"]').val();

    var url = site_url+"/details/"+posRegisterID;

    $.ajax({
        url: url,
        success: function (data) {

            $('.cash_register_details').html(data);
        },
        error: function (data) {
            console.log('Error:', data);
        }
    }); 
    

}

function closeRegister(){

	var posRegisterID = $('input[name="pos_register_id"]').val();

        var url = site_url+"/close/"+posRegisterID;

        $.ajax({
            url: url,
            success: function (data) {

                $('.close_register_details').html(data);

                $('.total_cash_submitted').focus();
            },
            error: function (data) {
                console.log('Error:', data);
            }
        }); 
    

}

function availability(elem,type){

	$('.panel-onward').empty();

	$('#total-passengers').text('0');

	$('input[name="total_adults"]').val(0);

	$('input[name="total_children"]').val(0);

	$('#total').text('0.00');

	var originID = $('input[name="origin"]').val();

	var destinationID = $('input[name="destination"]').val();			

	if(type == 'return'){
		$('.journeyicon').html('<div class="fa fa-exchange"></div>');
	}
	else{
		$('.journeyicon').html('<div class="fa fa-long-arrow-right"></div>');
	}

	$('.'+type+'triptime').html(elem.attr('data-time-text'));

	$('.'+type+'-trip-button').removeClass('active');

	elem.addClass('active');

	var trip_id = elem.attr('rel');

	var url = site_url+"/availability/"+trip_id+'/'+type+'/'+originID+'/'+destinationID;

	$('input[name="'+type+'_trip"]').val(trip_id);

    $.ajax({
        url: url,
        success: function (data) {

            $('.availability').html(data);

		   // This button will increment the value	

        },
        error: function (data) {
            console.log('Error:', data);
        }
    }); 

    $('#open-ticket-code').focus();

}

function select_destination(elem){

	$('input[name=\'destination\']').val(elem.attr('rel'));

	$('.destinationtext').html(elem.attr('data-destination-code'));

	$('#open-ticket-code').focus();

	$('.journeyicon').show();

	$('.onwarddatetext').text('N/A');		

	$('.onwardtriptime').empty();	

	$('.onward-trips').empty();

	$('.panel-onward').empty();

}

function total_passenger(elem){

	if(elem.attr('data-trip-type') == 'onward'){

		var total_passengers = 0;

		var total_adults = 0;

		var total_children = 0;	

		// var total_adults = $('.total_adults').val() == '' ? 0 : $('.total_adults').val();

		// var total_children = $('.total_children').val() == '' ? 0 : $('.total_children').val();

		for(var a=0;a<$('.onward_adults').length;a++){

			total_adults += parseInt($('.onward_adults').eq(a).val());

		}

		$('input[name="total_adults"]').val(total_adults);

		for(var c=0;c<$('.onward_children').length;c++){

			total_children += parseInt($('.onward_children').eq(c).val());

		}

		$('input[name="total_children"]').val(total_children);	

		total_passengers = total_adults + total_children;

		$('#total-passengers').html(total_passengers);

	    // var url = site_url+"/passenger-details/"+total_adults+"/"+total_children;

	    // $.ajax({
	    //     url: url,
	    //     success: function (data) {
	    //         $('.passenger-details-body').html(data);
	    //     },
	    //     error: function (data) {
	    //         console.log('Error:', data);
	    //     }
	    // }); 

	}

}

function check_passenger_selection(elem){

	var triptype = elem.attr('data-trip-type');

	if(triptype == 'onward'){

		var total_passengers = 0;

		var tripid = elem.attr('data-trip-id');

		var vesselseatid = elem.attr('data-vessel-seat-id');

		var availability = $('.vessel_seat_'+tripid+'_'+vesselseatid).val();

		for(var i = 0;i < $('.passenger_'+tripid+'_'+vesselseatid).length;i++){

			total_passengers += parseFloat($('.passenger_'+tripid+'_'+vesselseatid).eq(i).val());

		}

		if(total_passengers > availability){

			alert('You have selected more passengers than the availability');

			return false;
		}

	}
	else if(triptype == 'return'){

		var total_adults = $('input[name="total_adults"]').val();

		var total_children = $('input[name="total_children"]').val();

		var return_total_adults = 0;

		var return_total_children = 0;

		for(var a=0;a<$('.return_adults').length;a++){

			return_total_adults += parseInt($('.return_adults').eq(a).val());

		}

		if(total_adults < return_total_adults){

			alert('You can only select '+total_adults+' adult'+(total_adults > 1 ? 's' : ''));

			return false;

		}

		for(var c=0;c<$('.return_children').length;c++){

			return_total_children += parseInt($('.return_children').eq(c).val());

		}

		if(total_children < return_total_children){

			alert('You can only select '+total_children+' '+(total_children > 1 ? 'children' : 'child'));

			return false;

		}			

	}

	

	update_ticket_details(triptype);

	$('#open-ticket-code').focus();

}

function update_ticket_details(triptype){

	var html = '';

	var passenger_type_text = '';

	var vessel_seat_name = '';

	var gtotal = 0;

	// var total_adults = 0;

	// var total_children = 0;

	var passenger_index = 0;

	var passenger_number = 1;

	for(var i=0;i<$('.onward-passengers').length;i++){

		if($('.onward-passengers').eq(i) != '0'){

			passenger_type_text = $('.onward-passengers').eq(i).attr('data-passenger-type-text');

			vessel_seat_category_id = $('.onward-passengers').eq(i).attr('data-seat-category-id');

			vessel_seat_name = $('.onward-passengers').eq(i).attr('data-vessel-seat-name');

			var total_passengers = parseInt($('.onward-passengers').eq(i).val());

			for(var j=0;j<total_passengers;j++){

				var price = parseFloat($('.onward-passengers').eq(i).attr('data-price'));



				html += '<div class="col-md-8"><div class="form-group"><span style="font-size:12px"><a href="#" data-toggle="modal" data-target="#onward_passenger_details_'+passenger_number+'modal">P'+passenger_number+' '+passenger_type_text+' ('+vessel_seat_name+')</a> <input type="checkbox" name="passenger_citizen_'+passenger_index+'" onchange="changePrice($(this),\''+passenger_type_text+'\','+vessel_seat_category_id+','+passenger_number+')"/> Citizen</span></div></div><div class="col-md-4 text-right"><div class="form-group"><div class="text-right"><label class="passenger-price-text-'+passenger_number+'">'+price.toFixed(2)+'</label></div></div></div>';

				html += '<div id="open_ticket_passenger_'+passenger_index+'"></div>';

				html += ' <div class="modal fade" id="onward_passenger_details_'+passenger_number+'modal" role="dialog"> <div class="modal-dialog"> <div class="modal-content"> <div class="modal-header"> <button type="button" class="close" data-dismiss="modal">&times;</button> <h4 class="modal-title">Passenger '+passenger_number+' Details</h4> </div><div class="modal-body"> <div class="row"> <div class="col-md-6"> <div class="form-group"> <label>Full Name</label> <input type="text" name="passenger_name[]" class="form-control"/> </div></div><div class="col-md-6"> <div class="form-group"> <label>Identity Type</label> <select class="form-control" name="id_type[]"> <option>Mykad</option> <option>Mykid</option> <option>Passport</option> </select> </div></div><div class="col-md-6"> <div class="form-group"> <label>Identity Number</label> <input type="text" name="identity_number[]" class="form-control"/> </div></div></div></div><div class="modal-footer"> <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> </div></div></div></div>';

				html += '<input type="hidden" rel="'+passenger_index+'" class="passenger_original_price_'+passenger_number+'" value="'+price.toFixed(2)+'"/>';

				html += '<input type="hidden" rel="'+passenger_index+'" class="passenger_current_price passenger_price_'+passenger_number+'" name="passenger_price[]" value="'+price.toFixed(2)+'"/>';

				html += '<input type="hidden" rel="'+passenger_index+'" class="passenger_seat_category_id" name="passenger_seat_category_id[]" value="'+vessel_seat_category_id+'"/>';

				html += '<input type="hidden" rel="'+passenger_index+'" class="passenger_type passenger_type_'+passenger_index+'" name="passenger_type[]" value="'+passenger_type_text+'"/>';

				html += '<input type="hidden" class="open_ticket_passenger_'+passenger_index+'" name="open_ticket_passenger[]" value="0"/>';

				gtotal+=price;

				passenger_number++;

				passenger_index++;

			}

		}


		
	}

	$('.panel-onward').html(html);

	// $('.panel-return').html(html);

	// if(triptype == 'return'){

	// 	html = '';

	// 	passenger_number = 1;

	// 	for(var i=0;i<$('.return-passengers').length;i++){

	// 		passenger_type_text = $('.onward-passengers').eq(i).attr('data-passenger-type-text');

	// 		vessel_seat_name = $('.onward-passengers').eq(i).attr('data-vessel-seat-name');

	// 		if($('.return-passengers').eq(i) != '0'){

	// 			var total_passengers = parseInt($('.return-passengers').eq(i).val());

	// 			for(var j=0;j<total_passengers;j++){

	// 				var price = parseFloat($('.return-passengers').eq(i).attr('data-price'));

	// 				html += '<div class="col-md-8"><div class="form-group"><span style="font-size:12px">P'+passenger_number+' '+passenger_type_text+' ('+vessel_seat_name+')</div></div><div class="col-md-4"><div class="form-group"><div class="text-right"><label class="">'+price.toFixed(2)+'</label></div></div></div>';

	// 				// html += ' <div class="modal fade" id="return_passenger_details_'+j+'_modal" role="dialog"> <div class="modal-dialog"> <div class="modal-content"> <div class="modal-header"> <button type="button" class="close" data-dismiss="modal">&times;</button> <h4 class="modal-title">Passenger '+passenger_number+' Details</h4> </div><div class="modal-body"> <div class="row"> <div class="col-md-6"> <div class="form-group"> <label>Full Name</label> <input type="text" class="form-control"/> </div></div><div class="col-md-6"> <div class="form-group"> <label>Identity Type</label> <select class="form-control"> <option>Mykad</option> <option>Mykid</option> <option>Passport</option> </select> </div></div><div class="col-md-6"> <div class="form-group"> <label>Identity Number</label> <input type="text" name="identity_number" class="form-control"/> </div></div></div></div><div class="modal-footer"> <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> </div></div></div></div>';

	// 				gtotal+=price;

	// 				passenger_number++;

	// 			}

	// 		}


			
	// 	}

	// 	$('.panel-return').html(html);

	// }	



	$('#total').html(gtotal.toFixed(2));

	$('input[name="subtotal"]').val(gtotal.toFixed(2));

	var discount_amount = parseFloat($('input[name="discount"]').val());

	var new_gtotal = gtotal - discount_amount;

	$('#gtotal').html(new_gtotal.toFixed(2));

	$('input[name="gtotal"]').val(new_gtotal.toFixed(2));

	disable_return_trips();

}

function changePrice(elem,passengerType,seatCategoryID,passengerNum){

	var price = 0;

	var subtotal = 0;

	if(elem.is(":checked")){

		if(passengerType == 'Adult'){

			price = parseFloat($('.adult_citizen_price_'+seatCategoryID).val());

			$('.passenger-price-text-'+passengerNum).html(price.toFixed(2));

			$('.passenger_price_'+passengerNum).val(price.toFixed(2));

		}
		else{

			price = parseFloat($('.child_citizen_price_'+seatCategoryID).val());

			$('.passenger-price-text-'+passengerNum).html(price.toFixed(2));

			$('.passenger_price_'+passengerNum).val(price.toFixed(2));
		}

	}else{

		price = parseFloat($('.passenger_original_price_'+passengerNum).val());

		$('.passenger-price-text-'+passengerNum).html(price.toFixed(2));

		$('.passenger_price_'+passengerNum).val(price.toFixed(2));		

	}

	calculate_grand_total();

}

function calculate_grand_total(){

	var subtotal = 0;

    $(".passenger_current_price").each(function(){

    	subtotal += parseFloat($(this).val());
        
    });

    $(".passenger_citizen_prices").each(function(){

    	subtotal -= parseFloat($(this).val());
        
    });    

    var discount_amount = $('input[name="discount"]').val() == '' ? 0 : parseFloat($('input[name="discount"]').val());

	$('#total').html(subtotal.toFixed(2));

	$('input[name="subtotal"]').val(subtotal.toFixed(2));

    var new_gtotal = subtotal - discount_amount;

    $('input[name="gtotal"]').val(new_gtotal.toFixed(2));

    $('#gtotal').html(new_gtotal.toFixed(2));    
}

function disable_return_trips(){

	var attempttotal = 0;

	for(var i=0;i<$('.onward-passengers').length;i++){

		if($('.onward-passengers').eq(i).val() != '0'){

			var vesselseatcategoryid = $('.onward-passengers').eq(i).attr('data-seat-category-id');

			var onwardtripid = $('.onward-passengers').eq(i).attr('data-trip-id');

			attempttotal += parseInt($('.onward-passengers').eq(i).val());


		}


	}

	var tripid = $('.return_capacity_'+vesselseatcategoryid).attr('data-trip-id');

	// alert(tripid);

	if(parseInt($('.return_capacity_'+vesselseatcategoryid).val()) < attempttotal){

		$('#return-trip-'+tripid).prop('disabled',true);

	}
	else{

		$('#return-trip-'+tripid).prop('disabled',false);

	}

	for(var j=0;j<$('.return_seat_'+vesselseatcategoryid).length;j++){



	}

}
// $('.qtyplus').click(function(e){

function incrementvalue(elem){


    // Stop acting like a button
    // e.preventDefault();
    // Get the field name
    fieldName = elem.attr('field');

    // alert(fieldName);
    // Get its current value
    var currentVal = parseInt($('input[name='+fieldName+']').val());


    // If is not undefined
    if (!isNaN(currentVal)) {
        // Increment
        $('input[name='+fieldName+']').val(currentVal + 1);
    } else {
        // Otherwise put a 0 there
        $('input[name='+fieldName+']').val(0);
    }

    total_passenger(elem);

    check_passenger_selection(elem);
}
// This button will decrement the value till 0


// $(".qtyminus").click(function(e) {

function decrementvalue(elem){	
    // Stop acting like a button
    // e.preventDefault();
    // Get the field name
    fieldName = elem.attr('field');
    // Get its current value
    var currentVal = parseInt($('input[name='+fieldName+']').val());
    // If it isn't undefined or its greater than 0
    if (!isNaN(currentVal) && currentVal > 0) {
        // Decrement one
        $('input[name='+fieldName+']').val(currentVal - 1);
    } else {
        // Otherwise put a 0 there
        $('input[name='+fieldName+']').val(0);
    }

    total_passenger(elem);

    check_passenger_selection(elem);
}
