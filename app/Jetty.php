<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Jetty extends Model
{
    public function origins(){

    	return $this->hasMany('App\Booking','origin_id');

    }
    public function destinations(){

    	return $this->hasMany('App\Booking','destination_id');

    }
    public function route_froms(){

    	return $this->hasMany('App\Route','Route_From');

    }
    public function route_tos(){

    	return $this->hasMany('App\Route','Route_To');

    }

    public function seat_category_origins(){

        return $this->hasMany('App\OpenTicketPrice','origin_id');

    }
    public function origin_citizens(){

        return $this->hasMany('App\Citizen','origin_id');

    }

    public function destination_citizens(){

        return $this->hasMany('App\Citizen','destination_id');

    }           
}
