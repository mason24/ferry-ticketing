<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;

use App\Jetty;

use App\Route;

use App\Trip;

use App\TripPrices;

use App\Vessel_Seat;

use App\Vessel;

use App\Booking;

use App\SeatCategory;

use App\Passenger;

use App\OpenTicket;

use App\FerryBookedSeat;

use App\Pos;

use App\Citizen;

use App\BoardingPass;

class PosController extends Controller
{
    public function __construct()
    {

        $this->middleware('auth');

    }     
    public function index(){

        $pos_register_info = $this->is_opened(\Auth::user()->id);

    	$origins = Jetty::orderBy('Jetty_Name')->get();

    	return view('pos.index',compact('origins','pos_register_info'));

    }

    public function redeem(){

        $origins = Jetty::orderBy('Jetty_Name')->get();

        return view('pos.open_ticket_redeem',compact('origins'));

    }

    public function destination($id){

    	$destination_ids = Route::select('Route_To')->where('Route_From','=',$id)->get();

    	foreach($destination_ids as $destination_id){

    		$destinations = Jetty::where('id','=',$destination_id->Route_To)->get();

    	}

    	return view('pos.destination',compact('destinations'));

    }

    public function trips($from,$to,$type,$date){

        $depart_date = date_format(date_create($date),'Y-m-d');

        $routes = Route::select('id')->where([['Route_From','=',$from],['Route_To','=',$to]])->get();

        foreach($routes as $route){

            $trips = Trip::orderBy('Depart_Time')->where([

                ['Route_Id', '=', $route->id],

                ['Depart_Date', '=', $depart_date]

            ])->get();

            return view('pos.trips',compact('trips','type'));   
        }
 
    }

/*    public function trips($from,$to,$type,$date){

        $trips_array = $seat_array = array();

        $depart_date = date_format(date_create($date),'Y-m-d');

        $route = Route::select('id')->where([

            ['Route_From','=',$from],

            ['Route_To','=',$to]


        ])->first();

        // foreach($routes as $route){

            $trips = Trip::orderBy('Depart_Time')->where([

                ['Route_Id', '=', $route->id],

                ['Depart_Date', '=', $depart_date]

            ])->get();

            foreach($trips as $trip){

                $vessel_seat_ids = TripPrices::select('vessel_seat_id')->where('trip_id','=',$trip->id)->get();

                foreach($vessel_seat_ids as $vessel_seat_id){

                    $capacity = Vessel_Seat::select('capacity')->where([['seat_category_id','=',$vessel_seat_id->vessel_seat_id],['vessel_id','=',$trip->Vessel_Id]])->first();

                    $vessel_capacity = $capacity->capacity;

                    array_push($seat_array,array('seat_category_id' => $vessel_seat_id->vessel_seat_id , 'capacity' => $vessel_capacity));

                }

                array_push($trips_array,array(

                    'id' => $trip->id , 

                    'Depart_Time' => $trip->Depart_Time,

                    'capacity' => $seat_array

                ));

            }



            return view('pos.trips',compact('trips_array','type'));   
        // }
 
    }*/    

    public function availability($tripid,$type,$origin,$destination){

        $citizen_id = $total_booked_seats = 0;

        $citizen = Citizen::select('id')->where([

            ['origin_id','=',$origin],

            ['destination_id','=',$destination]

        ])->first();

        if($citizen !== null){

            $citizen_id = $citizen->id;

        }

        

        $availability = $balance_seats = array();

        $vessel = Trip::select('Vessel_Id')->where('id','=',$tripid)->first();

        $vessel_seats = Vessel_Seat::select('id','seat_category_id','capacity')->where('vessel_id','=',$vessel->Vessel_Id)->get();


        

        // $capacity = $vessel_seat->Capacity;

        // $seat_balance = $capacity - $bookings;

        $adult_prices = $child_prices = $adult_citizen_prices = $child_citizen_prices = array();

        foreach($vessel_seats as $vessel_seat){

            $adult_citizen_price = $child_citizen_price = 0.00;

            $vessel_id = $vessel->Vessel_Id;

            $total_booked_seats = FerryBookedSeat::where([

                ['seat_category_id','=',$vessel_seat->seat_category_id],

                ['trip_id','=',$tripid],

                ['status','=',1],

            ])->count();

            // echo $vessel->Vessel_Id;

            $citizen_price_info = DB::table('citizen_categories')->select('adult_price','child_price')->where([['citizen_id','=',$citizen_id],['vessel_id','=',$vessel_id],['vessel_seat_id','=',$vessel_seat->seat_category_id]])->first();

            // dd($citizen_price_info);

            $seat_category = SeatCategory::select('Seat_Category_Name')->where('id','=',$vessel_seat->seat_category_id)->first();

            array_push($availability,array('id'=>$vessel_seat->id,'seat_category_id'=>$vessel_seat->seat_category_id,'seat_category_name'=>$seat_category->Seat_Category_Name,'capacity'=>$vessel_seat->capacity));

            $price_info = TripPrices::select('adult_price','child_price')->where([['vessel_seat_id','=',$vessel_seat->seat_category_id],['trip_id','=',$tripid]])->first();

            $adult_prices[$tripid][$vessel_seat->id] = $price_info->adult_price;

            $child_prices[$tripid][$vessel_seat->id] = $price_info->child_price;

            if($citizen_price_info !== null){

                $adult_citizen_price = $citizen_price_info->adult_price;

                $child_citizen_price = $citizen_price_info->child_price;
            }

            $adult_citizen_prices[$tripid][$vessel_seat->id] = $adult_citizen_price;

            $child_citizen_prices[$tripid][$vessel_seat->id] = $child_citizen_price;   

            $balance_seats[$tripid][$vessel_seat->seat_category_id] = $vessel_seat->capacity - $total_booked_seats;         

        }

        return view('pos.availability',compact('availability','vessel_seats','tripid','adult_prices','child_prices','adult_citizen_prices','child_citizen_prices','booked_seats','balance_seats','type')); 

    }

    public function passenger_details($total_adults,$total_children){

        $passenger_number = 1;

        return view('pos.passenger_details',compact('total_adults','total_children','passenger_number'));

    }

    public function store(){

        $this->validate(request() , [

         'origin' => 'required',

         'destination' => 'required',
         
         'onward_date' => 'required|not_in:0',

         'onward_trip' => 'required'

        ]);  

        $trip_info = Trip::where('id','=',request('onward_trip'))->first();

        $booking_id = date('Ymd') . time() . rand(10*45, 100*98);

        $booking = new Booking;

        $booking->booking_id = $booking_id;

        $booking->journey = 'one-way';

        $booking->origin_id = request('origin');

        $booking->destination_id = request('destination');

        $booking->selected_onward_trip_id = request('onward_trip');

        // $booking->selected_return_trip_id = 0;

        $booking->total_adults = request('total_adults');

        $booking->total_children = request('total_children');

        $booking->total_tax = 0;

        $booking->total_discount = (empty(request('discount')) ? 0 : request('discount'));

        $booking->grand_total_amount = request('gtotal');

        $booking->created_by = \Auth::user()->id;

        $booking->source = 'counter';

        $booking->pos_register_id = request('pos_register_id');

        $booking->payment_mode = 'cash';

        $booking->booking_status = 1;

        $booking->save();

        $passenger = new Passenger;

        $open_ticket_passengers = request('open_ticket_passenger');

        $passenger_names = request('passenger_name');

        $passenger_seat_categories = request('passenger_seat_category_id');

        $passenger_types = request('passenger_type');

        $passenger_prices = request('passenger_price');

        // $passenger_citizens = request('passenger_citizen');

        $id_types = request('id_type');

        $id_numbers = request('identity_number');  



        for($i=0;$i<count($passenger_types);$i++){

            $seat_num = FerryBookedSeat::where([

                ['trip_id','=',request('onward_trip')],

                ['date','=',$trip_info->Depart_Date],

                ['seat_category_id','=',$passenger_seat_categories[$i]]

                ])->max('seat_num');            

            $seat_number = $seat_num;

            $new_seat_number = $seat_number + 1;

            FerryBookedSeat::insert([
                [
                    'booking_id' => $booking_id,

                    'trip_id' => request('onward_trip'),

                    'vessel_id' => 1,

                    'date' => $trip_info->Depart_Date,

                    'seat_num' => $new_seat_number,

                    'seat_category_id' => $passenger_seat_categories[$i],

                    'status' => 1
                ]
            ]);  

            $origin = Jetty::select('Jetty_Code')->where('id','=',request('origin'))->first(); 

            $destination = Jetty::select('Jetty_Code')->where('id','=',request('destination'))->first();     

            if($open_ticket_passengers[$i] == '1'){

                $open_ticket_passenger_id = request('waive_passenger_'.$i);

                Passenger::where('id', $open_ticket_passenger_id)->update([

                    'open_ticket_code_used' => 1,

                    'SeatNo' => $new_seat_number

                ]);

                $passenger_id = $open_ticket_passenger_id;

            }
            else{
                $passenger_id = $passenger->insertGetId(array(
                    
                        'Pass_name' => (empty($passenger_names[$i]) ? 'N/A' : $passenger_names[$i]),

                        'Pass_type' => $passenger_types[$i],

                        'pass_identity_type' => $id_types[$i],

                        'pass_identity_id' => (empty($id_numbers[$i]) ? 'N/A' : $id_numbers[$i]),

                        'pass_langkawicitizen' => (request('passenger_citizen_'.$i) !== null ? 1 : 0),

                        'SeatNo' => $new_seat_number,

                        'SeatCategoryId' => $passenger_seat_categories[$i],

                        'booking_id' => $booking_id,

                        'ticket_price' => $passenger_prices[$i],

                        'status' => 1
                ));

                



            }

            $boarding_pass_id = $this->boarding_pass_id($origin->Jetty_Code,$destination->Jetty_Code);

            DB::table('boarding_passes')->insert([

                'way' => 'depart',

                'boarding_pass_id' => $boarding_pass_id,

                'booking_id' => $booking_id,

                'selected_trip_id' => request('onward_trip'),

                'seat_no' => $new_seat_number,

                'seat_category_id' => $passenger_seat_categories[$i],

                'passenger_id' => $passenger_id,

                'status' => 'not_on_board',

            ]);                      

        }

        return redirect('pos/print/'.$booking_id);


    }
    public function print_ticket($booking_id){

        $booking = Booking::where('booking_id','=',$booking_id)->first();

        // $passengers = Passenger::where('booking_id','=',$booking_id)->get();

        $boarding_passes = BoardingPass::where('booking_id','=',$booking_id)->get();

        $depart_time = $booking->onward_trip->Depart_Time;      

        $time = strtotime($depart_time);

        $time = $time - (30 * 60);

        $boarding_time = date("H:i", $time);

        $booking->boarding_time = $boarding_time;

        // return view('pos.print',compact('passengers','booking'));

        return view('pos.print',compact('passengers','booking','boarding_passes'));

    }

    public function is_opened($user_id){

        $pos_info = DB::table('pos_registers')->where([

            ['user_id','=',$user_id],

            ['status','=','open']

        ])->first();

        if($pos_info === null){

            return false;

        }
        return $pos_info;

    }

    public function open(){

        $pos_register_id = DB::table('pos_registers')->insertGetId(array('user_id' => 1,

            'cash_in_hand' => request('cash_in_hand'),

            'status' => 'open'

        ));

        return redirect('pos')->with('pos_register_id');

    }

    public function details($id){

        $info = DB::table('pos_registers')->where('id','=',$id)->first();

        $total_sales = Booking::where('pos_register_id','=',$id)->sum('grand_total_amount');

        $total_cash  = $total_sales + $info->cash_in_hand;

        return view('pos.details',compact('info','total_sales','total_cash'));

    }

    public function close_details($id){

        $info = DB::table('pos_registers')->where('id','=',$id)->first();

        $total_sales = Booking::where([

            ['pos_register_id','=',$id],

            ['booking_status','=',1]

            ])->sum('grand_total_amount');

        $total_cash  = $total_sales + $info->cash_in_hand;

        return view('pos.close',compact('info','total_sales','total_cash'));

    }

    public function close(){

        // DB::enableQueryLog();

        DB::table('pos_registers')->where('id', request('pos_register_id'))->update([

            'status' => 'close',

            'total_cash' => request('total_cash'),

            'total_cc_slips' => request('total_cc_slips_submitted'),

            'total_cash_submitted' => request('total_cash_submitted'),

            'total_cc_slips_submitted' => request('total_cc_slips_submitted'),

            'closed_at' => date('Y-m-d H:i:s'),

            'closed_by' => \Auth::user()->id

            ]);

        $pos = DB::table('pos_registers')->where('id','=',request('pos_register_id'))->first();

        $sales = Passenger::leftJoin('bookings', 'passengers.booking_id', '=', 'bookings.booking_id')->leftJoin('users', 'bookings.created_by', '=', 'users.id')->where('bookings.pos_register_id','=',request('pos_register_id'))->get();



        $total_sales = Booking::where('pos_register_id','=',request('pos_register_id'))->sum('grand_total_amount');

        $total_amount = Passenger::leftJoin('bookings', 'passengers.booking_id', '=', 'bookings.booking_id')->where('bookings.pos_register_id','=',request('pos_register_id'))->sum('ticket_price');

        // dd(DB::getQueryLog());

        $total_transactions = $total_seats = $total_adults = $total_children = $total_citizen = $total_cash_payments = $total_open_tickets = 0;

        $number = 1;

        return view('pos.report',compact('pos','total_transactions','number','sales','total_sales','total_amount','total_seats','total_adults','total_children','total_citizen','total_cash_payments','total_open_tickets')); 

    }  

    public function boarding_pass_id($origin_code,$destination_code){

        $chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";

        $code = "";

        for ($i = 0; $i <= 6; $i++) {

            $code .= $chars[mt_rand(0, strlen($chars)-1)];

        } 

        $code = $origin_code.$destination_code.date('Y').$code;

        $code = $this->check_existing_boarding_pass_id($code);  

        return $code;   
    }
    public function check_existing_boarding_pass_id($code){     

        if (DB::table('boarding_passes')->where([['boarding_pass_id','=',$code]])->exists()) {

            $this->boarding_pass_id();

        }
        else{

            return $code;

        }
    }              

}
