<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePosRegistersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pos_registers', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamp('opened_at')->useCurrent();
            $table->integer('user_id');
            $table->double('cash_in_hand',25,2);
            $table->string('status',10)->nullable();
            $table->double('total_cash',25,2)->nullable();
            $table->integer('total_cc_slips')->default(0)->nullable();
            $table->double('total_cash_submitted',25,2)->nullable();
            $table->integer('total_cc_slips_submitted')->default(0)->nullable();
            $table->text('note')->nullable();
            $table->dateTime('closed_at')->nullable();
            $table->string('transfer_opened_bills',50)->nullable();
            $table->integer('closed_by')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pos_registers');
    }
}
