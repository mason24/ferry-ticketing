<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Ferry Ticketing System</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <meta name="layout" content="main"/>
    
    <script type="text/javascript" src="http://www.google.com/jsapi"></script>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js" type="text/javascript" ></script>
    <link href="{{ asset('css/customize-template.css') }}" type="text/css" media="screen, projection" rel="stylesheet" />

    <style>
    </style>
</head>
    <body>
        <div class="navbar navbar-fixed-top">
            <div class="navbar-inner">
                <div class="container">
                    <!-- <button class="btn btn-navbar" data-toggle="collapse" data-target="#app-nav-top-bar">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button> -->
                    <a href="{{ URL::to('/') }}" class="brand"><i class="icon-cog"> Ferry Ticketing</i></a>
                    <div id="app-nav-top-bar" class="nav-collapse">
                        
                        <ul class="nav pull-right">
                            
                             <li class="dropdown">
                                <a href="#" class="dropdown-toggle" onclick="GoOutFullscreen()">Exit System
                                </a>
                               
                            </li> 
                        </ul>
                       
                    </div>
                </div>
            </div>
        </div>
        <div id="body-container">
                    <div id="body-content">
                        
                        
            <div class='container'>
                
                <div class="signin-row row">
                    <div class="span4"></div>
                    <div class="span8">
                        <div class="container-signin">
                            @include ('layout.errors')
                            <legend>Please Login</legend>
                            <form action="{{ URL::to('/login') }}" method='post' id='loginForm' class='form-signin' autocomplete='off'>
                                {{ csrf_field() }}
                                <div class="form-inner">
                                    <div class="input-prepend">
                                        
                                        <span class="add-on" rel="tooltip" title="Username" data-placement="top"><i class="icon-user"></i></span>
                                        <input type='text' class='span4' id='username' name='username' placeholder='Username' autofocus/>
                                    </div>

                                    <div class="input-prepend">
                                        
                                        <span class="add-on"><i class="icon-lock"></i></span>
                                        <input type='password' class='span4' id='password' name='password' placeholder='Password'/>
                                    </div>
                                    <label>Select System</label>
                                    <select class='span4' name="system">
                                        <option value="admin">Administration</option>
                                        <option value="pos">POS</option>
                                    </select>
                                </div>
                                <footer class="signin-actions">
                                    <input class="btn btn-primary" type='submit' id="submit" value='Login'/>
                                </footer>
                            </form>
                        </div>
                    </div>
                    <div class="span3"></div>
                </div>
            <!--<div class="span4">

                </div>-->
            </div>
    

            </div>
        </div>

        <div id="spinner" class="spinner" >
            Loading&hellip;
        </div>

        <footer class="application-footer">
            <div class="container">
                <p>Ferry Ticketing System</p>
                <div class="disclaimer">
                    <p>All rights reserved.</p>
                    <p>Copyright © Netsoft Media {{ date('Y') }} </p>
                </div>
            </div>
        </footer>
            <div id="element">
        <span>Full Screen Mode Disabled</span>
        <button id="go-button">Enable Full Screen</button>
    </div>
 <Script>

/* Get into full screen */
function GoInFullscreen(element) {
    if(element.requestFullscreen)
        element.requestFullscreen();
    else if(element.mozRequestFullScreen)
        element.mozRequestFullScreen();
    else if(element.webkitRequestFullscreen)
        element.webkitRequestFullscreen();
    else if(element.msRequestFullscreen)
        element.msRequestFullscreen();
}

/* Get out of full screen */
function GoOutFullscreen() {

    if(document.exitFullscreen)
        document.exitFullscreen();
    else if(document.mozCancelFullScreen)
        document.mozCancelFullScreen();
    else if(document.webkitExitFullscreen)
        document.webkitExitFullscreen();
    else if(document.msExitFullscreen)
        document.msExitFullscreen();
}

/* Is currently in full screen or not */
function IsFullScreenCurrently() {
    return (document.fullscreen || document.webkitIsFullScreen || document.mozFullScreen);
}

$("#go-button").on('click', function() {
    if(IsFullScreenCurrently())
        GoOutFullscreen();
    else
        GoInFullscreen($("#element").get(0));
});

$(document).on('fullscreenchange webkitfullscreenchange mozfullscreenchange msfullscreenchange', function() {
    if(IsFullScreenCurrently()) {
        $("#element span").text('Full Screen Mode Enabled');
        $("#go-button").text('Disable Full Screen');
    }
    else {
        $("#element span").text('Full Screen Mode Disabled');
        $("#go-button").text('Enable Full Screen');
    }
});
 </Script>       
        
<style type="text/css">

body {
    margin: 0;
}

body {
    /*margin: 40px auto 20px auto;
    height: 200px;
    width: 400px;*/
    /*background-color: #e9e9e9;*/
    /*font-size: 20px;*/
    /*padding: 40px 0 0 0;*/
    /*text-align: center;*/
    /*box-sizing: border-box;*/
}

#go-button {
    width: 200px;
    display: block;
    margin: 50px auto 0 auto;
}

/* webkit requires explicit width, height = 100% of sceeen */
/* webkit also takes margin into account in full screen also - so margin should be removed (otherwise black areas will be seen) */
body:-webkit-full-screen {
    width: 100%;
    height: 100%;
    /*background-color: pink;*/
    margin: 0;
}

body:-moz-full-screen {
    /*background-color: pink;*/
    margin: 0;
}

body:-ms-fullscreen {
    /*background-color: pink;*/
    margin: 0;
}

/* W3C proposal that will eventually come in all browsers */
body:fullscreen { 
    /*background-color: pink;*/
    margin: 0;
}

</style>
    </body>
</html>