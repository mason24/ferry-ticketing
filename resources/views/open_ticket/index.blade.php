@extends('layout.master')
@section('content')
                        <div class="box pattern pattern-sandstone">
                            <div class="box-header">
                                <i class="icon-table"></i>
                                <h5>
                                    Open Tickets
                                </h5>
                            </div>
                            <div class="box-content box-table">
                            <div><a href="open-ticket/new">New Open Ticket</a></div>
                            Sort: <a href="open-ticket?sort=asc">Ascending</a> | <a href="trips?sort=desc">Descending</a>
                                <table id="sample-table" class="table table-hover table-bordered tablesorter">
                                    <thead>
                                        <tr>
                                            <th>Trip Code</th>
                                            <th>Trip Name</th>
                                            <th>Operating Vessel</th>
                                            <th>Departure Datetime</th>
                                            <th>Arrival Datetime</th>
                                            <th>Availability</th>
                                            <th class="td-actions"></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    	@foreach ($open_tickets as $open_ticket)
	                                        <tr>
	                                            <td>{{ $open_ticket->open_ticket_code }}</td>
                                                <td>{{ $open_ticket->open_ticket_code }}</td>
                                                <td>{{ $open_ticket->open_ticket_code }}</td>
                                                <td>{{ $open_ticket->open_ticket_code }}</td>
                                                <td>{{ $open_ticket->open_ticket_code }}</td>
	                                            <td>99/100</td>
	                                            <td class="td-actions">
	                                                <a href="open-ticket/edit/{{ $open_ticket->id }}" class="btn btn-small btn-info">
	                                                    <i class="btn-icon-only icon-edit"></i>
	                                                </a>

	                                                <a href="open-ticket/delete/{{ $open_ticket->id }}" class="btn btn-small btn-danger">
	                                                    <i class="btn-icon-only icon-remove"></i>
	                                                </a>
	                                            </td>
	                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>

                                {{ $open_tickets->links() }}
                            </div>
                        </div>
@endsection