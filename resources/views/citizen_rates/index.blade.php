@extends('layout.master')
@section('content')
                        <div class="box pattern pattern-sandstone">
                            <div class="box-header">
                                <i class="icon-table"></i>
                                <h5>
                                    Citizen Rates
                                </h5>

                            </div>
                            <div class="form-group">
                                <a href="citizen-rates/new">New Citizen Rate</a> 
                            </div>                            
                            <div class="form-group">
                                Sort: <a href="citizen-rates?sort=asc">Ascending</a> | <a href="citizen-rates?sort=desc">Descending</a> 
                            </div>
                            <div class="box-content box-table">
                                <table id="sample-table" class="table table-hover table-bordered tablesorter">
                                    <thead>
                                        <tr>
                                            <th>Origin</th>
                                            <th>Destination</th>
                                            <th>Status</th>
                                            <th class="td-actions"></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    	@foreach ($citizens as $citizen)
	                                        <tr>
	                                            <td>{{ $citizen->origin->Jetty_Name }}</td>
	                                            <td>{{ $citizen->destination->Jetty_Name }}</td>
	                                            <td>{{ $citizen->status == 1 ? 'Active' :'Inactive' }}</td>
	                                            <td class="td-actions">
	                                                <a href="user_groups/edit/{{ $citizen->id }}" class="btn btn-small btn-info">
	                                                    <i class="btn-icon-only icon-edit"></i>
	                                                </a>

	                                                <a href="user_groups/delete/{{ $citizen->id }}" class="btn btn-small btn-danger">
	                                                    <i class="btn-icon-only icon-remove"></i>
	                                                </a>
	                                            </td>
	                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>

                                {{ $citizens->links() }}
                            </div>
                        </div>
@endsection