<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SessionController extends Controller
{

	
    public function __construct()
    {
        // $this->middleware('auth');
    }
    public function index(){

        $branch = new Branch;

        $branches = $branch->orderBy('branch_name')->get();

        $application = new Application;

        $applications = $application->orderBy('application_id')->get();

        $product = new Product;

        $products = $product->orderBy('product_name')->get();         

    	return view('auth.dashboard', [

            'branches'=> $branches,

            'applications'=> $applications,

            'products'=> $products

        ]);

    }

    public function create(){

    	return view('sessions.login');

    }

    public function login(){

    	if(! auth()->attempt(request(['username' , 'password']))){

    		return back()->withErrors([

    			'message' => 'Please check your credentials and login again.'

    		]);

    	}	


    	return redirect('dashboard');

    }  

    public function destroy(){

    	auth()->logout();
        
        return redirect('signin');
    }

    public function temp(){

        if(request('system') == 'pos'){

            return redirect('pos'); 

        }
        else{
            return redirect('dashboard'); 
        }

        

    }
}
