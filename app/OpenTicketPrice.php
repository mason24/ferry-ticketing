<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OpenTicketPrice extends Model
{
    public function origin(){

    	return $this->belongsTo('App\Jetty','origin_id');

    }

    public function destination(){

    	return $this->belongsTo('App\Jetty','destination_id');

    }
    public function seat_category(){

    	return $this->belongsTo('App\SeatCategory','seat_category_id');

    }    
}
