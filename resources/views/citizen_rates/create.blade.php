@extends('layout.master')
@section('content')
    @include('layout.errors')
    <form method="post" action="{{ URL::to('/settings/citizen-rates/new') }}">
        {{ csrf_field() }}
        <div class="row">
            <div id="acct-password-row" class="span7">
                <fieldset>
                    <legend>New Citizen Rate</legend><br>
                    <div class="control-group ">
                        <label for="title" class="control-label">Origin <span class="required">*</span></label>
                        <div class="controls">
                            <select class="chosen span4" name="origin">
                                <option>--Choose--</option>
                                @foreach($jetties as $jetty)
                                    <option value="{{ $jetty->id }}">{{ $jetty->Jetty_Name }}</option>
                                @endforeach
                            </select>

                        </div>
                    </div>
                    <div class="control-group ">
                        <label for="title" class="control-label">Destination <span class="required">*</span></label>
                        <div class="controls">
                            <select class="chosen span4" name="destination">
                                <option>--Choose--</option>
                                @foreach($jetties as $jetty)
                                    <option value="{{ $jetty->id }}">{{ $jetty->Jetty_Name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="control-group ">
                        <label for="title" class="control-label">Vessel <span class="required">*</span></label>
                        <div class="controls">
                            <select class="chosen span4" name="vessel" class="vessel" onchange="seats($(this))">
                                <option>--Choose--</option>
                                @foreach($vessels as $vessel)
                                    <option value="{{ $vessel->id }}">{{ $vessel->Vessel_Name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="vessel-seats">
                    </div> 
                    
                    <div class="control-group ">
                        <label for="title" class="control-label">Status <span class="required">*</span></label>
                        <div class="controls">
                            <select class="chosen span4" name="status">
                                
                                <option value="1">Active</option>
                                <option value="0">Inactive</option>
                                
                            </select>
                        </div>
                    </div>                                                                                                                     
                </fieldset>
            </div>
        </div>
        <footer id="submit-actions" class="form-actions">
            <button id="submit-button" type="submit" class="btn btn-primary" value="">Create</button>
            <a href="{{ URL::to('/settings/citizen-rates') }}" class="btn btn-default">Back</a>
        </footer>

<script>

function seats(elem){

    var vessel_id = elem.val();  

    $('.vessel-seats').empty();

    if(vessel_id != 0){
        var url = "{{ URL::to('/settings/citizen-rates/vessel-seats') }}"+"/"+vessel_id;

        $.ajax({
            url: url,
            success: function (data) {

                $('.vessel-seats').html(data);
            },
            error: function (data) {
                console.log('Error:', data);
            }
        }); 
    }

        

}

</script>        
    </form>
@endsection