                <div class="body-nav body-nav-horizontal body-nav-fixed">
                    <div class="container">
                        <ul>
                            <li>
                                <a href="{{ URL::to('/dashboard') }}">
                                    <i class="icon-dashboard icon-large"></i> Dashboard
                                </a>
                            </li>
                            <li>
                                <a href="{{ URL::to('/bookings') }}">
                                    <i class="icon-calendar icon-large"></i> Bookings
                                </a>
                            </li>
                            <li>
                                <a href="{{ URL::to('/open-ticket') }}">
                                    <div class="icon-large" style="
                                        width: 35px;
                                        height: 35px;
                                        background-image:url(http://localhost/ferry/public/images/ticket.png);
                                        background-size: 35px 35px;
                                        background-repeat: no-repeat;
                                        line-height: 32px;
                                        text-align: center;
                                        margin: -5px auto 0px auto;
                                        display:block;
                                        "></div> <span style="font-size:10px">Open Ticket</span>
                                </a>
                            </li>                            

                            
                            <li>
                                <a href="{{ URL::to('/customer') }}">
                                    <div class="icon-large" style="
                                        width: 34px;
                                        height: 34px;
                                        background-image:url(http://localhost/ferry/public/images/passenger.png);
                                        background-size: 34px 34px;
                                        background-repeat: no-repeat;
                                        line-height: 32px;
                                        margin-top: -10px;
                                        text-align: center;
                                        "></div> Customers
                                </a>
                            </li> 

                            <li>
                                <a href="{{ URL::to('/group') }}">
                                    <i class="icon-group icon-large"></i> Groups
                                </a>
                            </li> 
                            <li>
                                <a href="{{ URL::to('/trips') }}">
                                    <i class="icon-time icon-large"></i> Trips
                                </a>
                            </li>                            
                            <li>
                                <a href="{{ URL::to('/routes') }}">
                                    <div class="icon-large" style="
                                        width: 35px;
                                        height: 35px;
                                        background-image:url(http://localhost/ferry/public/images/route-white.png);
                                        background-size: 35px 35px;
                                        background-repeat: no-repeat;
                                        line-height: 32px;
                                        margin: -5px auto 0px auto;
                                        text-align: center;
                                        display: block;
                                        "></div> Routes
                                </a>
                            </li>
                            
                            <li>
                                <a href="{{ URL::to('/jetties') }}">
                                    <i class="icon-map-marker icon-large"></i> Jetties
                                </a>
                            </li> 
                            <li>
                                <a href="{{ URL::to('/vessels') }}">
                                    <div class="icon-large" style="
                                        width: 40px;
                                        height: 29px;
                                        background-image:url(http://localhost/ferry/public/images/vessels.png);
                                        background-size: 40px 29px;
                                        background-repeat: no-repeat;
                                        line-height: 32px;
                                        margin-top: -5px;
                                        text-align: center;
                                        "></div> Vessels
                                </a>
                            </li>                            
                            <li>
                                <a href="{{ URL::to('/users') }}">
                                    <i class="icon-user icon-large"></i> Users
                                </a>
                            </li> 
                            <li>
                                <a href="{{ URL::to('/user-groups') }}">
                                    <i class="icon-link icon-large"></i> <span style="font-size:10px">User Groups</span>
                                </a>
                            </li>                                                                                  
                            <li>
                                <a href="{{ URL::to('/settings/system') }}">
                                    <i class="icon-cogs icon-large"></i> <span style="font-size:10px">System Settings</span>
                                </a>
                            </li>
                            <li>
                                <a href="{{ URL::to('settings/company') }}">
                                    <div class="icon-large" style="
                                        width: 35px;
                                        height: 35px;
                                        background-image:url(http://localhost/ferry/public/images/company.png);
                                        background-size: 35px 35px;
                                        background-repeat: no-repeat;
                                        line-height: 32px;
                                        margin-top: -10px;
                                        text-align: center;
                                        "></div> Company
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>