@extends('layout.master')
@section('content')
    @include('layout.errors')
    <form method="post" action="{{ URL::to('/jetties/edit/'.$task->id) }}">
        {{ csrf_field() }}
        <div class="row">
            <div id="acct-password-row" class="span7">
                <fieldset>
                    <legend>Editing Jetty {{ $task->Jetty_Name }}</legend><br>
                    <div class="control-group ">
                        <label for="Jetty_Name" class="control-label">Jetty Name <span class="required">*</span></label>
                        <div class="controls">
                            <input id="current-pass-control" name="Jetty_Name" class="span4" type="text" value="{{ $task->Jetty_Name }}"/>

                        </div>
                    </div>
                    <div class="control-group ">
                        <label class="control-label">Jetty Code <span class="required">*</span></label>
                        <div class="controls">
                            <input id="current-pass-control" name="Jetty_Code" class="span4" type="text" value="{{ $task->Jetty_Code }}"/>

                        </div>
                    </div>
                    <div class="control-group ">
                        <label class="control-label">Button Background Color in POS<span class="required">*</span></label>
                        <div class="controls">
                            <!-- <input id="current-pass-control" name="button_background_color" class="span4 html_color_hex colorpicker-component" type="text" value="#ffffff" readonly="readonly" style="cursor:pointer;"/> -->
                            <div class="html_color_hex input-prepend colorpicker-component">
                                <input class="span4" type="text" value="{{ $task->pos_button_background_color }}" readonly="readonly" name="pos_button_background_color" />
                                <span class="add-on" style="cursor:pointer;"></span>
                            </div>
                        </div>
                    </div>                                                             
                </fieldset>
            </div>
        </div>
        <footer id="submit-actions" class="form-actions">
            <button id="submit-button" type="submit" class="btn btn-primary" value="">Save Changes</button>
            <a href="{{ URL::to('/jetties') }}" class="btn btn-default">Back</a>
        </footer>
    </form>
@endsection