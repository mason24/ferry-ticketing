<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRoutesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('routes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('Message_ID',30);
            $table->string('Company_Code',15);
            $table->string('Route_Code',10);
            $table->string('Route_Name',100);
            $table->integer('Route_From')->unsigned();
            $table->foreign('Route_From')->references('id')->on('jetties')->onDelete('cascade');
            $table->integer('Route_To')->unsigned();
            $table->foreign('Route_To')->references('id')->on('jetties')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('routes');
    }
}
