<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SeatCategory extends Model
{
    public function trip_price(){

    	return $this->hasMany('App\TripPrices','vessel_seat_id');

    }
    public function passengers(){

    	return $this->hasMany('App\Passenger','SeatCategoryId');

    }
	public function vessel_seats(){

		return $this->hasMany('App\Vessel_Seat');
		
	}
	public function open_ticket_seat_categories(){

		return $this->hasMany('App\OpenTicketPrice','seat_category_id');

	}


}
