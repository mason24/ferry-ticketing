@extends('layout.master')
@section('content')
    @include('layout.errors')
    <form method="post" action="{{ URL::to('/trips/create') }}">
        {{ csrf_field() }}

        <div class="row">
            <div id="acct-password-row" class="span7">
                <fieldset>
                    <legend>Editing "{{ $trip_info->Trip_Name }}"</legend><br>
                    <div class="control-group ">
                        <label class="control-label">Select Route <span class="required">*</span></label>
                        <div class="controls">
                            <select name="route" class="chosen span4" data-placeholder="Select">
                                <option value="0">--Choose--</option>
                            @foreach($routes as $route)
                                <option {{ $trip_info->Route_Id == $route->id ? 'selected="selected"' : '' }} value="{{ $route->id }}">{{ $route->Route_Name }}</option>
                            @endforeach
                            </select>

                        </div>
                    </div>                    
                    <div class="control-group ">
                        <label class="control-label">Operating Vessel <span class="required">*</span></label>
                        <div class="controls">
                            <select name="vessel" class="chosen span4" data-placeholder="Select" onchange="vesselSeats($(this).val())">
                                <option value="0">--Choose--</option>
                            @foreach($vessels as $vessel)
                                <option {{ $trip_info->Vessel_Id == $vessel->id ? 'selected="selected"' : '' }} value="{{ $vessel->id }}">{{ $vessel->Vessel_Name }}</option>
                            @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="control-group trip-seats">
                        <label class="control-label">Seat Prices</label>
                        <div class="controls">
                            <table class="table-bordered" cellpadding="5">
                                <thead>
                                    <tr>
                                        <th>Seat Category</th>
                                        <th>Adult Price</th>
                                        <th>Child Price</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($seats_array as $seat)
                                    <tr>
                                        <td>Normal</td>
                                        <td>                        
                                            <div class="input-prepend">
                                                <span class="add-on">RM</span>
                                                <input class="span2" type="text" name="adult-price[{{ $seat['seat_category_id'] }}]">
                                            </div>   
                                        </td>
                                        <td>                                        
                                            <div class="input-prepend">
                                                <span class="add-on">RM</span>
                                                <input class="span2" type="text" name="child-price[{{ $seat['seat_category_id'] }}]">
                                            </div> 
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>                                                                             
                    <div class="control-group ">
                        <label class="control-label">Depart Date <span class="required">*</span></label>
                  
                        <div class="input-prepend date datepicker">
                            <span class="add-on"><i class="icon-th"></i></span>
                            <input name="Depart_Date" class="span4" type="text" value="{{ $trip_info->Depart_Date }}" readonly="readonly" style="cursor:pointer"/>
                        </div>                          
                    </div>                                                          
                    <div class="control-group ">
                        <label class="control-label">Depart Time <span class="required">*</span></label>

<!--                         <div class="input-prepend date" style="padding: 4px;">
                            <span class="add-on"><i class="icon-time"></i></span>
                            <input name="Depart_Time" class="span4" type="text" readonly="readonly" style="cursor:pointer"/>
                        </div>  -->
                        <table>
                            <tr>
                                <td>
                                    <select class="chosen span2" name="Depart_Time_Hour">
                                        <option>--Hour--</option>

                                        @for($h = 0;$h <= 23;$h++)
                                            <option {{ explode(':',$trip_info->Depart_Time)[0] == sprintf("%02d", $h) ? 'selected="selected"' : '' }}>{{ sprintf("%02d", $h) }}</option>
                                        @endfor
                                    </select>
                                </td>
                                <td>
                                    <select class="chosen span2" name="Depart_Time_Minute">
                                        <option>--Minutes--</option>
                                        @for($m = 0;$m <= 59;$m++)
                                            <option {{ explode(':',$trip_info->Depart_Time)[1] == sprintf("%02d", $m) ? 'selected="selected"' : '' }}>{{ sprintf("%02d", $m) }}</option>
                                        @endfor                                        
                                    </select>
                                </td>
                            </tr>
                        </table>                       
                    </div>                                                          
                    <div class="control-group ">
                        <label class="control-label">Arrival Date <span class="required">*</span></label>
                        
                        <div class="input-prepend date datepicker">
                            <span class="add-on"><i class="icon-th"></i></span>
                            <input name="Arrival_Date" class="span4" type="text" value="{{ $trip_info->Arvl_Date }}" readonly="readonly" style="cursor:pointer"/>
                        </div>                          
                    </div>                                                          
                    <div class="control-group ">
                        <label class="control-label">Arrival Time <span class="required">*</span></label>

                       <!--  <div class="input-prepend date" style="padding: 4px;">
                            <span class="add-on"><i class="icon-time"></i></span>
                            <input name="Arrival_Time" class="span4" type="text" readonly="readonly" style="cursor:pointer"/>
                        </div>  -->   
                        <table>
                            <tr>
                                <td>
                                    <select class="chosen span2" name="Arrival_Time_Hour">
                                        <option>--Hour--</option>
                                        @for($h = 0;$h <= 23;$h++)
                                            <option {{ explode(':',$trip_info->Arvl_Time)[0] == sprintf("%02d", $h) ? 'selected="selected"' : '' }}>{{ sprintf("%02d", $h) }}</option>
                                        @endfor 
                                    </select>
                                </td>
                                <td>
                                    <select class="chosen span2" name="Arrival_Time_Minute">
                                        <option>--Minutes--</option>
                                        @for($m = 0;$m <= 59;$m++)
                                            <option {{ explode(':',$trip_info->Arvl_Time)[1] == sprintf("%02d", $m) ? 'selected="selected"' : '' }}>{{ sprintf("%02d", $m) }}</option>
                                        @endfor                                         
                                    </select>
                                </td>
                            </tr>
                        </table>                                               
                    </div>                                                                                                                           
                    <div class="control-group ">
                        <label class="control-label">Trip Operation Status</label>
                        <div class="controls">
                            <select class="chosen span2" name="status">
                                <option {{ $trip_info->status == 1 ? 'selected="selected"' : '' }} value="1">Active</option>
                                <option {{ $trip_info->status == 0 ? 'selected="selected"' : '' }} value="0">Inactive</option>
                            </select>
                        </div>
                    </div>                   
                </fieldset>
            </div>
        </div>
        
        <footer id="submit-actions" class="form-actions">
            <button id="submit-button" type="submit" class="btn btn-primary" value="">Create</button>
            <a href="{{ URL::to('/trips') }}" class="btn btn-default">Back</a>
        </footer>
    </form>
    <script>



        function vesselSeats(vessel_id){
            $('.trip-seats').empty();
            if(vessel_id != 0){
                var url = "{{ URL::to('/trips/seats') }}"+"/"+vessel_id;

                $.ajax({
                    url: url,
                    success: function (data) {
                        $('.trip-seats').html(data);
                    },
                    error: function (data) {
                        console.log('Error:', data);
                    }
                }); 
            }

        }

       
    </script>      
@endsection