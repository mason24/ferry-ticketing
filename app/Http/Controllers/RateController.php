<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Rate;

class RateController extends Controller
{
    public function index(){

        $rates = new Rate;

        if(request()->has('sort')){

            $rates = $rates->orderBy('Rate_Title',request('sort'));

        }

    	
        $rates = $rates->paginate(20)->appends([

            'sort' => request('sort')

        ]);

    	return view('Rates.index',compact('rates'));
    }

    public function create(){

        return view('Rates.create');

    }

    public function store(){

        $this->validate(request(),[

            'Rate_Title' => 'required',

            'Amount' => 'required'

        ]);

        $Rate = new Rate;

        $Rate->Rate_Title = request('Rate_Title');

        $Rate->Rate_Type = request('Rate_Type');

        $Rate->Amount = request('Amount');

        $Rate->Amount_Type = request('Amount_Type');

        $Rate->save();

        return redirect('rates');


    }

    public function show($id){

        $rates = new Rate;

        $rate = $rates->findOrFail($id);

        return view('rates.edit')->withTask($rate);
    }

    public function update($id){

        $rates = new Rate;

        $this->validate(request(),[

            'Rate_Title' => 'required',

            'Amount' => 'required'

        ]);

        $Rate = $rates->find($id);

        $Rate->Rate_Title = request('Rate_Title');

        $Rate->Rate_Type = request('Rate_Type');

        $Rate->Amount = request('Amount');

        $Rate->Amount_Type = request('Amount_Type');

        $Rate->save();

        return redirect('rates');

    }    

    public function delete($id){

        $rates = new Rate;

        $Rate = $rates->find($id);

        $Rate->delete();

        return redirect()->back();

    }
}
