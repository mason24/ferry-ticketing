<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', 'HomeController@index');

Route::get('/', 'DashboardController@index');

Route::post('/signin', 'SessionController@temp');

// Route::get('/bookings', 'BookingsController@index');

// Route::get('/bookings', 'BookingsController@index');

// Route::get('/routes', function(){

// 	$routes = DB::table('routes')->latest()->get();

// 	$routes = App\Route::paginate(20);

// 	return view('routes.index',compact('routes'));

// });

/*Routes*/

Route::get('/routes', 'RouteController@index');

Route::get('/routes/create', 'RouteController@create');

Route::post('/routes', 'RouteController@store');

Route::get('/routes/edit/{id}', 'RouteController@show');

Route::post('/routes/edit/{id}', 'RouteController@update');

Route::get('/routes/delete/{id}', 'RouteController@delete');

/*Jetties*/

Route::get('/jetties', 'JettyController@index');

Route::get('/jetties/create', 'JettyController@create');

Route::post('/jetties', 'JettyController@store');

Route::get('/jetties/edit/{id}', 'JettyController@show');

Route::post('/jetties/edit/{id}', 'JettyController@update');

Route::get('/jetties/delete/{id}', 'JettyController@delete');

/*Vessels*/

Route::get('/vessels', 'VesselController@index');

Route::get('/vessels/create', 'VesselController@create');

Route::post('/vessels', 'VesselController@store');

Route::get('/vessels/edit/{id}', 'VesselController@show');

Route::post('/vessels/edit/{id}', 'VesselController@update');

Route::get('/vessels/delete/{id}', 'VesselController@delete');

/*Rates*/

Route::get('/rates', 'RateController@index');

Route::get('/rates/create', 'RateController@create');

Route::post('/rates', 'RateController@store');

Route::get('/rates/edit/{id}', 'RateController@show');

Route::post('/rates/edit/{id}', 'RateController@update');

Route::get('/rates/delete/{id}', 'RateController@delete');

/*Citizen Rates*/
Route::get('/settings/citizen-rates', 'CitizenController@index');

Route::get('/settings/citizen-rates/new', 'CitizenController@create');

Route::post('/settings/citizen-rates/new', 'CitizenController@store');

Route::get('/settings/citizen-rates/edit/{id}', 'CitizenController@show');

Route::post('/settings/citizen-rates/edit/{id}', 'CitizenController@update');

Route::get('/settings/citizen-rates/delete/{id}', 'CitizenController@delete');

Route::get('/settings/citizen-rates/vessel-seats/{id}', 'CitizenController@vessel_seats');

/*User Groups*/

Route::get('/user-groups', 'UserGroupController@index');

Route::get('/user-groups/create', 'UserGroupController@create');

Route::post('/user-groups', 'UserGroupController@store');

Route::get('/user-groups/edit/{id}', 'UserGroupController@show');

Route::post('/user-groups/edit/{id}', 'UserGroupController@update');

Route::get('/user-groups/delete/{id}', 'UserGroupController@delete');


/*Users*/

Route::get('/register', 'RegistrationController@create');

Route::post('/register', 'RegistrationController@store');

Route::get('/login', ['as' => 'login', 'uses' => 'SessionsController@create']);

Route::post('/login', 'SessionsController@store');

Route::get('/logout', 'SessionsController@destroy');

Route::get('pos/logout', 'SessionsController@destroy');

/*Users*/

Route::get('/users', 'SessionsController@users');

Route::get('/users/delete/{id}', 'SessionsController@delete');

Route::get('/users/edit/{id}', 'SessionsController@show');

Route::post('/users/edit/{id}', 'SessionsController@update');

/*Booking*/

Route::get('/bookings', 'BookingsController@index');

Route::get('/bookings/new', 'BookingsController@create');

Route::get('/bookings/edit/{id}', 'BookingsController@update');

Route::get('/bookings/delete/{id}', 'BookingsController@delete');

Route::get('/bookings/edit/{id}', 'BookingsController@show');

Route::post('/bookings/edit/{id}', 'BookingsController@update');

/*Trips*/
// Route::get('/trips', function(){

// 	$trips = DB::table('trips')->latest()->get();

// 	$trips = App\Trip::paginate(20);

// 	return view('trips.index',compact('trips'));

// });

Route::get('/trips', 'TripController@index');

Route::get('/trips/create', 'TripController@create');

Route::post('/trips/create', 'TripController@store');

Route::get('/trips/edit/{id}', 'TripController@show');

Route::get('/trips/seats/{id}', 'VesselController@seats');

Route::get('/trips/delete/{id}', 'TripController@delete');

/*AJAX Test*/
Route::get('/bookings/ajax', function(){

	echo 'AJAX was called';

});

Route::get('/open-ticket', 'OpenTicketController@index');

Route::get('/open-ticket/new', 'OpenTicketController@create');

Route::post('/open-ticket/new', 'OpenTicketController@store');

Route::get('/open-ticket/code', 'OpenTicketController@code');


Route::get('/group', 'CustomerGroupController@index');

Route::get('/group/new', 'CustomerGroupController@create');

Route::post('/group', 'CustomerGroupController@store');

Route::get('/group/edit/{id}', 'CustomerGroupController@show');

Route::post('/group/edit/{id}', 'CustomerGroupController@update');

Route::get('/group/delete/{id}', 'CustomerGroupController@delete');

Route::get('/customer', 'CustomerController@index');

Route::get('/customer/new', 'CustomerController@create');

Route::post('/customer/new', 'CustomerController@store');

Route::get('/customer/edit/{id}', 'CustomerController@show');

Route::post('/customer/edit/{id}', 'CustomerController@update');

Route::get('/customer/delete/{id}', 'CustomerController@delete');

Route::get('/dashboard', 'DashboardController@index');

Route::get('/report/sales', 'BookingsController@report');

Route::get('/report/daily-sales', 'BookingsController@daily_sales');

/*Settings*/
Route::get('/settings', 'SettingController@system');

Route::get('/settings/system', 'SettingController@system');

Route::get('/settings/company', 'SettingController@company');

Route::get('/settings/open-ticket', 'SettingController@open_ticket_index');

Route::get('/settings/open-ticket/new', 'SettingController@open_ticket_create');

Route::post('/settings/open-ticket/new', 'SettingController@open_ticket_store');

Route::get('/settings/open-ticket/new', 'SettingController@open_ticket_create');

Route::get('/settings/open-ticket/edit', 'SettingController@open_ticket_edit');

Route::post('/settings/open-ticket/edit', 'SettingController@open_ticket_update');

Route::get('/settings/open-ticket/delete', 'SettingController@open_ticket_delete');

/*POS*/
Route::get('/pos', 'PosController@index');

Route::post('/pos/open', 'PosController@open');

Route::get('/pos/details/{id}', 'PosController@details');

Route::get('/pos/close/{id}', 'PosController@close_details');

Route::post('/pos/close', 'PosController@close');

Route::get('/pos/open-ticket/details/{id}', 'PosController@details');

Route::get('/pos/open-ticket/retrieve/{code}/{origin}/{destination}', 'OpenTicketController@check_code');

Route::get('/pos/open-ticket/close/{id}', 'PosController@close_details');

Route::post('/pos/open-ticket/close', 'PosController@close');

Route::post('/pos/process', 'PosController@store');

Route::get('/pos/destination/{id}', 'PosController@destination');

Route::get('/pos/trips/{from}/{to}/{type}/{date}', 'PosController@trips');

Route::get('/pos/availability/{tripid}/{type}/{origin}/{destination}', 'PosController@availability');

Route::get('/pos/passenger-details/{total_adults}/{total_children}', 'PosController@passenger_details');

/*Open Ticket*/

Route::get('/pos/open-ticket', 'OpenTicketController@pos_index');

Route::post('/pos/open-ticket', 'OpenTicketController@store');

Route::get('/pos/open-ticket/redeem', 'PosController@redeem');

Route::post('/pos/open-ticket/create', 'OpenTicketController@store');

Route::get('/pos/open-ticket/destination/{id}', 'PosController@destination');

Route::get('/pos/open-ticket/code', 'OpenTicketController@code');

Route::get('/pos/open-ticket/details', 'PosController@open_ticket_details');

Route::get('/pos/open-ticket/seat-category/{origin}/{destination}', 'OpenTicketController@seat_category');

Route::get('/pos/open-ticket/print/{booking_id}', 'OpenTicketController@print_ticket');

Route::get('/pos/print/{booking_id}', 'PosController@print_ticket');

/*Seat categories*/
Route::get('/seat-category', 'SeatCategoryController@index');

Route::get('/seat-category/new', 'SeatCategoryController@create');
