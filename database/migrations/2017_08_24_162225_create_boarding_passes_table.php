<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBoardingPassesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('boarding_passes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('way',20);
            $table->string('boarding_pass_id');
            $table->string('booking_id');
            $table->integer('selected_trip_id')->unsigned();
            $table->foreign('selected_trip_id')->references('id')->on('trips')->onDelete('cascade');
            $table->integer('seat_no');
            $table->integer('seat_category_id')->unsigned();
            $table->foreign('seat_category_id')->references('id')->on('seat_categories')->onDelete('cascade');
            $table->integer('passenger_id')->unsigned();
            $table->foreign('passenger_id')->references('id')->on('passengers')->onDelete('cascade');
            $table->enum('status', ['not_on_board', 'on_board']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('boarding_passes');
    }
}
