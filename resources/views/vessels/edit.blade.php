<!-- @extends('layout.master')
@section('content')
    @include('layout.errors')
    <form method="post" action="{{ URL::to('/vessels') }}">
        {{ csrf_field() }}
        <div class="row">
            <div id="acct-password-row" class="span7">
                <fieldset>
                    <legend>New Vessel</legend><br>
                    <div class="control-group ">
                        <label for="Vessel_Code" class="control-label">Vessel Code <span class="required">*</span></label>
                        <div class="controls">
                            <input id="current-pass-control" name="Vessel_Code" class="span4" type="text" value="{{ $vessel->Vessel_Code }}"/>

                        </div>
                    </div>                                        
                    <div class="control-group ">
                        <label for="Vessel_Name" class="control-label">Vessel Name <span class="required">*</span></label>
                        <div class="controls">
                            <input id="current-pass-control" name="Vessel_Name" class="span4" type="text" value="{{ $vessel->Vessel_Name }}"/>

                        </div>
                    </div> 
                    <div class="control-group ">
                        <div class="controls">
                            <button class="btn btn-default" onclick="addCategory()" type="button">Add new seat category</button>
                        </div>
                    </div>
                    <div class="control-group ">
                        <div class="controls">
                            <table class="seat-category-table table-bordered" cellpadding="5">
                                <thead>
                                    <th>Select Seat Category</th>
                                    <th>Capacity</th>
                                    <th>Display Capacity in Online Booking</th>
                                    <th></th>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>                                                           
                </fieldset>
            </div>
        </div>
        <footer id="submit-actions" class="form-actions">
            <button id="submit-button" type="submit" class="btn btn-primary" value="">Create</button>
            <a href="{{ URL::to('/vessels') }}" class="btn btn-default">Back</a>
        </footer>
    </form>
<div class="seat_category_selection" style="display:none">
<select name="seat_category[]">
    @foreach($seat_categories as $seat_category)
        <option value="{{ $seat_category->id }}">{{ $seat_category->Seat_Category_Name }}</option>
    @endforeach
</select>
</div> 
<script>
function addCategory(){

    var seat_category_selection = $('.seat_category_selection').html();

    var html = '<tr><td>'+seat_category_selection+'</td><td><input type="number" style="width:150px" name="capacity[]"/></td><td><input type="number" style="width:150px" name="display-capacity[]"/></td><td><button onclick="$(this).closest(\'tr\').remove()" type="button" class="btn btn-sm btn-remove">Remove</button></td></tr>';

    $('.seat-category-table tbody').append(html);
}
</script>
@endsection -->