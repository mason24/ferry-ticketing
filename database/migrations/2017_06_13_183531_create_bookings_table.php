<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bookings', function (Blueprint $table) {
            $table->string('booking_id');
            $table->string('journey');
            $table->integer('origin_id')->unsigned();
            $table->foreign('origin_id')->references('id')->on('jetties')->onDelete('cascade');
            $table->integer('destination_id')->unsigned();
            $table->foreign('destination_id')->references('id')->on('jetties')->onDelete('cascade');
            $table->integer('selected_onward_trip_id')->unsigned()->nullable();
            $table->foreign('selected_onward_trip_id')->references('id')->on('trips')->onDelete('cascade');
            $table->integer('selected_return_trip_id')->unsigned()->nullable();
            $table->foreign('selected_return_trip_id')->references('id')->on('trips')->onDelete('cascade');
            $table->integer('total_adults');
            $table->integer('total_children');
            $table->double('total_tax',12,2);
            $table->double('total_discount',12,2);
            $table->double('grand_total_amount',12,2);
            $table->integer('created_by')->unsigned();
            $table->foreign('created_by')->references('id')->on('users')->onDelete('cascade');
            $table->enum('source', ['counter', 'online_booking']);
            $table->integer('pos_register_id')->default(0)->nullable();
            $table->date('open_ticket_expiry_date')->nullable();
            $table->enum('payment_mode', ['cash', 'credit_card','open_ticket']);
            $table->tinyInteger('booking_status')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bookings');
    }
}
