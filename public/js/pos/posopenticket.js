var site_url = window.location.href;
   
$(document).ready(function(){
     

    $(".origin-button").click(function () {

            var originID = $(this).attr('rel');

            $('input[name="origin"]').val(originID);

            $('.origintext').html($(this).attr('data-origin-code'));

            var url = site_url+"/destination/"+originID;

            $.ajax({
                url: url,
                success: function (data) {
                    $('.destination-list').html(data);
                },
                error: function (data) {
                    console.log('Error:', data);
                }
            }); 
        

    });

    $(".expiry-date").change(function () {

            $('.onwarddatetext').html($('.expiry-date').val());

            $('input[name=\'expiry_date\']').val($('.expiry-date').val());

            var originID = $('input[name="origin"]').val();

            var destinationID = $('input[name="destination"]').val();

            var url = site_url+"/seat-category/"+originID+"/"+destinationID;

            console.log(url);

            $.ajax({
                url: url,
                success: function (data) {
                    $('.passenger-selection').html(data);
                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });             

    });

    $("#payment").click(function () {

            $('#formOpenTicket').submit();
        
            setTimeout(function(){ location.reload(true); }, 500);
        

    });          

});

function select_destination(elem){

    $('input[name=\'destination\']').val(elem.attr('rel'));

    $('.destinationtext').html(elem.attr('data-destination-code'));

}

function total_passenger(elem){

    if(elem.attr('data-trip-type') == 'onward'){

        var total_passengers = 0;

        var total_adults = 0;

        var total_children = 0; 

        // var total_adults = $('.total_adults').val() == '' ? 0 : $('.total_adults').val();

        // var total_children = $('.total_children').val() == '' ? 0 : $('.total_children').val();

        for(var a=0;a<$('.onward_adults').length;a++){

            total_adults += parseInt($('.onward_adults').eq(a).val());

        }

        $('input[name="total_adults"]').val(total_adults);

        for(var c=0;c<$('.onward_children').length;c++){

            total_children += parseInt($('.onward_children').eq(c).val());

        }

        $('input[name="total_children"]').val(total_children);  

        total_passengers = total_adults + total_children;

        $('#total-passengers').html(total_passengers);

        // var url = site_url+"/passenger-details/"+total_adults+"/"+total_children;

        // $.ajax({
        //     url: url,
        //     success: function (data) {
        //         $('.passenger-details-body').html(data);
        //     },
        //     error: function (data) {
        //         console.log('Error:', data);
        //     }
        // }); 

    }

}

function registerDetails(){

    var posRegisterID = $('input[name="pos_register_id"]').val();

        var url = site_url+"/details/"+posRegisterID;

        $.ajax({
            url: url,
            success: function (data) {

                $('.cash_register_details').html(data);
            },
            error: function (data) {
                console.log('Error:', data);
            }
        }); 
    

}

function update_ticket_details(){

    var html = '';

    var passenger_type_text = '';

    var vessel_seat_name = '';

    var gtotal = 0;

    var total_adults = 0;

    var total_children = 0;

    var num_of_passengers = 0;

    var passenger_index = 0;

    var passenger_number = 1;

    for(var i=0;i<$('.adults').length;i++){

        if($('.adults').eq(i) != '0'){

            vessel_seat_category_id = $('.adults').eq(i).attr('data-seat-category-id');

            vessel_seat_name = $('.adults').eq(i).attr('data-seat-category-name');

            var total_passengers = parseInt($('.adults').eq(i).val());

            for(var j=0;j<total_passengers;j++){

                // var price = parseFloat($('.onward-passengers').eq(i).attr('data-price'));

                var price = parseFloat($('.adults').eq(i).attr('data-price'));



                html += '<div class="col-md-8"><div class="form-group"><span style="font-size:12px">P'+passenger_number+' Adult ('+vessel_seat_name+') <a href="#" data-toggle="modal" data-target="#onward_passenger_details_'+passenger_number+'_modal"><i class="fa fa-edit"></i></a> <input type="checkbox" onchange="changePrice($(this),\'Adult\','+vessel_seat_category_id+','+passenger_number+')" name="passenger_citizen_'+passenger_index+'"/> Citizen</span></div></div><div class="col-md-4"><div class="form-group"><div class="text-right"><label class="passenger-price-text-'+passenger_number+'">'+price.toFixed(2)+'</label></div></div></div>';

                html += ' <div class="modal fade" id="onward_passenger_details_'+passenger_number+'_modal" role="dialog"> <div class="modal-dialog"> <div class="modal-content"> <div class="modal-header"> <button type="button" class="close" data-dismiss="modal">&times;</button> <h4 class="modal-title">Passenger '+passenger_number+' Details</h4> </div><div class="modal-body"> <div class="row"> <div class="col-md-6"> <div class="form-group"> <label>Full Name</label> <input type="text" name="passenger_name[]" class="form-control"/> </div></div><div class="col-md-6"> <div class="form-group"> <label>Identity Type</label> <select class="form-control" name="id_type[]"> <option>Mykad</option> <option>Mykid</option> <option>Passport</option> </select> </div></div><div class="col-md-6"> <div class="form-group"> <label>Identity Number</label> <input type="text" name="identity_number[]" class="form-control"/> </div></div></div></div><div class="modal-footer"> <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> </div></div></div></div>';

                html += '<input type="hidden" rel="'+j+'" class="passenger_original_price_'+passenger_number+'" value="'+price.toFixed(2)+'"/>';

                html += '<input type="hidden" class="passenger_current_price passenger_price_'+passenger_number+'" name="passenger_price[]" value="'+price.toFixed(2)+'"/>';

                html += '<input type="hidden" name="passenger_seat_category_id[]" value="'+vessel_seat_category_id+'"/>';

                html += '<input type="hidden" name="passenger_type[]" value="Adult"/>';

                gtotal+=price;

                passenger_number++;

                passenger_index++;

                num_of_passengers++;

                total_adults++;

            }

        }


        
    }

    $('input[name="total_adults"]').val(total_adults);

    for(var i=0;i<$('.children').length;i++){

        if($('.children').eq(i) != '0'){

            vessel_seat_category_id = $('.children').eq(i).attr('data-seat-category-id');

            vessel_seat_name = $('.children').eq(i).attr('data-seat-category-name');

            var total_passengers = parseInt($('.children').eq(i).val());

            for(var j=0;j<total_passengers;j++){

                // var price = parseFloat($('.onward-passengers').eq(i).attr('data-price'));

                var price = parseFloat($('.children').eq(i).attr('data-price'));



                html += '<div class="col-md-8"><div class="form-group"><span style="font-size:12px">P'+passenger_number+' Child ('+vessel_seat_name+') <a href="#" data-toggle="modal" data-target="#onward_passenger_details_'+passenger_number+'_modal"><i class="fa fa-edit"></i></a> <input type="checkbox" onchange="changePrice($(this),\'Child\','+vessel_seat_category_id+','+passenger_number+')" name="passenger_citizen_'+passenger_index+'"/> Citizen</span></div></div><div class="col-md-4"><div class="form-group"><div class="text-right"><label class="passenger-price-text-'+passenger_number+'">'+price.toFixed(2)+'</label></div></div></div>';

                html += ' <div class="modal fade" id="onward_passenger_details_'+passenger_number+'_modal" role="dialog"> <div class="modal-dialog"> <div class="modal-content"> <div class="modal-header"> <button type="button" class="close" data-dismiss="modal">&times;</button> <h4 class="modal-title">Passenger '+passenger_number+' Details</h4> </div><div class="modal-body"> <div class="row"> <div class="col-md-6"> <div class="form-group"> <label>Full Name</label> <input type="text" name="passenger_name[]" class="form-control"/> </div></div><div class="col-md-6"> <div class="form-group"> <label>Identity Type</label> <select class="form-control" name="id_type[]"> <option>Mykad</option> <option>Mykid</option> <option>Passport</option> </select> </div></div><div class="col-md-6"> <div class="form-group"> <label>Identity Number</label> <input type="text" name="identity_number[]" class="form-control"/> </div></div></div></div><div class="modal-footer"> <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> </div></div></div></div>';

                html += '<input type="hidden" rel="'+j+'" class="passenger_original_price_'+passenger_number+'" value="'+price.toFixed(2)+'"/>';

                html += '<input type="hidden" class="passenger_current_price passenger_price_'+passenger_number+'" name="passenger_price[]" value="'+price.toFixed(2)+'"/>';

                html += '<input type="hidden" name="passenger_seat_category_id[]" value="'+vessel_seat_category_id+'"/>';

                html += '<input type="hidden" name="passenger_type[]" value="Child"/>';

                gtotal+=price;

                passenger_number++;

                passenger_index++;

                num_of_passengers++;

                total_children++;

            }

        }


        
    }   

    $('input[name="total_children"]').val(total_children); 

    $('.panel-passengers').html(html);

    $('#total-passengers').html(num_of_passengers);

    // $('#total').html(gtotal.toFixed(2));

    // $('input[name="subtotal"]').val(gtotal.toFixed(2));

    // var discount_amount = parseFloat($('input[name="discount"]').val());

    // var new_gtotal = gtotal - discount_amount;

    $('#gtotal').html(gtotal.toFixed(2));

    $('input[name="gtotal"]').val(gtotal.toFixed(2));


}

function incrementvalue(elem){


    // Stop acting like a button
    // e.preventDefault();
    // Get the field name
    fieldName = elem.attr('field');

    // alert(fieldName);
    // Get its current value
    var currentVal = parseInt($('input[name='+fieldName+']').val());


    // If is not undefined
    if (!isNaN(currentVal)) {
        // Increment
        $('input[name='+fieldName+']').val(currentVal + 1);
    } else {
        // Otherwise put a 0 there
        $('input[name='+fieldName+']').val(0);
    }

    update_ticket_details();

}

function closeRegister(){

    var posRegisterID = $('input[name="pos_register_id"]').val();

        var url = site_url+"/close/"+posRegisterID;

        $.ajax({
            url: url,
            success: function (data) {

                $('.close_register_details').html(data);

                $('.total_cash_submitted').focus();
            },
            error: function (data) {
                console.log('Error:', data);
            }
        }); 
    

}

function changePrice(elem,passengerType,seatCategoryID,passengerNum){

    var price = 0;

    if(elem.is(":checked")){

        if(passengerType == 'Adult'){

            price = parseFloat($('.adult_citizen_price_'+seatCategoryID).val());

            $('.passenger-price-text-'+passengerNum).html(price.toFixed(2));

            $('.passenger_price_'+passengerNum).val(price.toFixed(2));

        }
        else{

            price = parseFloat($('.child_citizen_price_'+seatCategoryID).val());

            $('.passenger-price-text-'+passengerNum).html(price.toFixed(2));

            $('.passenger_price_'+passengerNum).val(price.toFixed(2));
        }

    }else{

        price = parseFloat($('.passenger_original_price_'+passengerNum).val());

        $('.passenger-price-text-'+passengerNum).html(price.toFixed(2));

        $('.passenger_price_'+passengerNum).val(price.toFixed(2));      

    }

    calculate_grand_total()

}
function calculate_grand_total(){

    var subtotal = 0;

    $(".passenger_current_price").each(function(){

        subtotal += parseFloat($(this).val());
        
    });

    var discount_amount = 0;

    // var discount_amount = $('input[name="discount"]').val() == '' ? 0 : parseFloat($('input[name="discount"]').val());

    $('#total').html(subtotal.toFixed(2));

    $('input[name="subtotal"]').val(subtotal.toFixed(2));

    var new_gtotal = subtotal - discount_amount;

    $('input[name="gtotal"]').val(new_gtotal.toFixed(2));

    $('#gtotal').html(new_gtotal.toFixed(2));    
}
// This button will decrement the value till 0


// $(".qtyminus").click(function(e) {

function decrementvalue(elem){  
    // Stop acting like a button
    // e.preventDefault();
    // Get the field name
    fieldName = elem.attr('field');
    // Get its current value
    var currentVal = parseInt($('input[name='+fieldName+']').val());
    // If it isn't undefined or its greater than 0
    if (!isNaN(currentVal) && currentVal > 0) {
        // Decrement one
        $('input[name='+fieldName+']').val(currentVal - 1);
    } else {
        // Otherwise put a 0 there
        $('input[name='+fieldName+']').val(0);
    }

    update_ticket_details();


}
