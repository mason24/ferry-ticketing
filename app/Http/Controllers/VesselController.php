<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;

use App\Vessel;

use App\Vessel_Seat;

use App\SeatCategory;

class VesselController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }     
    public function index(){

        $vessels = new Vessel;

        if(request()->has('sort')){

            $vessels = $vessels->orderBy('Vessel_Name',request('sort'));

        }

    	
        $vessels = $vessels->paginate(20)->appends([

            'sort' => request('sort')

        ]);

    	return view('vessels.index',compact('vessels'));
    }

    public function create(){

        $seat_categories = SeatCategory::orderBy('Seat_Category_Name')->get();

        return view('vessels.create',compact('seat_categories'));

    }

    public function store(){

        $this->validate(request(),[

            'Vessel_Name' => 'required',

            'Vessel_Code' => 'required'

        ]);

        $Vessel = new Vessel;

        // $Vessel->Message_ID = '111';

        // $Vessel->Company_Code = '200';

        // $Vessel->Vessel_Name = request('Vessel_Name');

        // $Vessel->Vessel_Code = request('Vessel_Code');

        // $Vessel->Capacity = 10;

        // $Vessel->save();

        $vessel_id = $Vessel->insertGetId([
            
                'Message_ID' => '111',

                'Company_Code' => '200',

                'Vessel_Name' => request('Vessel_Name'),

                'Vessel_Code' => request('Vessel_Code'),

                'Capacity' => 10
            
        ]);         

        $vessel_seat = new Vessel_Seat;

        $seat_categories = request('seat_category');

        $capacity = request('capacity');



        // for($i=0;$i<count($seat_categories);$i++){

        //         $vessel_seat->timestamps = false;

        //         $vessel_seat->vessel_id = 1;

        //         $vessel_seat->seat_category_name = $seat_categories[$i];

        //         $vessel_seat->capacity = $capacity[$i];

        //         $vessel_seat->save();

        // }
        for($i=0;$i<count($seat_categories);$i++){

            $vessel_seat->insert([
                [
                    'vessel_id' => $vessel_id,

                    'seat_category_id' => $seat_categories[$i],

                    'capacity' => $capacity[$i]
                ]
            ]); 
                         
        }
      

        return redirect('vessels');
    }

    public function show($id){

        $vessels = new Vessel;

        $vessel = $vessels->findOrFail($id);

        return view('vessels.edit',compact('vessel'));
    }

    public function update($id){

        $vessels = new Vessel;

        $this->validate(request(),[

            'Company_Code' => 'required',

            'Vessel_Name' => 'required',

            'Vessel_Code' => 'required',

            'Capacity' => 'required'

        ]);

        $Vessel = $vessels->find($id);

        $Vessel->Message_ID = '111';

        $Vessel->Vessel_Name = request('Vessel_Name');

        $Vessel->Vessel_Code = request('Vessel_Code');

        $Vessel->Vessel_Code = request('Vessel_Code');

        $Vessel->Capacity = request('Capacity');        

        $Vessel->save();

        return redirect('vessels');

    }    

    public function delete($id){

        $vessels = new Vessel;

        $Vessel = $vessels->find($id);

        $Vessel->delete();

        return redirect()->back();

    }

    public function seats($vessel_id){

        $seats_array = array();

        $seats = DB::table('vessel__seats')->select('*')->where('vessel_id','=',$vessel_id)->get();

        foreach($seats as $seat){
            
            $seat_category = SeatCategory::select('Seat_Category_Name')->where('id','=',$seat->seat_category_id)->first();

            array_push($seats_array,array(

                'id' => $seat->id,

                'seat_category_name' => $seat_category->Seat_Category_Name,

                'vessel_id' => $seat->vessel_id,

                'seat_category_id' => $seat->seat_category_id,

                'capacity' => $seat->capacity

            ));

        }

        return view('vessels.seats',compact('seats_array'));

    }
}
