<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\SeatCategory;

class SeatCategoryController extends Controller
{
    public function index(){

        if(request()->has('sortByName')){

            SeatCategory::orderBy('Seat_Category_Name',request('sort'));

        }

        
        $seat_categories = SeatCategory::paginate(20)->appends([

            'sort' => request('sort')

        ]);

        return view('seat_categories.index',compact('seat_categories'));
    }

    public function store(){

        $this->validate(request() , [

         'Trip_Code' => 'required',

         'Trip_Name' => 'required',
         
         'vessel' => 'required|not_in:0',

         'Depart_Date' => 'required',

         'Arrival_Date' => 'required',

         'Depart_Time_Hour' => 'required|not_in:--Hour--',

         'Depart_Time_Minute' => 'required|not_in:--Minutes--',

         'Arrival_Time_Hour' => 'required|not_in:--Hour--',
         
         'Arrival_Time_Minute' => 'required|not_in:--Minutes--'

        ]);

        $Trip = new Trip;

        $Trip->Message_ID = '111';

        $Trip->Company_Code = '200';

        $Trip->Trip_Code = request('Trip_Code');

        $Trip->Trip_Name = request('Trip_Name');

        $Trip->Vessel_Id = request('vessel');

        $Trip->Route_Id = request('route');

        $Trip->Depart_Date = date('Y-m-d', strtotime(request('Depart_Date')));

        $Trip->Arvl_Date = date('Y-m-d', strtotime(request('Arrival_Date')));;

        $Trip->Depart_Time = request('Depart_Time_Hour').':'.request('Depart_Time_Minute');

        $Trip->Arvl_Time = request('Arrival_Time_Hour').':'.request('Arrival_Time_Minute');

        $Trip->status = request('status');

        $Trip->save();

        $trip_id = $Trip->id;

        $trip_price = new TripPrices;

        $adult_seats = request('adult-price');

        $child_seats = request('child-price');

        foreach($adult_seats as $seat_id => $price){

            $trip_price->insert([
                [
                    'trip_id' => $trip_id,

                    'vessel_seat_id' => $seat_id,

                    'adult_price' => $price,

                    'child_price' => $child_seats[$seat_id]
                ]
            ]); 
                         
        }

        return redirect('/trips');

    }

    function create(){

        $vessel = new Vessel;

        $route = new Route;

        $vessels = $vessel->orderBy('Vessel_Name')->get();  

        $routes = $route->orderBy('Route_Name')->get();        

        return view('trips.create', ['vessels'=> $vessels , 'routes'=>$routes]);     

    }
}
