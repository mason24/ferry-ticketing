@extends('layout.master')
@section('content')
    @include('layout.errors')
    <form method="post" action="{{ URL::to('/trips/create') }}">
        {{ csrf_field() }}
        <div class="row">
            <div id="acct-password-row" class="span7">
                <fieldset>
                    <legend>New Trip</legend><br>
                    <!-- <div class="control-group ">
                        <label for="Trip_Code" class="control-label">Trip Code <span class="required">*</span></label>
                        <div class="controls">
                            <input id="current-pass-control" name="Trip_Code" class="span4" type="text" value=""/>

                        </div>
                    </div>
                    <div class="control-group ">
                        <label class="control-label">Trip Name <span class="required">*</span></label>
                        <div class="controls">
                            <input id="current-pass-control" name="Trip_Name" class="span4" type="text" value=""/>

                        </div>
                    </div> -->
                    <div class="control-group ">
                        <label class="control-label">Select Route <span class="required">*</span></label>
                        <div class="controls">
                            <select name="route" class="chosen span4" data-placeholder="Select">
                                <option value="0">--Choose--</option>
                            @foreach($routes as $route)
                                <option value="{{ $route->id }}">{{ $route->Route_Name }}</option>
                            @endforeach
                            </select>

                        </div>
                    </div>                    
                    <div class="control-group ">
                        <label class="control-label">Operating Vessel <span class="required">*</span></label>
                        <div class="controls">
                            <select name="vessel" class="chosen span4" data-placeholder="Select" onchange="vesselSeats($(this).val())">
                                <option value="0">--Choose--</option>
                            @foreach($vessels as $vessel)
                                <option value="{{ $vessel->id }}">{{ $vessel->Vessel_Name }}</option>
                            @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="control-group trip-seats">
                                               
                    </div>                                                                              
                    <div class="control-group ">
                        <label class="control-label">Depart Date <span class="required">*</span></label>
                  
                        <div class="input-prepend date datepicker">
                            <span class="add-on"><i class="icon-th"></i></span>
                            <input name="Depart_Date" class="span4" type="text" value="{{ date('m/d/Y') }}" readonly="readonly" style="cursor:pointer"/>
                        </div>                          
                    </div>                                                          
                    <div class="control-group ">
                        <label class="control-label">Depart Time <span class="required">*</span></label>

<!--                         <div class="input-prepend date" style="padding: 4px;">
                            <span class="add-on"><i class="icon-time"></i></span>
                            <input name="Depart_Time" class="span4" type="text" readonly="readonly" style="cursor:pointer"/>
                        </div>  -->
                        <table>
                            <tr>
                                <td>
                                    <select class="chosen span2" name="Depart_Time_Hour">
                                        <option>--Hour--</option>
                                        @for($h = 0;$h <= 23;$h++)
                                            <option>{{ sprintf("%02d", $h) }}</option>
                                        @endfor
                                    </select>
                                </td>
                                <td>
                                    <select class="chosen span2" name="Depart_Time_Minute">
                                        <option>--Minutes--</option>
                                        @for($m = 0;$m <= 59;$m++)
                                            <option>{{ sprintf("%02d", $m) }}</option>
                                        @endfor                                        
                                    </select>
                                </td>
                            </tr>
                        </table>                       
                    </div>                                                          
                    <div class="control-group ">
                        <label class="control-label">Arrival Date <span class="required">*</span></label>
                        
                        <div class="input-prepend date datepicker">
                            <span class="add-on"><i class="icon-th"></i></span>
                            <input name="Arrival_Date" class="span4" type="text" value="{{ date('m/d/Y') }}" readonly="readonly" style="cursor:pointer"/>
                        </div>                          
                    </div>                                                          
                    <div class="control-group ">
                        <label class="control-label">Arrival Time <span class="required">*</span></label>

                       <!--  <div class="input-prepend date" style="padding: 4px;">
                            <span class="add-on"><i class="icon-time"></i></span>
                            <input name="Arrival_Time" class="span4" type="text" readonly="readonly" style="cursor:pointer"/>
                        </div>  -->   
                        <table>
                            <tr>
                                <td>
                                    <select class="chosen span2" name="Arrival_Time_Hour">
                                        <option>--Hour--</option>
                                        @for($h = 0;$h <= 23;$h++)
                                            <option>{{ sprintf("%02d", $h) }}</option>
                                        @endfor 
                                    </select>
                                </td>
                                <td>
                                    <select class="chosen span2" name="Arrival_Time_Minute">
                                        <option>--Minutes--</option>
                                        @for($m = 0;$m <= 59;$m++)
                                            <option>{{ sprintf("%02d", $m) }}</option>
                                        @endfor                                         
                                    </select>
                                </td>
                            </tr>
                        </table>                                               
                    </div>                                                                                                                           
                    <div class="control-group ">
                        <label class="control-label">Trip Operation Status</label>
                        <div class="controls">
                            <select class="chosen span2" name="status">
                                <option value="1">Active</option>
                                <option value="0">Inactive</option>
                            </select>
                        </div>
                    </div>                   
                </fieldset>
            </div>
        </div>
        <footer id="submit-actions" class="form-actions">
            <button id="submit-button" type="submit" class="btn btn-primary" value="">Create</button>
            <a href="{{ URL::to('/trips') }}" class="btn btn-default">Back</a>
        </footer>
    </form>
    <script>



        function vesselSeats(vessel_id){
            $('.trip-seats').empty();
            if(vessel_id != 0){
                var url = "{{ URL::to('/trips/seats') }}"+"/"+vessel_id;

                $.ajax({
                    url: url,
                    success: function (data) {
                        $('.trip-seats').html(data);
                    },
                    error: function (data) {
                        console.log('Error:', data);
                    }
                }); 
            }

        }

       
    </script>      
@endsection