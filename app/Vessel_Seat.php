<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vessel_Seat extends Model
{
    protected $fillable = [
        'vessel_id'
    ];	
	public function vessel(){
		return $this->belongsTo('App\Vessel');
	}
	public function seat_category(){
		return $this->belongsTo('App\SeatCategory');
	}	
    
}
