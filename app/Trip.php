<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Trip extends Model
{
    public function route(){

    	return $this->belongsTo('App\Route','Route_Id');

    }

    public function bookings(){

    	return $this->belongsTo('App\Booking','selected_onward_trip_id');

    }
    public function vessel(){

        return $this->belongsTo('App\Vessel','Vessel_Id');

    }     
}
