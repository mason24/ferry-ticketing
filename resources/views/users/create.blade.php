@extends('layout.master')
@section('content')
    @include('layout.errors')
    <form method="post" action="{{ URL::to('/vessels') }}">
        {{ csrf_field() }}
        <div class="row">
            <div id="acct-password-row" class="span7">
                <fieldset>
                    <legend>New User</legend><br>
                    <div class="control-group ">
                        <label for="Company_Code" class="control-label">Username <span class="required">*</span></label>
                        <div class="controls">
                            <input id="current-pass-control" name="username" class="span4" type="text" value=""/>

                        </div>
                    </div>
                    <div class="control-group ">
                        <label for="Vessel_Code" class="control-label">Password <span class="required">*</span></label>
                        <div class="controls">
                            <input id="current-pass-control" name="password" class="span4" type="text" value=""/>

                        </div>
                    </div>                                        
                    <div class="control-group ">
                        <label for="Vessel_Name" class="control-label">Password Confirmation <span class="required">*</span></label>
                        <div class="controls">
                            <input id="current-pass-control" name="password_confirmation" class="span4" type="text" value=""/>

                        </div>
                    </div> 
                    <div class="control-group ">
                        <label for="Capacity" class="control-label">Display Name <span class="required">*</span></label>
                        <div class="controls">
                            <input id="current-pass-control" name="display_name" class="span4" type="text" value=""/>

                        </div>
                    </div>
                    <div class="control-group ">
                        <label for="Capacity" class="control-label">Select Branch <span class="required">*</span></label>
                        <div class="controls">
                            <select name="branch" class="chosen span4">
                            </select>

                        </div>
                    </div>                                                           
                </fieldset>
            </div>
        </div>
        <footer id="submit-actions" class="form-actions">
            <button id="submit-button" type="submit" class="btn btn-primary" value="">Create</button>
            <a href="{{ URL::to('/vessels') }}" class="btn btn-default">Back</a>
        </footer>
    </form>
@endsection