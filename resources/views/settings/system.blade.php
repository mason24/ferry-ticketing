@extends('layout.master')
@section('content')
    @include('layout.errors')
    <form method="post" action="{{ URL::to('/vessels') }}">
        {{ csrf_field() }}
        <div class="row">
            <div id="acct-password-row" class="span7">
                <fieldset>
                    <legend>System</legend><br>
                    <div class="control-group ">
                        <label for="Vessel_Code" class="control-label">Site Name <span class="required">*</span></label>
                        <div class="controls">
                            <input id="current-pass-control" name="site_name" class="span4" type="text" value=""/>

                        </div>
                    </div> 
                    <div class="control-group ">
                        <label for="Vessel_Name" class="control-label">Weather Report City <span class="required">*</span></label>
                        <div class="controls">
                            <input id="current-pass-control" name="weather_report_city" class="span4" type="text" value=""/>

                        </div>
                    </div>
                    <div class="control-group ">
                        <label for="Vessel_Name" class="control-label">Weather Report Region <span class="required">*</span></label>
                        <div class="controls">
                            <input id="current-pass-control" name="weather_report_region" class="span4" type="text" value=""/>

                        </div>
                    </div>
                </fieldset>
                <fieldset>
                    <legend>Email</legend><br>   
                    <div class="control-group ">
                        <label for="Vessel_Name" class="control-label">SMTP Host <span class="required">*</span></label>
                        <div class="controls">
                            <input id="current-pass-control" name="smtp_host" class="span4" type="text" value=""/>

                        </div>
                    </div>
                    <div class="control-group ">
                        <label for="Vessel_Name" class="control-label">SMTP Port <span class="required">*</span></label>
                        <div class="controls">
                            <input id="current-pass-control" name="smtp_port" class="span4" type="text" value=""/>

                        </div>
                    </div>
                    <div class="control-group ">
                        <label for="Vessel_Name" class="control-label">From Email <span class="required">*</span></label>
                        <div class="controls">
                            <input id="current-pass-control" name="from_email" class="span4" type="text" value=""/>

                        </div>
                    </div>
                    <div class="control-group ">
                        <label for="Vessel_Name" class="control-label">SMTP Username <span class="required">*</span></label>
                        <div class="controls">
                            <input id="current-pass-control" name="smtp_username" class="span4" type="text" value=""/>

                        </div>
                    </div>
                    <div class="control-group ">
                        <label for="Vessel_Name" class="control-label">SMTP Password <span class="required">*</span></label>
                        <div class="controls">
                            <input id="current-pass-control" name="smtp_password" class="span4" type="text" value=""/>

                        </div>
                    </div>                                                                                                                                       
                </fieldset>
                <fieldset>
                    <legend>Booking</legend><br>   
                    <div class="control-group ">
                        <label for="Vessel_Name" class="control-label">New Booking Email Notification<span class="required">*</span></label>
                        <div class="controls">
                            <select name="new_booking_email" class="chosen span2">
                                <option value="1">Yes</option>
                                <option value="0">No</option>
                            </select>
                        </div>
                    </div>                                                                                                                                       
                </fieldset>                
            </div>
        </div>
        <footer id="submit-actions" class="form-actions">
            <button id="submit-button" type="submit" class="btn btn-primary" value="">Save Changes</button>
            <a href="{{ URL::to('/vessels') }}" class="btn btn-default">Back</a>
        </footer>
    </form>
<script>
function addCategory(){
    $('.seat-category-table tbody').append('<tr><td><input type="text" style="width:150px" name="category-name[]"/></td><td><input type="number" style="width:150px" name="capacity[]"/></td><td><input type="number" style="width:150px" name="display-capacity[]"/></td><td><button onclick="$(this).closest(\'tr\').remove()" type="button" class="btn btn-sm btn-remove">Remove</button></td></tr>');
}
</script>
@endsection