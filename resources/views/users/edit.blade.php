@extends('layout.master')
@section('content')
    @include('layout.errors')
    <form method="post" action="{{ URL::to('/users/edit/'.$user->id) }}">
        {{ csrf_field() }}
        <div class="row">
            <div id="acct-password-row" class="span7">
                <fieldset>
                    <legend>Editing {{ $user->name }}</legend><br>
                    <div class="control-group ">
                        <label for="name" class="control-label">Assign to User Group</label>
                        <div class="controls">

                            <select id="current-pass-control" name="user_group_id" class="span4">
                                @foreach($user_groups as $user_group)
                                    <option 
                                        @if($user_group->id == $user->user_group_id) 
                                            {{ 'selected' }} 
                                        @endif 
                                    value="{{ $user_group->id }}">{{ $user_group->title }}</option>
                                @endforeach
                            </select>

                        </div>
                    </div>                    
                    <div class="control-group ">
                        <label for="name" class="control-label">Name <span class="required">*</span></label>
                        <div class="controls">
                            <input id="current-pass-control" name="name" class="span4" type="text" value="{{ $user->name }}" required/>

                        </div>
                    </div>
                    <div class="control-group ">
                        <label for="email" class="control-label">Email Address</label>
                        <div class="controls">
                            <input id="current-pass-control" name="email" class="span4" type="email" value="{{ $user->email }}"/>

                        </div>
                    </div>                                        
                    <div class="control-group ">
                        <label for="username" class="control-label">Username <span class="required">*</span></label>
                        <div class="controls">
                            <input id="current-pass-control" name="username" class="span4" type="text" value="{{ $user->username }}" required/>

                        </div>
                    </div> 
                    <div class="control-group ">
                        <label for="password" class="control-label">New Password <span class="required">*</span></label>
                        <div class="controls">
                            <input id="current-pass-control" name="password" class="span4" type="password" value="" required/>

                        </div>
                    </div>
                    <div class="control-group ">
                        <label for="password_confirmation" class="control-label">New Password Confirmation <span class="required">*</span></label>
                        <div class="controls">
                            <input id="current-pass-control" name="password_confirmation" class="span4" type="password" value="" required/>

                        </div>
                    </div>                                                         
                </fieldset>
            </div>
        </div>
        <footer id="submit-actions" class="form-actions">
            <button id="submit-button" type="submit" class="btn btn-primary" value="">Register</button>
            <a href="{{ URL::to('/sessions') }}" class="btn btn-default">Back</a>
        </footer>
    </form>
@endsection