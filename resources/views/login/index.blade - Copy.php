@extends ('layout.master')

@section ('content')
    
            
    <div class="signin-row row">
        <div class="span4"></div>
        <div class="span8">
            <div class="container-signin">
                <legend>Please Login</legend>
                <form action='dashboard.html' method='POST' id='loginForm' class='form-signin' autocomplete='off'>
                    <div class="form-inner">
                        <div class="input-prepend">
                            
                            <span class="add-on" rel="tooltip" title="Username or E-Mail Address" data-placement="top"><i class="icon-envelope"></i></span>
                            <input type='text' class='span4' id='username'/>
                        </div>

                        <div class="input-prepend">
                            
                            <span class="add-on"><i class="icon-key"></i></span>
                            <input type='password' class='span4' id='password'/>
                        </div>
                        <label class="checkbox" for='remember_me'>Remember me
                            <input type='checkbox' id='remember_me'
                                   />
                        </label>
                    </div>
                    <footer class="signin-actions">
                        <input class="btn btn-primary" type='submit' id="submit" value='Login'/>
                    </footer>
                </form>
            </div>
        </div>
        <div class="span3"></div>
    </div>

            <!-- <div class="signin-row row">
                <div class="span4"></div>
                <div class="span8">
                    <div class="well well-small well-shadow">
                        <legend class="lead">Additional Content</legend>
                        Add additional content here.
                    </div>
                </div>
                <div class="span8"></div>
            </div> -->
        <!--<div class="span4">

            </div>-->
@endsection