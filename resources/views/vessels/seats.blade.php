                        <label class="control-label">Seat Prices</label>
                        <div class="controls">
                            <table class="table-bordered" cellpadding="5">
                                <thead>
                                    <th>Seat Category</th>
                                    <th>Adult Price</th>
                                    <th>Child Price</th>
                                </thead>
                                @foreach ($seats_array as $seat)
                                <tr>
                                    <td>{{ $seat['seat_category_name'] }}</td>
                                    <td>                        
                                        <div class="input-prepend">
                                            <span class="add-on">RM</span>
                                            <input class="span2" type="text" name="adult-price[{{ $seat['seat_category_id'] }}]"/>
                                        </div>   
                                    </td>
                                    <td>                                        
                                        <div class="input-prepend">
                                            <span class="add-on">RM</span>
                                            <input class="span2" type="text" name="child-price[{{ $seat['seat_category_id'] }}]"/>
                                        </div> 
                                    </td>
                                </tr>
                                @endforeach
                            </table>
                        </div>
                        