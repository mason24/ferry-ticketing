@extends('layout.master')
    @section('content')
        @if(session()->has('message'))
            <div class="alert alert-success">
                {{ session()->get('message') }}
            </div>
        @endif

                        <div class="box pattern pattern-sandstone">
                            <div class="box-header">
                                <i class="icon-table"></i>
                                <h5>
                                    Jetties
                                </h5>

                            </div>
                            <div class="form-group">
                                <a href="jetties/create">New Jetty</a> 
                            </div>                            
                            <div class="form-group">
                                Sort: <a href="jetties?sort=asc">Ascending</a> | <a href="jetties?sort=desc">Descending</a> 
                            </div>
                            <div class="box-content box-table">
                                <table id="sample-table" class="table table-hover table-bordered tablesorter">
                                    <thead>
                                        <tr>
                                            <th>Jetty Name</th>
                                            <th>Jetty Code</th>
                                            <th>Created Date</th>
                                            <th>Updated Date</th>
                                            <th class="td-actions"></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    	@foreach ($jetties as $jetty)
	                                        <tr>
	                                            <td>{{ $jetty->Jetty_Name }}</td>
	                                            <td>{{ $jetty->Jetty_Code }}</td>
	                                            <td>{{ $jetty->created_at }}</td>
	                                            <td>{{ $jetty->updated_at }}</td>
	                                            <td class="td-actions">
	                                                <a href="jetties/edit/{{ $jetty->id }}" class="btn btn-small btn-info">
	                                                    <i class="btn-icon-only icon-edit"></i>
	                                                </a>

	                                                <a href="jetties/delete/{{ $jetty->id }}" class="btn btn-small btn-danger">
	                                                    <i class="btn-icon-only icon-remove"></i>
	                                                </a>
	                                            </td>
	                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>

                                {{ $jetties->links() }}
                            </div>
                        </div>
@endsection