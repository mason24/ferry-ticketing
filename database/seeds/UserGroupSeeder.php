<?php

use Illuminate\Database\Seeder;
use App\User_Group;

class UserGroupSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $userGroup = new User_Group();
        $userGroup->title = 'Super Admin';
        $userGroup->save();

    }
}
