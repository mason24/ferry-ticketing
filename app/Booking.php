<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Booking extends Model
{
    public function origin(){

    	return $this->belongsTo('App\Jetty','origin_id');

    }

    public function destination(){

    	return $this->belongsTo('App\Jetty','destination_id');

    } 

    public function onward_trip(){

    	return $this->belongsTo('App\Trip','selected_onward_trip_id');

    }

    public function passengers(){

    	return $this->hasMany('App\Passenger','booking_id');

    }

    public function user(){

    	return $this->belongsTo('App\User','created_by');

    }              
}
