<?php

use Illuminate\Database\Seeder;
use App\User;
use Carbon\Carbon;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new User;
        $user->name = 'Admin';
        $user->username = 'admin';
        $user->email = 'admin@example.com';
        $user->password = bcrypt('admin');
        $user->user_group_id = 1;
        $user->last_login_at = Carbon::now();
        $user->user_status = 1;
        $user->user_status = 1;
        $user->save();
    }
}
