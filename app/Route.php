<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Route extends Model
{
    // protected $guarded = ['Message_ID'];
    public function trips(){

    	return $this->hasMany('App\Trip','Route_Id');

    }

    public function route_from(){

    	return $this->belongsTo('App\Jetty','Route_From');

    }

    public function route_to(){

    	return $this->belongsTo('App\Jetty','Route_To');

    }    
}
