@foreach($destinations as $destination)
<div class="col-md-6">
    <div class="form-group">
        <button data-destination-code="{{ $destination->Jetty_Code }}" data-tooltip="{{ $destination->Jetty_Name }}" type="button" rel="{{ $destination->id }}" value="{{ $destination->id }}" style="width:100%;font-size: 20px;background-color:{{ $destination->pos_button_background_color }}" class="btn btn-default destination-button" onclick="select_destination($(this))"><b {{ $destination->pos_button_background_color == '#ffffff' ? '' : 'style=color:white' }}>{{ $destination->Jetty_Code }}</b>
        </button>
    </div>
</div>
@endforeach